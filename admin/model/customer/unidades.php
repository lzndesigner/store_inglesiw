<?php
class ModelCustomerUnidades extends Model
{
	public function getUnidade($unidade_id)
	{
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "code_codigos_unidades WHERE unidade_id = '" . (int)$unidade_id . "'");

		return $query->row;
	}

	public function getUnidadeByNome($nome)
	{
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "code_codigos_unidades WHERE nome = '" . $this->db->escape($nome) . "'");

		return $query->row;
	}

	public function getUnidades($data = array())
	{
		$sql = "SELECT u.*, (SELECT COUNT(*) FROM " . DB_PREFIX . "code_codigos_codigos cc WHERE cc.unidade_id = u.unidade_id) AS `total` FROM " . DB_PREFIX . "code_codigos_unidades u";

		$implode = array();

		if (!empty($data['filter_nome'])) {
			$implode[] = "LIKE '%" . $this->db->escape($data['filter_nome']) . "%'";
		}
		if (!empty($data['filter_date_created'])) {
			$implode[] = "DATE(c.date_created) = DATE('" . $this->db->escape($data['filter_date_created']) . "')";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}

		$sort_data = array(
			'u.nome',
			'u.date_created'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY unidade_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}


	public function getTotalUnidades($data = array())
	{
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "code_codigos_unidades u";

		$implode = array();

		if (!empty($data['filter_nome'])) {
			$implode[] = "LIKE '%" . $this->db->escape($data['filter_nome']) . "%'";
		}

		if (!empty($data['filter_date_created'])) {
			$implode[] = "DATE(date_created) = DATE('" . $this->db->escape($data['filter_date_created']) . "')";
		}

		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function addUnidade($data)
	{
		$this->db->query("INSERT INTO " . DB_PREFIX . "code_codigos_unidades SET nome = '" . $this->db->escape($data['nome']) . "', prefixo = '" . $this->db->escape($data['prefixo']) . "', date_created = NOW()");

		$unidade_id = $this->db->getLastId();

		return $unidade_id;
	}

	public function editUnidade($unidade_id, $data)
	{
		$this->db->query("UPDATE " . DB_PREFIX . "code_codigos_unidades SET nome = '" . $this->db->escape($data['nome']) . "', prefixo = '" . $this->db->escape($data['prefixo']) . "' WHERE unidade_id = '" . (int)$unidade_id . "'");
	}

	public function deleteUnidade($unidade_id)
	{
		$this->db->query("DELETE FROM " . DB_PREFIX . "code_codigos_unidades WHERE unidade_id = '" . (int)$unidade_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "code_codigos_codigos WHERE unidade_id = '" . (int)$unidade_id . "'");
	}



	// Codigos	//*****************
	// Codigos	//*****************
	// Codigos	//*****************

	public function getCodigos($data = array())
	{

		$sql = "SELECT cc.codigo_id, cc.codigo, cc.unidade_id, cc.customer_id, cc.date_created, u.nome as unidade_nome, u.prefixo, c.custom_field, CONCAT(c.firstname, ' ', c.lastname) as customer_name, c.email as customer_email FROM " . DB_PREFIX . "code_codigos_codigos cc LEFT JOIN " . DB_PREFIX . "code_codigos_unidades u ON (cc.unidade_id = u.unidade_id) LEFT JOIN " . DB_PREFIX . "customer c ON (cc.customer_id = c.customer_id) WHERE cc.unidade_id = " . $data['unidade_id'] ." ";

		
		$implode = array();
		
		if (!empty($data['filter_codigo'])) {
			$implode[] = "cc.codigo = '" . $this->db->escape($data['filter_codigo']) . "'";
		}
		if (!empty($data['filter_customer_id'])) {
			$implode[] = "cc.customer_id = '" . $this->db->escape($data['filter_customer_id']) . "'";
		}

		if (!empty($data['filter_customer'])) {
			if($data['filter_customer'] == 1){
				$implode[] = "cc.customer_id >= 1";
			}else if($data['filter_customer'] == 2){
				$implode[] = "cc.customer_id IS NULL";
			}
		}

		if (!empty($data['filter_date_created'])) {
			$implode[] = "DATE(cc.date_created) = DATE('" . $this->db->escape($data['filter_date_created']) . "')";
		}
		
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}

		$sort_data = array(
			'cc.codigo',
			'cc.customer_id',
			'cc.unidade_id',
			'cc.date_created'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY cc.customer_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	
	public function delete_only_customers($unidade_id)
	{
		// SCRIPT para deletar os clientes que utilizaram os códigos de uma determinada UNIDADE
		// capta o todos CUSTOMER_ID dos clientes que utilizaram os códigos de acesso
		$sql = "SELECT cc.unidade_id, cc.customer_id FROM " . DB_PREFIX . "code_codigos_codigos cc LEFT JOIN " . DB_PREFIX . "code_codigos_unidades u ON (cc.unidade_id = u.unidade_id) WHERE cc.unidade_id = " . $unidade_id ." AND cc.customer_id >= 1 ";
		// adicionar após a unidade_id AND cc.customer_id >= 1
		
		// capta todos os CUSTOMER_ID dos clientes que estão na UNIDADE
		// SELECT * FROM oc_customer WHERE custom_field LIKE '{"3":"8",%' 


		$query = $this->db->query($sql);

		// com o CUSTOMER_ID, capto o ORDER_ID dos pedidos dos clientes desejados
		foreach($query->rows as $customers){
			$sql2 = "SELECT * FROM " . DB_PREFIX . "order WHERE customer_id = '".$customers['customer_id']."'";

			$query2 = $this->db->query($sql2);

			// com o ORDER_ID, apago todos os pedidos dos clientes
			foreach($query2->rows as $orders){
				$this->db->query("DELETE FROM ". DB_PREFIX ."order WHERE order_id = '".$orders['order_id']."'");
				$this->db->query("DELETE FROM ". DB_PREFIX ."order_history WHERE order_id = '".$orders['order_id']."'");
				$this->db->query("DELETE FROM ". DB_PREFIX ."order_product WHERE order_id = '".$orders['order_id']."'");
				$this->db->query("DELETE FROM ". DB_PREFIX ."order_total WHERE order_id = '".$orders['order_id']."'");
			}
			
			// após apagar os pedidos, apago todos os clientes
			$this->db->query("DELETE FROM " . DB_PREFIX . "customer WHERE customer_id = '" . $customers['customer_id'] . "'");
			$this->db->query("DELETE FROM " . DB_PREFIX . "customer_activity WHERE customer_id = '" . $customers['customer_id'] . "'");
			$this->db->query("DELETE FROM " . DB_PREFIX . "customer_affiliate WHERE customer_id = '" . $customers['customer_id'] . "'");
			$this->db->query("DELETE FROM " . DB_PREFIX . "customer_approval WHERE customer_id = '" . $customers['customer_id'] . "'");
			$this->db->query("DELETE FROM " . DB_PREFIX . "customer_history WHERE customer_id = '" . $customers['customer_id'] . "'");
			$this->db->query("DELETE FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . $customers['customer_id'] . "'");
			$this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . $customers['customer_id'] . "'");
			$this->db->query("DELETE FROM " . DB_PREFIX . "customer_ip WHERE customer_id = '" . $customers['customer_id'] . "'");
			$this->db->query("DELETE FROM " . DB_PREFIX . "address WHERE customer_id = '" . $customers['customer_id'] . "'");

			// finalizo apagando o código de acesso do cliente
			$this->db->query("DELETE FROM " . DB_PREFIX . "code_codigos_codigos WHERE customer_id = '" . $customers['customer_id'] . "'");
		}
	}


	public function deleteCustomerOrders($customer_id)
	{
		$sql = "SELECT * FROM " . DB_PREFIX . "customer c WHERE customer_id = '".$customer_id."'";
		
		$query = $this->db->query($sql);

		if($query->rows){
			foreach($query->rows as $customers){
				$sql2 = "SELECT * FROM " . DB_PREFIX . "order WHERE customer_id = '".$customers['customer_id']."'";

				$query2 = $this->db->query($sql2);

				// com o ORDER_ID, apago todos os pedidos dos clientes
				foreach($query2->rows as $orders){
					$this->db->query("DELETE FROM ". DB_PREFIX ."order WHERE order_id = '".$orders['order_id']."'");
					$this->db->query("DELETE FROM ". DB_PREFIX ."order_history WHERE order_id = '".$orders['order_id']."'");
					$this->db->query("DELETE FROM ". DB_PREFIX ."order_product WHERE order_id = '".$orders['order_id']."'");
					$this->db->query("DELETE FROM ". DB_PREFIX ."order_total WHERE order_id = '".$orders['order_id']."'");
				}
				
				// após apagar os pedidos, apago todos os clientes
				$this->db->query("DELETE FROM " . DB_PREFIX . "customer WHERE customer_id = '" . $customers['customer_id'] . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "customer_activity WHERE customer_id = '" . $customers['customer_id'] . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "customer_affiliate WHERE customer_id = '" . $customers['customer_id'] . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "customer_approval WHERE customer_id = '" . $customers['customer_id'] . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "customer_history WHERE customer_id = '" . $customers['customer_id'] . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . $customers['customer_id'] . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . $customers['customer_id'] . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "customer_ip WHERE customer_id = '" . $customers['customer_id'] . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "address WHERE customer_id = '" . $customers['customer_id'] . "'");

				// finalizo apagando o código de acesso do cliente
				$this->db->query("DELETE FROM " . DB_PREFIX . "code_codigos_codigos WHERE customer_id = '" . $customers['customer_id'] . "'");
			}
		}else{
			// finalizo apagando o código de acesso do cliente
			$this->db->query("DELETE FROM " . DB_PREFIX . "code_codigos_codigos WHERE customer_id = '" . $customer_id . "'");
		}
		
	}


		
	public function delete_customer_orders($unidade_id, $customer_id)
	{
		$sql = "SELECT * FROM " . DB_PREFIX . "customer c WHERE customer_id = '". $customer_id ."'";
		
		$query = $this->db->query($sql);
		
		if($query->rows){
			foreach($query->rows as $customers){
				$sql2 = "SELECT * FROM " . DB_PREFIX . "order WHERE customer_id = '".$customers['customer_id']."'";

				$query2 = $this->db->query($sql2);

				// com o ORDER_ID, apago todos os pedidos dos clientes
				foreach($query2->rows as $orders){
					$this->db->query("DELETE FROM ". DB_PREFIX ."order WHERE order_id = '".$orders['order_id']."'");
					$this->db->query("DELETE FROM ". DB_PREFIX ."order_history WHERE order_id = '".$orders['order_id']."'");
					$this->db->query("DELETE FROM ". DB_PREFIX ."order_product WHERE order_id = '".$orders['order_id']."'");
					$this->db->query("DELETE FROM ". DB_PREFIX ."order_total WHERE order_id = '".$orders['order_id']."'");
				}				
				
				// após apagar os pedidos, apago todos os clientes
				$this->db->query("DELETE FROM " . DB_PREFIX . "customer WHERE customer_id = '" . $customers['customer_id'] . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "customer_activity WHERE customer_id = '" . $customers['customer_id'] . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "customer_affiliate WHERE customer_id = '" . $customers['customer_id'] . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "customer_approval WHERE customer_id = '" . $customers['customer_id'] . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "customer_history WHERE customer_id = '" . $customers['customer_id'] . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . $customers['customer_id'] . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . $customers['customer_id'] . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "customer_ip WHERE customer_id = '" . $customers['customer_id'] . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "address WHERE customer_id = '" . $customers['customer_id'] . "'");
				
			}
		}else{
			// finalizo apagando o código de acesso do cliente
			$this->db->query("DELETE FROM " . DB_PREFIX . "code_codigos_codigos WHERE customer_id = '" . $customer_id . "'");
		}
	}


	public function block_only_customers($unidade_id)
	{
	// precisa ser rodado manualmente no PHPMyADMIN
	// 4 = Campo Limpo
	// 9 = EmbuGuaçu

	// 1° encontrar os clientes que escolheram a unidade 4 e 9, depois mudar o status para 0 para bloquear o acesso
	// 2° encontrar os clientes que utilizaram o código de acesso da unidade 4 e 9, depois bloquear o acesso

	// SELECT * FROM oc_customer WHERE custom_field LIKE '{"3":"9"%'
	// UPDATE oc_customer SET status = '0' WHERE custom_field LIKE '{"3":"9"%'

		// 6 = Embu Guaçu
		// 8 = Campo Limpo

		$sql = "SELECT * FROM oc_code_codigos_codigos WHERE unidade_id = '6' AND customer_id IS NOT NULL";
		$query = $this->db->query($sql);

		// com o CUSTOMER_ID, capto o ORDER_ID dos pedidos dos clientes desejados
		foreach($query->rows as $customers){
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET status = '0' WHERE customer_id = '" . $customers['customer_id'] . "'");
		}
	}


	public function getTotalCodigos($data = array())
	{
		$sql = "SELECT cc.codigo, COUNT(*) AS total FROM " . DB_PREFIX . "code_codigos_codigos cc WHERE cc.unidade_id = " . $data['unidade_id'];

		$implode = array();

		if (!empty($data['filter_codigo'])) {
			$implode[] = "cc.codigo = '" . $this->db->escape($data['filter_codigo']) . "'";
		}
		if (!empty($data['filter_customer_id'])) {
			$implode[] = "cc.customer_id = '" . $this->db->escape($data['filter_customer_id']) . "'";
		}

		if (!empty($data['filter_customer'])) {
			if($data['filter_customer'] == 1){
				$implode[] = "cc.customer_id >= 1";
			}else if($data['filter_customer'] == 2){
				$implode[] = "cc.customer_id IS NULL";
			}
		}

		if (!empty($data['filter_date_created'])) {
			$implode[] = "DATE(date_created) = DATE('" . $this->db->escape($data['filter_date_created']) . "')";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getCodigosUsed($data = array())
	{
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "code_codigos_codigos cc WHERE cc.unidade_id = " . $data['unidade_id'] ." AND cc.customer_id >= 1 ";

		$implode = array();

		if (!empty($data['filter_codigo'])) {
			$implode[] = "cc.codigo = '" . $this->db->escape($data['filter_codigo']) . "'";
		}
		if (!empty($data['filter_customer_id'])) {
			$implode[] = "cc.customer_id = '" . $this->db->escape($data['filter_customer_id']) . "'";
		}

		if (!empty($data['filter_customer'])) {
			if($data['filter_customer'] == 1){
				$implode[] = "cc.customer_id >= 1";
			}else if($data['filter_customer'] == 2){
				$implode[] = "cc.customer_id IS NULL";
			}
		}

		if (!empty($data['filter_date_created'])) {
			$implode[] = "DATE(date_created) = DATE('" . $this->db->escape($data['filter_date_created']) . "')";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}


	public function addCodigos($data = array(), $unidade_id)
	{
		
		foreach($data as $codigo){

			$sql = "SELECT COUNT(*) FROM ". DB_PREFIX ."code_codigos_codigos WHERE codigo = '". $codigo ."'";
			$query = $this->db->query($sql);
			$verify = $query->rows;

			if($verify == 1){
				// var_dump('existe');
			}else{
				// var_dump('nao existe');
				$this->db->query("INSERT INTO " . DB_PREFIX . "code_codigos_codigos SET codigo = '" . $codigo . "', customer_id = null, unidade_id = ". $unidade_id .", date_created = NOW(), date_modified = NOW()");
			}
			
		}

		return true;
	}


	public function verifyDuplicateCodigos($unidade_id)
	{
		$sql = "SELECT codigo, COUNT(*) FROM oc_code_codigos_codigos WHERE unidade_id = '" . $unidade_id . "' GROUP BY codigo HAVING COUNT(*) > 1";
		$query = $this->db->query($sql);
		
		return $query->rows;
	}

	public function deleteCodigo($codigo_id)
	{
		$this->db->query("DELETE FROM " . DB_PREFIX . "code_codigos_codigos WHERE codigo_id = '" . (int)$codigo_id . "'");
	}

	public function deleteAllCodigo($unidade_id)
	{
		$this->db->query("DELETE FROM " . DB_PREFIX . "code_codigos_codigos WHERE unidade_id = '" . (int)$unidade_id . "' AND customer_id IS NULL");
	}


	public function exportAllCodes($data = array(), $unidade_id) {

		$sql = "SELECT codigo_id, codigo, customer_id FROM " . DB_PREFIX . "code_codigos_codigos where unidade_id = '".$unidade_id."'";

		$query = $this->db->query($sql);

		return $query->rows;	
	}


	public function exportAvailable($data = array(), $unidade_id) {

		$sql = "SELECT codigo_id, codigo FROM " . DB_PREFIX . "code_codigos_codigos where unidade_id = '".$unidade_id."' and customer_id is NULL";

		$query = $this->db->query($sql);

		return $query->rows;	
	}
}