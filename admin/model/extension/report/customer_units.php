<?php
class ModelExtensionReportCustomerUnits extends Model
{
    public function getUnits($data = array())
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "custom_field_value_description` WHERE custom_field_id = '3' ORDER BY name ASC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        $units = [];

        foreach ($query->rows as $unit) {
            $units[$unit['custom_field_value_id']] = [
                'unit_id' => $unit['custom_field_value_id'],
                'unit_name' => $unit['name']
            ];
        }
        return $units;
    }

    public function getTotalUnits($data = array())
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "custom_field_value_description` WHERE custom_field_id = '3' ORDER BY name ASC";
        $query = $this->db->query($sql);

        return $query->num_rows;
    }

    public function getUnit($unit_id)
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "custom_field_value_description` WHERE custom_field_value_id = '" . $unit_id . "'";
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getProducts($order_id)
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "order_product` WHERE order_id = '" . $order_id . "'";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getComments($order_id)
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "order_history` WHERE order_id = '" . $order_id . "' ORDER BY order_history_id ASC";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getCustomers($unit_id, $data = array())
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "customer` ORDER BY customer_id ASC";
        $query = $this->db->query($sql);

        $customers = [];

        foreach ($query->rows as $customer) {
            $custom_field = json_decode($customer['custom_field'], true);
            if (isset($custom_field) && isset($custom_field[3]) && $custom_field[3] == $unit_id) {
                $sql_orders = "SELECT * FROM `" . DB_PREFIX . "order` WHERE customer_id = '" . $customer['customer_id'] . "'";

                if (!empty($data['filter_date_start'])) {
                    $sql_orders .= " AND DATE(date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
                }

                if (!empty($data['filter_date_end'])) {
                    $sql_orders .= " AND DATE(date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
                }

                $sql_orders .= " ORDER BY date_added DESC";

                $query_orders = $this->db->query($sql_orders);

                $orders = [];
                if ($query_orders->num_rows > 0) {
                    foreach ($query_orders->rows as $order) {

                        $sql_products = "SELECT * FROM `" . DB_PREFIX . "order_product` WHERE order_id = '" . $order['order_id'] . "' ORDER BY order_id ASC";
                        $query_products = $this->db->query($sql_products);

                        $products = [];
                        if ($query_products->num_rows > 0) {
                            foreach ($query_products->rows as $product) {
                                $products[] = [
                                    'product_name' => $product['name'],
                                    'product_quantity' => $product['quantity'],
                                    'product_price' => $this->currency->format($product['price'], $this->config->get('config_currency')),
                                    'product_total' => $this->currency->format($product['total'], $this->config->get('config_currency')),
                                ];
                            }
                        }

                        $orders[] = [
                            'order_id' => $order['order_id'],
                            'order_total' => $this->currency->format($order['total'], $this->config->get('config_currency')),
                            'order_payment_method' => $order['payment_method'],
                            'order_date_added' => date($this->language->get('date_format_short'), strtotime($order['date_added'])),
                            'order_products' => $products,
                        ];
                        // var_dump($orders);exit;
                    }
                }

                $customers[] = [
                    'customer_id' => $customer['customer_id'],
                    'name' => $customer['firstname'] . ' ' . $customer['lastname'],
                    'orders' => $orders,
                ];
            }
        }

        // var_dump($customers);exit;

        return $customers;
    }

    public function getOrders($unit_id, $data = array())
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "order` ";

        if (isset($data['filter_date_start'])) {
            $sql .= " WHERE DATE(date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
        } else {
            $sql .= "";
        }

        $sql .= " ORDER BY date_added ASC";

        // $sql .= " LIMIT 10";

        $query = $this->db->query($sql);

        $orders = [];

        foreach ($query->rows as $order) {
            $custom_field = json_decode($order['custom_field'], true);
            if (isset($custom_field) && isset($custom_field[3])) {
                $unit_name = $this->getUnit($custom_field[3]);
            } else {
                $unit_name = '-';
            }
            $products = $this->getProducts($order['order_id']);

            // var_dump($order['payment_method']);exit;
            $ar_products = [];
            foreach ($products as $product) {
                // var_dump($product);
                $ar_products[] = [
                    'product_name' => $product['name'],
                    'product_quantity' => $product['quantity'],
                    'product_price' => $this->currency->format($product['price'], $this->config->get('config_currency')),
                    'product_total' => $this->currency->format($product['total'], $this->config->get('config_currency')),
                ];
            }

            $orders[] = [
                'date_added' => date($this->language->get('date_format_short'), strtotime($order['date_added'])),
                'customer_name' => $order['firstname'] . ' ' . $order['lastname'],
                'products' => $ar_products,
                'payment_method' => $order['payment_method'],
                'total' => $this->currency->format($order['total'], $this->config->get('config_currency')),
                'unit_name' => isset($unit_name['name']) ? ($unit_name['name'] ? $unit_name['name'] : '-') : '-',
            ];
            //}
        }

        // var_dump($orders);exit;

        return $orders;
    }

    public function getOrdersUnits($data = array())
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "order` WHERE 1 ";

        if (isset($data['filter_date_start'])) {
            $sql .= " AND DATE(date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
        } else {
            $sql .= "";
        }
        if (isset($data['filter_date_end'])) {
            $sql .= " AND DATE(date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
        } else {
            $sql .= "";
        }

        // Agrupar as condições de order_status_id com parênteses
        $sql .= " AND (order_status_id = '15' OR order_status_id = '5' OR order_status_id = '3')";    

        $sql .= " ORDER BY date_added ASC";

        // $sql .= " LIMIT 10";
        
        $query = $this->db->query($sql);

        $orders = [];

        foreach ($query->rows as $order) {
            $custom_field = json_decode($order['custom_field'], true);
            if (isset($custom_field) && isset($custom_field[3])) {
                $unit_name = $this->getUnit($custom_field[3]);

                $products = $this->getProducts($order['order_id']);

                // var_dump($order['payment_method']);exit;
                $ar_products = [];
                foreach ($products as $product) {
                    // var_dump($product);
                    $ar_products[] = [
                        'product_name' => $product['name'],
                        'product_quantity' => $product['quantity'],
                        'product_price' => $this->currency->format($product['price'], $this->config->get('config_currency')),
                        'product_total' => $this->currency->format($product['total'], $this->config->get('config_currency')),
                    ];
                }

                $order_comments = $this->getComments($order['order_id']);
                // $string_replace = [
                //     'Pagamento por Cartão MasterCard',
                //     'Pagamento por Cartão Visa',
                //     'Dados do Titular do Cartão:',
                //     'Os 4 últimos números do Cartão: ',
                //     '<br>',
                //     // 'parcelas',
                // ];

                $comment = [];
                if(isset($order_comments)){
                    if($order['payment_method'] == 'Cartão de Crédito'){
                        foreach($order_comments as $comments){
                            // $text_comment = str_replace($string_replace, '', $comments['comment']);
                            // $comment[] = substr($text_comment, 4, 6);
                            $comment[] = $comments['comment'];
                        }
                    }
                }else{
                    $comment = '-';
                }

                $orders[] = [
                    'date_added' => date($this->language->get('date_format_short'), strtotime($order['date_added'])),
                    'unit_name' => isset($unit_name['name']) ? ($unit_name['name'] ? $unit_name['name'] : '-') : '-',
                    'customer_name' => $order['firstname'] . ' ' . $order['lastname'],
                    'products' => $ar_products,
                    'payment_method' => $order['payment_method'],
                    'total' => $this->currency->format($order['total'], $this->config->get('config_currency')),
                    'parcelas' => $comment,
                ];
            }
        }

        // var_dump($orders);exit;

        return $orders;
    }
}
