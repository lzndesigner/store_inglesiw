<?php

use Vindi\Exceptions\ValidationException;

require_once DIR_SYSTEM . '/library/code_vindi/vendor/autoload.php';

/**
 * Class ModelExtensionPaymentCodeVindi
 *
 * @author Felipo Antonoff Araújo
 * @version 1.0
 * @package code_vindi
 */
class ModelExtensionPaymentCodeVindi extends Model
{
    /**
     * @var \stdClass
     */
    private $conf;
    private $log;

    /**
     * Definicoes iniciais de boot
     *
     * @param $registry
     *
     * @throws \Exception
     */
    public function __construct($registry)
    {
        parent::__construct($registry);

        $this->load->model('module/codemarket_module');
        $this->conf = $this->model_module_codemarket_module->getModulo('570');

        if ($this->conf->code_env === 'sandbox') {
            putenv('VINDI_API_KEY=' . $this->conf->code_sandbox_private);
            putenv('VINDI_API_URI=https://sandbox-app.vindi.com.br/api/v1/');
        } else {
            putenv('VINDI_API_KEY=' . $this->conf->code_production_private);
            putenv('VINDI_API_URI=https://app.vindi.com.br/api/v1/');
        }

        $this->log = new Log('Code-Vindi-Admin.log');
    }

    public function getTransactions($query)
    {
        $billService = new \Vindi\Bill();

        //$query = 'subscription_id=' . $subscriptionId;

        try {
            $bills = $billService->all(
                [
                    'page'       => 1,
                    'per_page'   => 10000,
                    'query'      => $query,
                    'sort_by'    => 'created_at',
                    'sort_order' => 'asc',
                ]
            );

            if (!empty($bills)) {
                return $bills;
                /*
                foreach ($bills as $bill) {
                    if ($bill->subscription->id != $subscriptionId) {
                        break;
                    }

                    echo $bill->id;
                    echo "<br>";
                    echo $bill->status;
                    echo "<br>";
                    echo $bill->url;

                    echo $bill->created_at;
                    echo $bill->updated_at;
                    print_r($bill);
                }
                */
            }
            //print_r($bills);
        } catch (ValidationException $v) {
            $this->log->write('Buscar fatura falhou: ' . print_r($v, true));
            //print_r($v);

            return false;
        }
    }

    public function getTransactionsArray($vindi_assinatura_id)
    {
        $bill = $this->getTransactions("subscription_id=$vindi_assinatura_id");
        $sub = $this->getAssinaturas($vindi_assinatura_id);

        if (empty($bill) || empty($sub)) {
            return false;
        }
        //print_r($bill);
        //print_r($sub);
        //exit();

        //echo   date("d/m/Y", strtotime($bill[0]->charges[0]->created_at));
        //print_r($this->conf);
        //print_r($bill);

        $created_at = date("d/m/Y", strtotime($sub->created_at));
        $updated_at = date("d/m/Y", strtotime($sub->updated_at));

        $sub = [
            'id'         => $sub->id,
            'plan'       => $sub->plan->name,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
        ];

        //Faturas
        $bills = [];
        foreach ($bill as $b) {
            if (!empty($b->charges[0]->last_transaction->status)) {
                $status = 'code_charge_' . $b->charges[0]->last_transaction->status;
            } else {
                $this->log->write('getTransactionsArray - Sem Status definido na Vindi');
                continue;
            }

            $status = 'code_charge_' . $b->charges[0]->last_transaction->status;
            //echo $b->status; exit();

            if ($status == 'code_charge_waiting') {
                $status = 'code_charge_created';
            }

            if ($status == 'code_charge_success') {
                $status = 'code_charge_paid';
            }

            if (empty($this->conf->{$status})) {
                $this->log->write('getTransactionsArray - Sem Status definido: ' . $status);
                return false;
            }

            $order_status_id = $this->conf->{$status};
            $order_status = $this->db->query("SELECT name FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int) $order_status_id . "' LIMIT 1");

            $id = $b->charges[0]->id;
            $created_at = date("d/m/Y", strtotime($b->charges[0]->created_at));
            $updated_at = date("d/m/Y", strtotime($b->charges[0]->updated_at));
            $amount = $this->currency->format(($b->charges[0]->amount), $this->session->data['currency']);

            if (!empty($b->charges[0]->print_url)) {
                $urlBoleto = $b->charges[0]->print_url;
                $barcodeBoleto = !empty($b->charges[0]->last_transaction->gateway_response_fields->typable_barcode) ? $b->charges[0]->last_transaction->gateway_response_fields->typable_barcode : '';

                $comment = "Pagamento por Boleto<br>";
                $comment .= '<b><a href="' . $urlBoleto . '" target="_blank">Abrir Boleto</a></b>';
            }

            if (empty($comment)) {
                $comment = 'Pagamento por Cartão';
            }

            $bills[] = [
                'id'         => $id,
                'created_at' => $created_at,
                'updated_at' => $updated_at,
                'amount'     => $amount,
                'status'     => $order_status->row['name'],
                'comment'    => $comment,
            ];
        }

        //print_r($bills); exit();
        return ['sub' => $sub, 'bills' => $bills];
    }

    public function getAssinaturas($id)
    {
        $subscriptionService = new \Vindi\Subscription();
        return $subscriptionService->get($id);
    }
}
