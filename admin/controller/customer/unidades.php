<?php
class ControllerCustomerUnidades extends Controller
{
	private $error = array();

	public function index()
	{
		$this->load->language('customer/unidades');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('customer/customer');
		$this->load->model('customer/unidades');

		$this->getList();
	}

	protected function getList()
	{
		if (isset($this->request->get['filter_nome'])) {
			$filter_nome = $this->request->get['filter_nome'];
		} else {
			$filter_nome = '';
		}

		if (isset($this->request->get['filter_date_created'])) {
			$filter_date_created = $this->request->get['filter_date_created'];
		} else {
			$filter_date_created = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'nome';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = (int)$this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_nome'])) {
			$url .= '&filter_nome=' . urlencode(html_entity_decode($this->request->get['filter_nome'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date_created'])) {
			$url .= '&filter_date_created=' . $this->request->get['filter_date_created'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('customer/unidades', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		$data['add'] = $this->url->link('customer/unidades/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['delete'] = $this->url->link('customer/unidades/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);


		$data['unidades'] = array();

		$filter_data = array(
			'filter_nome'              => $filter_nome,
			'filter_date_created'        => $filter_date_created,
			'sort'                     => $sort,
			'order'                    => $order,
			'start'                    => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                    => $this->config->get('config_limit_admin')
		);

		$unidade_total = $this->model_customer_unidades->getTotalUnidades($filter_data);

		$results = $this->model_customer_unidades->getUnidades($filter_data);

		foreach ($results as $result) {

			$data['unidades'][] = array(
				'unidade_id'     => $result['unidade_id'],
				'nome'           => $result['nome'],
				'prefixo'        => $result['prefixo'],
				'total'          => $result['total'],
				'date_created'   => date($this->language->get('date_format_short'), strtotime($result['date_created'])),
				'showcodes'      => $this->url->link('customer/unidades/codigos', 'user_token=' . $this->session->data['user_token'] . '&unidade_id=' . $result['unidade_id'] . $url, true),
				'edit'           => $this->url->link('customer/unidades/edit', 'user_token=' . $this->session->data['user_token'] . '&unidade_id=' . $result['unidade_id'] . $url, true)
			);
		}

		$data['user_token'] = $this->session->data['user_token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_nome'])) {
			$url .= '&filter_nome=' . urlencode(html_entity_decode($this->request->get['filter_nome'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_date_created'])) {
			$url .= '&filter_date_created=' . $this->request->get['filter_date_created'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_nome'] = $this->url->link('customer/unidades', 'user_token=' . $this->session->data['user_token'] . '&sort=nome' . $url, true);
		$data['sort_date_created'] = $this->url->link('customer/unidades', 'user_token=' . $this->session->data['user_token'] . '&sort=c.date_created' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_nome'])) {
			$url .= '&filter_nome=' . urlencode(html_entity_decode($this->request->get['filter_nome'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date_created'])) {
			$url .= '&filter_date_created=' . $this->request->get['filter_date_created'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $unidade_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('customer/unidades', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($unidade_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($unidade_total - $this->config->get('config_limit_admin'))) ? $unidade_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $unidade_total, ceil($unidade_total / $this->config->get('config_limit_admin')));

		$data['filter_nome'] = $filter_nome;
		$data['filter_date_created'] = $filter_date_created;

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('customer/unidade_list', $data));
	}

	public function add()
	{
		$this->load->language('customer/unidades');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('customer/unidades');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_customer_unidades->addUnidade($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_nome'])) {
				$url .= '&filter_nome=' . urlencode(html_entity_decode($this->request->get['filter_nome'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_prefixo'])) {
				$url .= '&filter_prefixo=' . urlencode(html_entity_decode($this->request->get['filter_prefixo'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_date_created'])) {
				$url .= '&filter_date_created=' . $this->request->get['filter_date_created'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('customer/unidades', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit()
	{
		$this->load->language('customer/unidades');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('customer/unidades');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_customer_unidades->editUnidade($this->request->get['unidade_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_nome'])) {
				$url .= '&filter_nome=' . urlencode(html_entity_decode($this->request->get['filter_nome'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_prefixo'])) {
				$url .= '&filter_prefixo=' . urlencode(html_entity_decode($this->request->get['filter_prefixo'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_date_created'])) {
				$url .= '&filter_date_created=' . $this->request->get['filter_date_created'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('customer/unidades', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	protected function getForm()
	{
		$data['text_form'] = !isset($this->request->get['unidade_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

		$data['user_token'] = $this->session->data['user_token'];

		if (isset($this->request->get['unidade_id'])) {
			$data['unidade_id'] = (int)$this->request->get['unidade_id'];
		} else {
			$data['unidade_id'] = 0;
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['nome'])) {
			$data['error_nome'] = $this->error['nome'];
		} else {
			$data['error_nome'] = '';
		}

		if (isset($this->error['prefixo'])) {
			$data['error_prefixo'] = $this->error['prefixo'];
		} else {
			$data['error_prefixo'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_nome'])) {
			$url .= '&filter_nome=' . urlencode(html_entity_decode($this->request->get['filter_nome'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_prefixo'])) {
			$url .= '&filter_prefixo=' . urlencode(html_entity_decode($this->request->get['filter_prefixo'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('customer/unidades', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		if (!isset($this->request->get['unidade_id'])) {
			$data['action'] = $this->url->link('customer/unidades/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('customer/unidades/edit', 'user_token=' . $this->session->data['user_token'] . '&unidade_id=' . $this->request->get['unidade_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('customer/unidades', 'user_token=' . $this->session->data['user_token'] . $url, true);

		if (isset($this->request->get['unidade_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$unidade_info = $this->model_customer_unidades->getUnidade($this->request->get['unidade_id']);
		}

		if (isset($this->request->post['nome'])) {
			$data['nome'] = $this->request->post['nome'];
		} elseif (!empty($unidade_info)) {
			$data['nome'] = $unidade_info['nome'];
		} else {
			$data['nome'] = '';
		}

		if (isset($this->request->post['prefixo'])) {
			$data['prefixo'] = $this->request->post['prefixo'];
		} elseif (!empty($unidade_info)) {
			$data['prefixo'] = $unidade_info['prefixo'];
		} else {
			$data['prefixo'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('customer/unidade_form', $data));
	}

	protected function validateForm()
	{
		if (!$this->user->hasPermission('modify', 'customer/customer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['nome']) < 1) || (utf8_strlen(trim($this->request->post['nome'])) > 32)) {
			$this->error['nome'] = $this->language->get('error_nome');
		}

		if ((utf8_strlen($this->request->post['prefixo']) < 1) || (utf8_strlen(trim($this->request->post['prefixo'])) > 32)) {
			$this->error['prefixo'] = $this->language->get('error_prefixo');
		}

		// Validar se existe já um nome ou prefixo igual
		$unidade_info = $this->model_customer_unidades->getUnidadeByNome($this->request->post['nome']);

		if (!isset($this->request->get['unidade_id'])) {
			if ($unidade_info) {
				$this->error['warning'] = $this->language->get('error_exists');
			}
		} else {
			if ($unidade_info && ($this->request->get['unidade_id'] != $unidade_info['unidade_id'])) {
				$this->error['warning'] = $this->language->get('error_exists');
			}
		}
		//end validation

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}

	public function delete()
	{
		$this->load->language('customer/unidades');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('customer/unidades');

		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $unidade_id) {
				$this->model_customer_unidades->deleteUnidade($unidade_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_nome'])) {
				$url .= '&filter_nome=' . urlencode(html_entity_decode($this->request->get['filter_nome'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_prefixo'])) {
				$url .= '&filter_prefixo=' . urlencode(html_entity_decode($this->request->get['filter_prefixo'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_date_created'])) {
				$url .= '&filter_date_created=' . $this->request->get['filter_date_created'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('customer/unidades', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getList();
	}

	public function codigosGenerate()
	{
		$this->load->language('customer/codigos');
		$this->load->model('customer/unidades');

		$unidade_id = $this->request->get['unidade_id'];
		$unidade_info = $this->model_customer_unidades->getUnidade($unidade_id);
		
		$data['unidade_id'] = $this->request->get['unidade_id'];
		$data['unidade_nome'] = $unidade_info['nome'];
		$data['unidade_prefixo'] = $unidade_info['prefixo'];


		$numbers = array();

		for ($i = 1; $i <= $this->request->post['quantidade']; $i++) {
			$numbers[] = $unidade_info['prefixo'].mt_rand(100000000, 999999999);
		}
		
		// funcao para adicionar os numeros
		$this->model_customer_unidades->addCodigos($numbers, $unidade_id);
	}

	public function inn_del_only_customer()
	{
		$this->load->language('customer/codigos');
		$this->load->model('customer/unidades');

		$unidade_id = $this->request->get['unidade_id'];
		$unidade_info = $this->model_customer_unidades->getUnidade($unidade_id);
		
		$data['unidade_id'] = $this->request->get['unidade_id'];
		$data['unidade_nome'] = $unidade_info['nome'];
		$data['unidade_prefixo'] = $unidade_info['prefixo'];

		// funcao para adicionar os numeros
		$this->model_customer_unidades->delete_only_customers($unidade_id);

		$this->getList();
	}

	public function inn_block_only_customer()
	{
		$this->load->language('customer/codigos');
		$this->load->model('customer/unidades');

		$unidade_id = $this->request->get['unidade_id'];
		$unidade_info = $this->model_customer_unidades->getUnidade($unidade_id);
		
		$data['unidade_id'] = $this->request->get['unidade_id'];
		$data['unidade_nome'] = $unidade_info['nome'];
		$data['unidade_prefixo'] = $unidade_info['prefixo'];

		// funcao para adicionar os numeros
		$this->model_customer_unidades->block_only_customers($unidade_id);

		$this->getList();
	}

	public function delete_customer_orders()
	{
		$this->load->language('customer/codigos');
		$this->load->model('customer/unidades');

		$unidade_id = $this->request->get['unidade_id'];
		$customer_id = $this->request->get['customer_id'];
		$unidade_info = $this->model_customer_unidades->getUnidade($unidade_id);
		
		$data['unidade_id'] = $this->request->get['unidade_id'];
		$data['unidade_nome'] = $unidade_info['nome'];
		$data['unidade_prefixo'] = $unidade_info['prefixo'];

		// funcao para adicionar os numeros
		$this->model_customer_unidades->delete_customer_orders($unidade_id, $customer_id);

		$this->response->redirect($this->url->link('customer/unidades/codigos', 'user_token=' . $this->session->data['user_token'] . '&unidade_id='.$unidade_id . $url, true));

		// $this->getList();
	}


	public function codigos()
	{
		$this->load->language('customer/codigos');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('customer/unidades');
		$this->load->model('customer/customer');

		$unidade_id = $this->request->get['unidade_id'];
		$unidade_info = $this->model_customer_unidades->getUnidade($unidade_id);
		
		$data['unidade_id'] = $this->request->get['unidade_id'];
		$data['unidade_nome'] = $unidade_info['nome'];
		$data['unidade_prefixo'] = $unidade_info['prefixo'];

		$data['numbers_duplicados'] = $this->model_customer_unidades->verifyDuplicateCodigos($unidade_id);
		
		if (isset($this->request->get['filter_codigo'])) {
			$filter_codigo = $this->request->get['filter_codigo'];
		} else {
			$filter_codigo = '';
		}
		
		if (isset($this->request->get['filter_customer_id'])) {
			$filter_customer_id = $this->request->get['filter_customer_id'];
		} else {
			$filter_customer_id = '';
		}
		
		if (isset($this->request->get['filter_customer'])) {
			$filter_customer = $this->request->get['filter_customer'];
		} else {
			$filter_customer = '';
		}

		if (isset($this->request->get['filter_date_created'])) {
			$filter_date_created = $this->request->get['filter_date_created'];
		} else {
			$filter_date_created = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'nome';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = (int)$this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_codigo'])) {
			$url .= '&filter_codigo=' . urlencode(html_entity_decode($this->request->get['filter_codigo'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_customer_id'])) {
			$url .= '&filter_customer_id=' . urlencode(html_entity_decode($this->request->get['filter_customer_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date_created'])) {
			$url .= '&filter_date_created=' . $this->request->get['filter_date_created'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Unidades',
			'href' => $this->url->link('customer/unidades', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		$data['add'] = $this->url->link('customer/unidades/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['delete'] = $this->url->link('customer/unidades/codigo_delete', 'user_token=' . $this->session->data['user_token'] . '&unidade_id='.$unidade_id . $url, true);
		// $data['delete'] = $this->url->link('customer/unidades/delete_customers', 'user_token=' . $this->session->data['user_token'] . '&unidade_id='.$unidade_id . $url, true);
		$data['delete_all'] = $this->url->link('customer/unidades/codigo_delete_all', 'user_token=' . $this->session->data['user_token'] . '&unidade_id='.$unidade_id . $url, true);
		$data['delete_only_customers'] = $this->url->link('customer/unidades/inn_del_only_customer', 'user_token=' . $this->session->data['user_token'] . '&unidade_id='.$unidade_id . $url, true);

		$data['export_all_codes'] = $this->url->link('customer/unidades/export_all_codes', 'user_token=' . $this->session->data['user_token'] . '&unidade_id='.$unidade_id . $url, true);
		$data['export_available'] = $this->url->link('customer/unidades/export_available', 'user_token=' . $this->session->data['user_token'] . '&unidade_id='.$unidade_id . $url, true);

		$data['unidades'] = array();

		$filter_data = array(
			'unidade_id'		       => $unidade_id,
			'filter_codigo'              => $filter_codigo,
			'filter_customer_id'              => $filter_customer_id,
			'filter_customer'              => $filter_customer,
			'filter_date_created'      => $filter_date_created,
			'sort'                     => $sort,
			'order'                    => $order,
			'start'                    => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                    => $this->config->get('config_limit_admin')
		);

		$codigos_total = $this->model_customer_unidades->getTotalCodigos($filter_data);

		$data['codigos_total'] = $codigos_total;

		$data['codigos_utilizados'] = $this->model_customer_unidades->getCodigosUsed($filter_data);

		$data['codigos_disponiveis'] = $codigos_total - $data['codigos_utilizados'];

		$results = $this->model_customer_unidades->getCodigos($filter_data);

		foreach ($results as $result) {

			$custom_fields = json_decode($result['custom_field'], true);
			$getUnidadeCustomer = isset($custom_fields[3]) ? $custom_fields[3] : 'Sem Unidade';
			$name_custom_field = $this->model_customer_customer->getNameCustomField($getUnidadeCustomer);

			$getDocumentCustomer = isset($custom_fields[1]) ? $custom_fields[1] : 'Sem CPF/CNPJ';

			$data['unidades'][] = array(
				'codigo_id'           => $result['codigo_id'],
				'codigo'           => $result['codigo'],
				'customer_id'     => $result['customer_id'],	
				'customer_name'     => $result['customer_name'],
				'customer_email'     => $result['customer_email'],
				'customer_document'     => $getDocumentCustomer,
				'customer_unidade' => (isset($name_custom_field[0]['name']) ? $name_custom_field[0]['name'] : ''),
				'customer_url'           => $this->url->link('customer/customer/edit', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $result['customer_id'] . $url, true),
				'delete_customer'           => $this->url->link('customer/unidades/delete_customer_orders', 'user_token=' . $this->session->data['user_token'] . '&unidade_id='. $result['unidade_id'] . '&customer_id=' . $result['customer_id'] . $url, true),
				'unidade_nome'        => $result['unidade_nome'],
				'unidade_id'        => $result['unidade_id'],
				'date_created'   => date($this->language->get('date_format_short'), strtotime($result['date_created'])),
			);
		}

		$data['user_token'] = $this->session->data['user_token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_codigo'])) {
			$url .= '&unidade_id='.$unidade_id.'&filter_codigo=' . urlencode(html_entity_decode($this->request->get['filter_codigo'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_customer_id'])) {
			$url .= '&unidade_id='.$unidade_id.'&filter_customer_id=' . urlencode(html_entity_decode($this->request->get['filter_customer_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&unidade_id='.$unidade_id.'&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date_created'])) {
			$url .= '&unidade_id='.$unidade_id.'&filter_date_created=' . $this->request->get['filter_date_created'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_nome'] = $this->url->link('customer/unidades', 'user_token=' . $this->session->data['user_token'] . '&sort=nome' . $url, true);
		$data['sort_date_created'] = $this->url->link('customer/unidades', 'user_token=' . $this->session->data['user_token'] . '&sort=c.date_created' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_codigo'])) {
			$url .= '&unidade_id='.$unidade_id.'&filter_codigo=' . urlencode(html_entity_decode($this->request->get['filter_codigo'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_customer_id'])) {
			$url .= '&unidade_id='.$unidade_id.'&filter_customer_id=' . urlencode(html_entity_decode($this->request->get['filter_customer_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&unidade_id='.$unidade_id.'&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date_created'])) {
			$url .= '&unidade_id='.$unidade_id.'&filter_date_created=' . $this->request->get['filter_date_created'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $codigos_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('customer/unidades/codigos', 'user_token=' . $this->session->data['user_token'] . '&unidade_id=' . $unidade_id . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($codigos_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($codigos_total - $this->config->get('config_limit_admin'))) ? $codigos_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $codigos_total, ceil($codigos_total / $this->config->get('config_limit_admin')));

		$data['filter_codigo'] = $filter_codigo;
		$data['filter_customer'] = $filter_customer;
		$data['filter_date_created'] = $filter_date_created;

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('customer/codigos_list', $data));
	}

	public function delete_customers()
	{
		$unidade_id = $this->request->get['unidade_id'];

		$this->load->language('customer/unidades');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('customer/unidades');

		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $customer_id) {
				$this->model_customer_unidades->deleteCustomerOrders($customer_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_nome'])) {
				$url .= '&filter_nome=' . urlencode(html_entity_decode($this->request->get['filter_nome'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_prefixo'])) {
				$url .= '&filter_prefixo=' . urlencode(html_entity_decode($this->request->get['filter_prefixo'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_date_created'])) {
				$url .= '&filter_date_created=' . $this->request->get['filter_date_created'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('customer/unidades/codigos', 'user_token=' . $this->session->data['user_token'] . '&unidade_id='.$unidade_id . $url, true));
		}

		$this->getList();
	}
	
	public function codigo_delete()
	{
		$unidade_id = $this->request->get['unidade_id'];

		$this->load->language('customer/unidades');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('customer/unidades');

		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $codigo_id) {
				$this->model_customer_unidades->deleteCodigo($codigo_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_nome'])) {
				$url .= '&filter_nome=' . urlencode(html_entity_decode($this->request->get['filter_nome'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_prefixo'])) {
				$url .= '&filter_prefixo=' . urlencode(html_entity_decode($this->request->get['filter_prefixo'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_date_created'])) {
				$url .= '&filter_date_created=' . $this->request->get['filter_date_created'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('customer/unidades/codigos', 'user_token=' . $this->session->data['user_token'] . '&unidade_id='.$unidade_id . $url, true));
		}

		$this->getList();
	}
	
	public function codigo_delete_all()
	{
		$unidade_id = $this->request->get['unidade_id'];

		$this->load->language('customer/unidades');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('customer/unidades');

		$this->model_customer_unidades->deleteAllCodigo($unidade_id);

		$this->session->data['success'] = $this->language->get('text_success');

		$url = '';

		if (isset($this->request->get['filter_nome'])) {
		$url .= '&filter_nome=' . urlencode(html_entity_decode($this->request->get['filter_nome'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_prefixo'])) {
		$url .= '&filter_prefixo=' . urlencode(html_entity_decode($this->request->get['filter_prefixo'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date_created'])) {
		$url .= '&filter_date_created=' . $this->request->get['filter_date_created'];
		}

		if (isset($this->request->get['sort'])) {
		$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
		$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
		$url .= '&page=' . $this->request->get['page'];
		}

		$this->response->redirect($this->url->link('customer/unidades/codigos', 'user_token=' . $this->session->data['user_token'] . '&unidade_id='.$unidade_id . $url, true));
		

		$this->getList();
	}


	public function export_all_codes($getarr='') {
		$this->load->model('customer/unidades');

		$unidade_id = $this->request->get['unidade_id'];
		$info_unidade = $this->model_customer_unidades->getUnidade($unidade_id);

		$name_unidade = preg_replace('/[^aA-zZ0-9\_\-]/', '', $info_unidade['nome']);

		$data = array();

		$customers = array();

		$customers_column=array();


		$customers_column = array('#', 'Codigo', 'Aluno');

		$customers[0] = $customers_column;  

		$customers_list = $this->model_customer_unidades->exportAllCodes($data, $unidade_id);

		foreach($customers_list as $customer_row)
		{
			$customers[]=$customer_row;
		}
		require_once(DIR_SYSTEM . 'library/PHPExcel.php');
		$filename='codes_'.$name_unidade.'_todos_'.date('Y-m-d _ H:i:s');
		$filename = preg_replace('/[^aA-zZ0-9\_\-]/', '', $filename);
		// Create new PHPExcel object

		$objPHPExcel = new PHPExcel();


		$objPHPExcel->getActiveSheet()->fromArray($customers, null, 'A1');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Save Excel 95 file

		$callStartTime = microtime(true);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

		//Setting the header type
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition: attachment; filename=\"" . $filename . ".xls\"");

		header('Cache-Control: max-age=0');

		$objWriter->save('php://output');
	}


	public function export_available($getarr='') {
		$this->load->model('customer/unidades');

		$unidade_id = $this->request->get['unidade_id'];
		$info_unidade = $this->model_customer_unidades->getUnidade($unidade_id);

		$name_unidade = preg_replace('/[^aA-zZ0-9\_\-]/', '', $info_unidade['nome']);

		$data = array();

		$customers = array();

		$customers_column=array();


		$customers_column = array('#', 'Codigo', 'Aluno');

		$customers[0] = $customers_column;  

		$customers_list = $this->model_customer_unidades->exportAvailable($data, $unidade_id);

		foreach($customers_list as $customer_row)
		{
			$customers[]=$customer_row;
		}
		require_once(DIR_SYSTEM . 'library/PHPExcel.php');
		$filename='codes_'.$name_unidade.'_disponiveis_'.date('Y-m-d _ H:i:s');
		$filename = preg_replace('/[^aA-zZ0-9\_\-]/', '', $filename);
		// Create new PHPExcel object

		$objPHPExcel = new PHPExcel();


		$objPHPExcel->getActiveSheet()->fromArray($customers, null, 'A1');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Save Excel 95 file

		$callStartTime = microtime(true);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

		//Setting the header type
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition: attachment; filename=\"" . $filename . ".xls\"");

		header('Cache-Control: max-age=0');

		$objWriter->save('php://output');
	}
}
