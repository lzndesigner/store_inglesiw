<?php
class ControllerExtensionReportCustomerUnits extends Controller
{
	public function index()
	{
		$this->load->language('extension/report/customer_units');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('report_customer_units', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=report', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=report', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/report/customer_units', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/report/customer_units', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=report', true);

		if (isset($this->request->post['report_customer_units_status'])) {
			$data['report_customer_units_status'] = $this->request->post['report_customer_units_status'];
		} else {
			$data['report_customer_units_status'] = $this->config->get('report_customer_units_status');
		}

		if (isset($this->request->post['report_customer_units_sort_order'])) {
			$data['report_customer_units_sort_order'] = $this->request->post['report_customer_units_sort_order'];
		} else {
			$data['report_customer_units_sort_order'] = $this->config->get('report_customer_units_sort_order');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/report/customer_units_form', $data));
	}

	public function customers()
	{
		$this->load->language('extension/report/customer_units');

		$this->document->setTitle('Clientes da Unidade');

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('report_customer_units', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=report', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=report', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Clientes da Unidade',
			'href' => $this->url->link('extension/report/customer_units', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/report/customer_units', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=report', true);

		if (isset($this->request->post['report_customer_units_status'])) {
			$data['report_customer_units_status'] = $this->request->post['report_customer_units_status'];
		} else {
			$data['report_customer_units_status'] = $this->config->get('report_customer_units_status');
		}

		if (isset($this->request->post['report_customer_units_sort_order'])) {
			$data['report_customer_units_sort_order'] = $this->request->post['report_customer_units_sort_order'];
		} else {
			$data['report_customer_units_sort_order'] = $this->config->get('report_customer_units_sort_order');
		}

		$this->load->model('extension/report/customer_units');

		$results = $this->model_extension_report_customer_units->getCustomers($this->request->get['unit_id']);

		foreach ($results as $result) {
			$data['customers'][] = array(
				'customer_id'     => $result['customer_id'],
				'customer_name'   => $result['name'],
				'customer_orders'   => $result['orders'],
			);
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/report/customer_units_customers', $data));
	}

	public function orders()
	{
		$this->load->language('extension/report/customer_units');

		$this->document->setTitle('Clientes da Unidade');

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('report_customer_units', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=report', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = null;
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = date('Y-m-d');
		}

		if (isset($this->request->get['filter_group'])) {
			$filter_group = $this->request->get['filter_group'];
		} else {
			$filter_group = 'week';
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$filter_order_status_id = $this->request->get['filter_order_status_id'];
		} else {
			$filter_order_status_id = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = (int)$this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=report', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Clientes da Unidade',
			'href' => $this->url->link('extension/report/customer_units', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/report/customer_units', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=report', true);

		if (isset($this->request->post['report_customer_units_status'])) {
			$data['report_customer_units_status'] = $this->request->post['report_customer_units_status'];
		} else {
			$data['report_customer_units_status'] = $this->config->get('report_customer_units_status');
		}

		if (isset($this->request->post['report_customer_units_sort_order'])) {
			$data['report_customer_units_sort_order'] = $this->request->post['report_customer_units_sort_order'];
		} else {
			$data['report_customer_units_sort_order'] = $this->config->get('report_customer_units_sort_order');
		}

		$this->load->model('extension/report/customer_units');

		$filter_data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_group'           => $filter_group,
			'filter_order_status_id' => $filter_order_status_id,
			'start'                  => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                  => $this->config->get('config_limit_admin')
		);

		if (isset($this->request->get['unit_id'])) {
			$unit_id = $this->request->get['unit_id'];
		} else {
			$unit_id = '';
		}

		$results = $this->model_extension_report_customer_units->getOrdersUnits($unit_id, $filter_data);

		foreach ($results as $result) {
			$data['orders'][] = array(
				'date_added'     => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'customer_name'   => $result['customer_name'],
				'products'   => $result['products'],
				'payment_method'   => $result['payment_method'],
				'total'   => $result['total'],
				'unit_name'   => $result['unit_name'],
			);
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/report/customer_units_orders', $data));
	}

	protected function validate()
	{
		if (!$this->user->hasPermission('modify', 'extension/report/customer_units')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function report()
	{
		$this->load->language('extension/report/customer_units');

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = date('Y-m-d');
		}

		if (isset($this->request->get['filter_group'])) {
			$filter_group = $this->request->get['filter_group'];
		} else {
			$filter_group = 'week';
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$filter_order_status_id = $this->request->get['filter_order_status_id'];
		} else {
			$filter_order_status_id = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = (int)$this->request->get['page'];
		} else {
			$page = 1;
		}

		$this->load->model('extension/report/customer_units');

		$data['orders'] = array();

		$filter_data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_group'           => $filter_group,
			'filter_order_status_id' => $filter_order_status_id,
			'start'                  => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                  => $this->config->get('config_limit_admin')
		);

		$order_total = $this->model_extension_report_customer_units->getTotalUnits($filter_data);

		$results = $this->model_extension_report_customer_units->getUnits($filter_data);

		$data['export_all_orders'] = $this->url->link('extension/report/customer_units/ordersExports', 'user_token=' . $this->session->data['user_token'], true);

		foreach ($results as $result) {
			$data['orders'][] = array(
				'unit_id'     => ($result['unit_id'] ? $result['unit_id'] : '0'),
				'unit_name'   => ($result['unit_name'] ? $result['unit_name'] : '-'),
				'unit_href'   => $this->url->link('extension/report/customer_units/customers', 'user_token=' . $this->session->data['user_token'] . '&unit_id=' . ($result['unit_id'] ? $result['unit_id'] : '0'), true),
				'unit_href_orders'   => $this->url->link('extension/report/customer_units/orders', 'user_token=' . $this->session->data['user_token'] . '&unit_id=' . ($result['unit_id'] ? $result['unit_id'] : '0'), true),
				'unit_export'   => $this->url->link('extension/report/customer_units/export', 'user_token=' . $this->session->data['user_token'] . '&unit_id=' . ($result['unit_id'] ? $result['unit_id'] : '0'), true),
			);
		}

		$data['user_token'] = $this->session->data['user_token'];

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$data['groups'] = array();

		$data['groups'][] = array(
			'text'  => $this->language->get('text_year'),
			'value' => 'year',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_month'),
			'value' => 'month',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_week'),
			'value' => 'week',
		);

		$data['groups'][] = array(
			'text'  => $this->language->get('text_day'),
			'value' => 'day',
		);

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

		$pagination = new Pagination();
		$pagination->total = $order_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/report', 'user_token=' . $this->session->data['user_token'] . '&code=customer_units' . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));

		$data['filter_date_start'] = $filter_date_start;
		$data['filter_date_end'] = $filter_date_end;
		$data['filter_date_start_formated'] = (new DateTime($filter_date_start))->format('d/m/Y');
		$data['filter_date_end_formated'] = (new DateTime($filter_date_end))->format('d/m/Y');
		$data['filter_group'] = $filter_group;
		$data['filter_order_status_id'] = $filter_order_status_id;

		return $this->load->view('extension/report/customer_units_info', $data);
	}

	public function export($getarr = '')
	{
		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start_formated = DateTime::createFromFormat('d/m/Y', $this->request->get['filter_date_start'])->format('Y-m-d');
			$filter_date_start = $filter_date_start_formated;
		} else {
			$filter_date_start = null;
		}
		
		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end_formated = DateTime::createFromFormat('d/m/Y', $this->request->get['filter_date_end'])->format('Y-m-d');
			$filter_date_end = $filter_date_end_formated;
		} else {
			$filter_date_end = null;
		}

		$filter_data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
		);

		$customers = array();

		$customers_column = array();

		$this->load->model('extension/report/customer_units');

		$unit_info = $this->model_extension_report_customer_units->getUnit($this->request->get['unit_id']);
		$unit_columns = array('ID', 'Nome da Unidade', '', '', '');
		$customers[0] = $unit_columns;
		$customers[1] = [
			$unit_info['custom_field_value_id'],
			$unit_info['name'],
			'',
			'',
			'',
		];
		$customers[2] = [
			'',
			'',
			'',
			'',
			'',
		];

		$customers_column = array('ID', 'Nome do Cliente', '', '', '');
		$customers[3] = $customers_column;

		$customers_list = $this->model_extension_report_customer_units->getCustomers($this->request->get['unit_id'], $filter_data);


		foreach ($customers_list as $customer_row) {
			$customers[] = [
				$customer_row['customer_id'],
				$customer_row['name'],
				'',
				'',
				'',
			];

			$customers[] = [
				'Pedido ID',
				'Produtos',
				'Data',
				'Método',
				'Total',
			];

			if (isset($customer_row['orders'])) {
				foreach ($customer_row['orders'] as $order) {
					$products = [];
					if (isset($order['order_products'])) {
						foreach ($order['order_products'] as $product) {
							// var_dump($product);exit;
							$products[] = $product['product_name'] . ' (' . $product['product_quantity'] . 'x ' . $product['product_price'] . ') Total: ' . $product['product_total'] . '';
						}
					}
					$customers[] = [
						$order['order_id'],
						implode('; ', $products),
						date($this->language->get('date_format_short'), strtotime($order['order_date_added'])),
						$order['order_payment_method'],
						$order['order_total'],
					];
				}
			}

			$customers[] = [
				'',
				'',
				'',
				'',
				'',
			];

			$customers[] = [
				'---------',
				'---------',
				'---------',
				'---------',
				'---------',
			];
		}
		require_once(DIR_SYSTEM . 'library/PHPExcel.php');
		$filename = 'report_' . $unit_info['name'] . '_' . date('Y-m-d _ H:i:s');
		$filename = preg_replace('/[^aA-zZ0-9\_\-]/', '', $filename);
		// Create new PHPExcel object

		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getActiveSheet()->fromArray($customers, null, 'A1');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Save Excel 95 file

		$callStartTime = microtime(true);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

		//Setting the header type
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition: attachment; filename=\"" . $filename . ".xls\"");

		header('Cache-Control: max-age=0');

		$objWriter->save('php://output');
	}

	public function ordersExports($getarr = '')
	{		
		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start_formated = DateTime::createFromFormat('d/m/Y', $this->request->get['filter_date_start'])->format('Y-m-d');
			$filter_date_start = $filter_date_start_formated;
		} else {
			$filter_date_start = null;
		}
		
		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end_formated = DateTime::createFromFormat('d/m/Y', $this->request->get['filter_date_end'])->format('Y-m-d');
			$filter_date_end = $filter_date_end_formated;
		} else {
			$filter_date_end = null;
		}

		$filter_data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
		);

		$customers = array();

		$customers_column = array();

		$this->load->model('extension/report/customer_units');

		$customers[0] = [
			'Data',
			'Unidade',
			'Nome do Aluno',
			'Produtos',
			'Método',
			'Total',
			'Parcelas',
		];

		
		$orders_list = $this->model_extension_report_customer_units->getOrdersUnits($filter_data);

		foreach ($orders_list as $customer_row) {
			// var_dump($customer_row);exit;
			$products = [];
			if (isset($customer_row['products'])) {
				foreach ($customer_row['products'] as $product) {
					// var_dump($product);exit;
					$products[] = '(' . $product['product_quantity'] . ') ' . $product['product_name'];
				}
			}
			$comments = [];
			if (isset($customer_row['parcelas'])) {
				foreach ($customer_row['parcelas'] as $comment) {
					$comments[] = $comment;
				}
			}
			
			$customers[] = [
				$customer_row['date_added'],
				$customer_row['unit_name'],
				$customer_row['customer_name'],
				implode('; ', $products),
				$customer_row['payment_method'],
				$customer_row['total'],
				implode('; ', $comments),
			];

			// var_dump($customers);exit;

			// $customers[] = [
			// 	'',
			// 	'',
			// 	'',
			// 	'',
			// 	'',
			// ];

			// $customers[] = [
			// 	'---------',
			// 	'---------',
			// 	'---------',
			// 	'---------',
			// 	'---------',
			// ];
			// var_dump($customers);exit;
		}

		require_once(DIR_SYSTEM . 'library/PHPExcel.php');
		$filename = 'report_total_' . date('Y-m-d _ H:i:s');
		$filename = preg_replace('/[^aA-zZ0-9\_\-]/', '', $filename);
		// Create new PHPExcel object

		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getActiveSheet()->fromArray($customers, null, 'A1');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Save Excel 95 file

		$callStartTime = microtime(true);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

		//Setting the header type
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition: attachment; filename=\"" . $filename . ".xls\"");

		header('Cache-Control: max-age=0');

		$objWriter->save('php://output');
	}
}
