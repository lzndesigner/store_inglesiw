<?php
// Heading
$_['heading_title']             = 'Códigos';

// Text
$_['text_success']              = 'Unidade modificado com sucesso!';
$_['text_list']                 = 'Listando codigos';
$_['text_add']                  = 'Novo codigo';
$_['button_generate']             = 'Gerar codigos';
$_['text_edit']                 = 'Editando codigo';
$_['text_default']              = 'Padrão';
$_['text_account']              = 'Informações do codigo';
$_['text_password']             = 'Senha';
$_['text_other']                = 'Outros';
$_['text_affiliate']            = 'Informações da afiliação';
$_['text_payment']              = 'Informações para pagamento';
$_['text_balance']              = 'Saldo';
$_['text_cheque']               = 'Cheque';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Transferência bancária';
$_['text_history']              = 'Histórico do codigo';
$_['text_history_add']          = 'Adicionar histórico';
$_['text_transaction']          = 'Histórico de transações';
$_['text_transaction_add']      = 'Adicionar transação';
$_['text_reward']               = 'Histórico de pontos';
$_['text_reward_add']           = 'Adicionar pontos';
$_['text_ip']                   = 'Histórico do IP';
$_['text_option']               = 'Opções';
$_['text_login']                = 'Acessar a loja com essa conta';
$_['text_unlock']               = 'Desbloquear a conta';
$_['text_confirm_all']          = 'Você tem certeza que deseja excluir os códigos não utilizados?';
$_['button_delete_all']          = 'Excluir códigos não utilizados';

// Column
$_['column_name']               = 'Unidade';
$_['column_email']              = 'E-mail';
$_['column_customer_group']     = 'Tipo de codigo';
$_['column_status']             = 'Situação';
$_['column_date_created']         = 'Cadastro';
$_['column_comment']            = 'Comentário';
$_['column_description']        = 'Detalhes';
$_['column_amount']             = 'Valor';
$_['column_points']             = 'Pontos';
$_['column_ip']                 = 'Endereço IP';
$_['column_total']              = 'Contas registradas';
$_['column_action']             = 'Ação';

// Entry
$_['entry_codigo']                = 'Codigo';
$_['entry_customer_id']           = 'Cliente ID';
$_['entry_customer']              = 'Status';
$_['entry_prefixo']             = 'Prefixo';
$_['entry_date_created']          = 'Cadastro';
$_['entry_quantidade']          = 'Quantidade de Códigos';

// Help
$_['help_safe']                 = 'Defina como Habilitado para evitar que este codigo seja pego pelo sistema anti-fraude';
$_['help_affiliate']            = 'Habilitar / Desabilitar a capacidade do codigo de usar o sistema de afiliados';
$_['help_tracking']             = 'Código de rastreamento que será utilizado para rastrear as indicações do afiliado';
$_['help_commission']           = 'Percentual que o afiliado recebe a cada pedido indicado e pago';
$_['help_points']               = 'Use o sinal de menos para remover pontos';

// Error
$_['error_warning']             = 'Atenção: Faltou preencher alguma informação, verifique todos os campos.';
$_['error_permission']          = 'Atenção: Você não tem permissão para modificar os codigos!';
$_['error_exists']              = 'Atenção: O e-mail já está cadastrado!';
$_['error_nome']                = 'O nome deve ter entre 1 e 32 caracteres!';
$_['error_prefixo']             = 'O prefixo deve ter entre 1 e 32 caracteres!';