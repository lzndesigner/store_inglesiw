<?php
// Heading
$_['heading_title']      = 'Pagamento Gratuito - Projeto Social';

// Text
$_['text_extension']     = 'Extensões';
$_['text_success']       = 'Pagamento Gratuito modificado com sucesso!';
$_['text_edit']          = 'Editando Pagamento Gratuito';

// Entry
$_['entry_order_status'] = 'Situação do pedido';
$_['entry_status']       = 'Situação';
$_['entry_sort_order']   = 'Posição';

// Error
$_['error_permission']   = 'Atenção: Você não tem permissão para modificar a extensão Pagamento Gratuito!';