<?php
// Heading
$_['heading_title']  = 'Painel de Administração';

// Text
$_['text_heading']   = 'Painel de Administração';
$_['text_login']     = 'Área Restrita';
$_['text_forgotten'] = 'Esqueceu sua senha?';

// Entry
$_['entry_username'] = 'Usuário';
$_['entry_password'] = 'Senha';

// Button
$_['button_login']   = 'Acessar';

// Error
$_['error_login']    = 'Os dados de acesso não são válidos.';
$_['error_token']    = 'O token de sessão é inválido. Tente acessar novamente.';