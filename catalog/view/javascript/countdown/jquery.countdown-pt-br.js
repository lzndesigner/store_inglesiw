/*
***************************************************************************
  T O D O S   O S   D I R E I T O S   R E S E R V A D O S       -      C O P Y R I G H T 2 0 1 7 ©
***************************************************************************


* Servidor: Mu Seven Games;
* Versão: 1.05 Season 4;

* Empresa: Seven Games - Servidor de Mu Online;
* URL: http://www.musevengames.com;
* E-mail: contato@musevengames.com;
* Autor: Leonardo Augusto & Vinicius Neses;

*/
/* http://keith-wood.name/countdown.html
   Brazilian initialisation for the jQuery countdown extension
   Translated by Marcelo Pellicano de Oliveira (pellicano@gmail.com) Feb 2008.
   and Juan Roldan (juan.roldan[at]relayweb.com.br) Mar 2012. */
(function($) {
	'use strict';
	$.countdown.regionalOptions['pt-BR'] = {
		labels: ['Anos','Meses','Semanas','Dias','Horas','Minutos','Segundos'],
		labels1: ['Ano','Mês','Semana','Dia','Hora','Minuto','Segundo'],
		compactLabels: ['a','m','s','d'],
		whichLabels: null,
		digits: ['0','1','2','3','4','5','6','7','8','9'],
		timeSeparator: ':',
		isRTL: false
	};
	$.countdown.setDefaults($.countdown.regionalOptions['pt-BR']);
})(jQuery);
