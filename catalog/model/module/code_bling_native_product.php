<?php

/**
 * © Copyright 2013-2025 Codemarket - Todos os direitos reservados.
 * Model nativo do Bling, que ficava no product passado para este model e feita algumas melhorias
 * Class ModelModuleCodeBlingNativeProduct
 */
class ModelModuleCodeBlingNativeProduct extends Model
{
    private $conf;
    private $log;

    /**
     * ModelModuleCodeBlingNativeProduct constructor.
     *
     * @param $registry
     */
    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->load->model('module/code_bling');
        $this->load->model('localisation/language');
        $this->load->model('module/codemarket_module');
        $conf = $this->model_module_codemarket_module->getModulo('548');
        $this->log = new log('Code-Bling-' . date('m-Y') . '.log');

        if (
            empty($conf) || empty($conf->code_habilitar)
            || (int) $conf->code_habilitar === 2
            || empty($conf->code_payment_id)
            || empty($conf->code_chave)
            || empty($conf->code_sku_opcao)
        ) {
            $this->log->write(
                'ModelModuleCodeBlingNativeProduct_construct() - Bling desativado, verifique a configuração'
            );
            exit('Desativado ou sem configuração');
        }

        $this->conf = $conf;

        return true;
    }

    /**
     * Retorna todos os Produtos
     *
     * @return mixed
     */
    public function getAllProduct()
    {
        $query = $this->db->query(
            "SELECT DISTINCT(p.product_id), pd.name, pd.description, p.model, p.sku, p.quantity, p.price, p.weight, p.length, p.width, p.height,p.date_added
        FROM " . DB_PREFIX . "product p LEFT JOIN  " . DB_PREFIX . "product_description pd ON pd.product_id = p.product_id
        ORDER BY p.date_added DESC
        "
        );

        $this->log->write(
            'ModelModuleCodeBlingNativeProduct_getAllProduct() - Retornando todos os Produtos'
        );

        return $query->rows;
    }

    /**
     * Retorna os Produtos pelo Filtro
     *
     * @param $filters
     *
     * @return mixed
     */
    public function getAllProductFilters($filters)
    {
        $filters = urldecode($filters);
        $filter = explode('|', $filters);
        $startDate = $filter[0];
        $finishDate = $filter[1];

        if ($startDate == date('Y-m-d')) {
            $d = date('d') + 1;
            $y = date('Y');
            $m = date('m');
            $finishDate = $y . "-" . $m . "-" . $d;
        }

        $this->log->write(
            'ModelModuleCodeBlingNativeProduct_getAllProductFilters() - Data inicial: '
            . $startDate . ', data final: ' . $finishDate
        );

        // Verifica se é para Exportar produto Desativado também
        if (empty($this->conf->code_exportar_desativados)
            || $this->conf->code_exportar_desativados == 2
        ) {
            $query = $this->db->query(
                "SELECT DISTINCT(pd.product_id), pd.name, pd.description, p.model, p.sku, p.quantity, p.price, p.weight, p.length, p.width, p.height,p.date_added
            FROM " . DB_PREFIX . "product p LEFT JOIN  " . DB_PREFIX . "product_description pd ON pd.product_id = p.product_id
            WHERE p.date_added BETWEEN '" . $startDate . "' AND '" . $finishDate . "'  AND p.status = '1'
            GROUP BY p.product_id"
            );
        } else {
            $query = $this->db->query(
                "SELECT DISTINCT(pd.product_id), pd.name, pd.description, p.model, p.sku, p.quantity, p.price, p.weight, p.length, p.width, p.height,p.date_added
            FROM " . DB_PREFIX . "product p LEFT JOIN  " . DB_PREFIX . "product_description pd ON pd.product_id = p.product_id
            WHERE p.date_added BETWEEN '" . $startDate . "' AND '" . $finishDate . "'
            GROUP BY p.product_id"
            );
        }

        $this->log->write(
            'ModelModuleCodeBlingNativeProduct_getAllProductFilters() - Retornando Produtos, total: '
            . count($query->rows)
        );

        return $query->rows;
    }

    //Products by filters
    public function getCountProduct()
    {
        $query = $this->db->query(
            "SELECT COUNT(product_id) as NrProducts FROM " . DB_PREFIX . "product "
        );

        return $query->rows;
    }

    //Insert products
    public function insert_oc_products($parameter)
    {
        $sku = strip_tags($parameter->codigo);

        //Retorna o Id do Produto ou da Variação
        $data = $this->getProductIdorOption($sku);
        $this->log->write(
            'ModelModuleCodeBlingNativeProduct_insert_oc_products() - Se já existe - Retornando ID do Produto ou Opção: '
            . print_r($data, true)
        );

        //Consultar pelo Código
        $productBling = $this->model_module_code_bling->getProductBling($sku, '', false);
        $blingProduct = true;
        //$this->log->write('ModelModuleCodeBlingNativeProduct_insert_oc_products() - Produto retornado: ' . print_r($productBling, true));

        //Verificando se é Produto
        if (!empty($data['product_option_value_id'])) {
            //É variação/opção, só retorna com o ID de volta para o Bling ERP
            $this->log->write(
                'ModelModuleCodeBlingNativeProduct_insert_oc_products() - Produto Variação/Opção'
            );
            $option['option_id'] = $data['product_id'] . 'v'
                . $data['product_option_value_id'];;

            return $option;
        } else {
            if (!empty($data['product_id'])) {
                $idProd = $data['product_id'];
            } else {
                if (!empty($productBling['variacao']['produtoPai']['id'])) {
                    //Verificando se é Variação/Opção
                    $this->log->write(
                        'ModelModuleCodeBlingNativeProduct_insert_oc_products() - Produto Variação/Opção 2'
                    );
                    //$this->log->write('ModelModuleCodeBlingNativeProduct_insert_oc_products() - Produto retornado: ' . print_r($productBling, true));
                    //Salvar variação/opção
                    //Importante na Loja já ter Opção com mesmo Nome e apenas do tipo Seleção única ou Multipla seleção

                    //Verificando se é para Cadastrar a Variação como Opção do Produto Pai
                    if (!empty($this->conf->code_importar_produto)
                        && $this->conf->code_importar_produto == 3
                    ) {
                        $this->log->write(
                            'ModelModuleCodeBlingNativeProduct_insert_oc_products() - Configurado para importar a Variação como Opção do Produto Pai'
                        );

                        // Encontrar o Código/SKU do Produto Pai
                        $productFatherBling = $this->model_module_code_bling->getProductBling(
                            '',
                            $productBling['variacao']['produtoPai']['id'],
                            true
                        );
                        if (empty($productFatherBling['codigo'])) {
                            $this->log->write(
                                'ModelModuleCodeBlingNativeProduct_insert_oc_products() - Não foi encontrado o Produto Pai ID ' . $productBling['variacao']['produtoPai']['id']
                            );

                            return false;
                        }

                        //Verificando se tem o produto Pai cadastrado
                        $productFatherID = $this->getProductIdorOption(
                            $productFatherBling['codigo']
                        );

                        if (!empty($productFatherID['product_id'])) {
                            return $this->model_module_code_bling->addOptionsProduct(
                                $productBling,
                                $productFatherID['product_id']
                            );
                        } else {
                            $this->log->write(
                                'ModelModuleCodeBlingNativeProduct_insert_oc_products() - Sem o ID do Produto Pai'
                            );
                        }
                    }

                    return false;
                } else {
                    if (empty($productBling)) {
                        //Sem retorno no Bling, só avisar para desconsiderar
                        $this->log->write(
                            'ModelModuleCodeBlingNativeProduct_insert_oc_products() - Sem retorno na consulta do Bling'
                        );
                        $blingProduct = false;
                    } else {
                        //Produto normal ou com variações
                        $this->log->write(
                            'ModelModuleCodeBlingNativeProduct_insert_oc_products() - Produto Novo'
                        );
                        $idProd = 0;
                    }
                }
            }
        }

        //Não é para Criar ou Editar um Produto, Bling não retornou um Produto
        if (empty($blingProduct)) {
            $option['option'] = true;

            return $option;
        }

        //Verificar se não é para Cadastrar Produto Variação como Produto normal
        if (!empty($option) && !empty($this->conf->code_importar_produto)
            && $this->conf->code_importar_produto == 1
        ) {
            $this->log->write(
                'ModelModuleCodeBlingNativeProduct_insert_oc_products() - Configurado para não Importar Produto Variação'
            );

            return $option;
        }

        if (empty($idProd)) {
            $length_class_id = !empty($this->conf->code_length_cm)
                ? (int) $this->conf->code_length_cm : 1;
            $weight_class_id = !empty($this->conf->code_weight_kg)
                ? (int) $this->conf->code_weight_kg : 1;

            $sql = $this->db->query(
                "INSERT INTO " . DB_PREFIX . "product (model, sku, upc, ean, jan, isbn, mpn, location, quantity, stock_status_id, image, manufacturer_id, shipping, price, points, tax_class_id, date_available, weight, weight_class_id, length, width, height, length_class_id, subtract, minimum, sort_order, status, viewed, date_added, date_modified )
            VALUES ('" . $sku . "', '" . $sku . "','','','','','','','"
                . $parameter->estoqueAtual . "','1','','0','1','" . $parameter->preco
                . "','0', '0','" . date('Y-m-d') . "', '" . $parameter->peso . "', '"
                . (int) $weight_class_id . "','" . $parameter->profundidadeProduto
                . "','" . $parameter->larguraProduto . "','"
                . $parameter->alturaProduto . "', '" . (int) $length_class_id
                . "','1','1','0','1','0', NOW(), NOW())"
            );

            $query = $this->db->query(
                "SELECT model, sku, quantity, MAX(product_id) as maximo FROM `"
                . DB_PREFIX . "product` ORDER BY product_id DESC Limit 1"
            );
            $this->log->write(
                'ModelModuleCodeBlingNativeProduct_insert_oc_products() - Adicionado Produto, retorno '
                . print_r($query->rows, true)
            );

            return $query->rows;
        } else {
            $sql = $this->db->query(
                "UPDATE " . DB_PREFIX . "product SET    model = '" . $sku . "', sku = '"
                . $sku . "', quantity = '" . $parameter->estoqueAtual . "', price = '"
                . $parameter->preco . "', weight = '" . $parameter->peso
                . "', length = '" . $parameter->profundidadeProduto . "', width = '"
                . $parameter->larguraProduto . "', height = '"
                . $parameter->alturaProduto
                . "', date_modified = NOW()  WHERE product_id = '" . $idProd . "'"
            );

            $this->log->write(
                'ModelModuleCodeBlingNativeProduct_insert_oc_products() - Atualizado Produto: '
                . $idProd
            );

            return ['id' => $idProd, 'returnUp' => $sql];
        }
    }

    /**
     * Retorna o ID do Produto ou da Opção e Produto com base no Código/SKU
     *
     * @param $sku
     *
     * @return array|false
     */
    public function getProductIdorOption($sku)
    {
        //$this->log->write('ModelModuleCodeBlingNativeProduct_getProductIdorOption() - Código/SKU : ' . $sku);
        $query = $this->db->query(
            "SELECT product_id FROM " . DB_PREFIX . "product WHERE sku = '"
            . $this->db->escape($sku) . "'"
        );

        if (!empty($query->row['product_id'])) {
            $data['product_id'] = $query->row['product_id'];

            return $data;
        }

        $queryOption = $this->db->query(
            "SELECT product_id, product_option_value_id FROM " . DB_PREFIX
            . "product_option_value WHERE " . $this->conf->code_sku_opcao . " = '"
            . $this->db->escape($sku) . "'"
        );
        if (!empty($queryOption->row['product_option_value_id'])) {
            $data['product_option_value_id']
                = $queryOption->row['product_option_value_id'];
            $data['product_id'] = $queryOption->row['product_id'];

            return $data;
        }

        return false;
    }

    /**
     * Atualiza a descrição
     *
     * @param $parameter
     * @param $id
     *
     * @return mixed
     */
    public function update_oc_description($parameter, $id)
    {
        $sql = $this->db->query(
            "UPDATE " . DB_PREFIX . "product_description SET `name` =  '"
            . strip_tags($parameter->nome) . "', `description` = '" . htmlentities(
                $parameter->descricaoComplementar
            ) . "' WHERE `product_id` = '" . $id . "'"
        );

        return $sql;
    }

    /**
     * Adicionado uma descrição
     *
     * @param $parameter
     * @param $id
     *
     * @return mixed
     */
    public function insert_oc_description($parameter, $id)
    {
        $languages = $this->model_localisation_language->getLanguages();

        foreach ($languages as $l) {
            $sql = $this->db->query(
                "INSERT INTO " . DB_PREFIX . "product_description (`product_id`, `language_id`, `name`, `description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`)
        VALUES('" . $id . "','" . (int) $l['language_id'] . "','" . strip_tags(
                    $parameter->nome
                ) . "','" . htmlentities($parameter->descricaoComplementar)
                . "','','','','')"
            );
        }

        $query = $this->db->query(
            "SELECT MAX(product_id) as idMax FROM " . DB_PREFIX
            . "product_description"
        );

        return $query->rows;
    }

    /**
     * Remove um Produto
     *
     * @param $id
     *
     * @return bool
     */
    public function delete_oc_products($id)
    {
        $this->log->write(
            'ModelModuleCodeBlingNativeProduct_delete_oc_products() - Deletando o Produto: '
            . $id
        );

        $del = $this->db->query(
            "DELETE FROM `" . DB_PREFIX . "product` WHERE product_id = '" . $id . "'"
        );

        return true;
    }

    /**
     * Retornando as Opções
     *
     * @param $parameters
     *
     * @return mixed
     */
    public function getVariation($parameters)
    {
        $language_id = $this->model_module_code_bling->languageId();

        $sku = 'pov.' . $this->conf->code_sku_opcao;
        $query = $this->db->query(
            "SELECT DISTINCT(" . $sku . ") AS sku, pd.name as variationName, od.name as nomeTipoVariacao,  ovd.name as tipoVariacao, pov.quantity as quantidadeVariacao, pov.price as precoVaricao, pov.price_prefix as prefixPrecoVaricao,  pov.weight as   pesoVaricao, pov.weight_prefix as prefixPesoVaricao, CONCAT(pd.product_id,'v', pov.product_option_value_id) as idVariation
            FROM " . DB_PREFIX . "option_description od
            LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON ( od.option_id = ovd.option_id )
            LEFT JOIN " . DB_PREFIX . "product_option_value pov ON ( ovd.option_value_id = pov.option_value_id )
            LEFT JOIN " . DB_PREFIX . "product_description pd ON ( pov.product_id = pd.product_id )
            WHERE pd.product_id = '" . $parameters['product_id'] . "' AND
            pd.language_id = '" . (int) $language_id . "' AND
            od.language_id = '" . (int) $language_id . "' AND
            ovd.language_id = '" . (int) $language_id . "'
        "
        );

        return $query->rows;
    }

    /**
     * Alterando o Estoque do Produto
     *
     * @param $id
     * @param $qtd
     *
     * @return bool
     */
    public function update_stock_product($id, $qtd)
    {
        $update = $this->db->query(
            "UPDATE `" . DB_PREFIX . "product` SET `quantity`= '" . (int) $qtd
            . "' WHERE  `product_id` = '" . (int) $id . "'"
        );

        $this->log->write(
            'ModelModuleCodeBlingNativeProduct_update_stock_product() - Alterado o estoque do Produto: '
            . $id . ' Qtd: ' . $qtd
        );

        if ($update) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Alterando o Estoque das Opções com base no ID e Quantidade
     *
     * @param $id
     * @param $qtd
     *
     * @return bool
     */
    public function update_stock_variation($id, $qtd)
    {
        $idOption = explode('v', $id);
        if (!empty($idOption[1])) {
            $id = $idOption[1];
        }

        $update = $this->db->query(
            "UPDATE `" . DB_PREFIX . "product_option_value` SET `quantity`= '" . (int) $qtd
            . "' WHERE  `product_option_value_id` = '" . (int) $id . "' "
        );
        $this->log->write(
            'ModelModuleCodeBlingNativeProduct_update_stock_variation() - Alterado o estoque da Opção: '
            . $id . ' Qtd: ' . $qtd
        );

        if ($update) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Atualizando o Preço do Produto
     *
     * @param $product_id ID do Produto na Loja
     *
     * @return bool
     * @uses \ModelModuleCodeBling::getProductBling() Retorna o Produto do Bling sem uso de cache
     * @uses \ModelModuleCodeBling::addPriceProductStoreSQL() Muda o Preço na Loja
     */
    public function update_price_product($product_id)
    {
        $this->log->write(
            'ModelModuleCodeBlingNativeProduct_update_price_product() - Produto ID: '
            . $product_id
        );

        $query = $this->db->query(
            "SELECT sku FROM " . DB_PREFIX . "product WHERE product_id = '"
            . (int) $product_id . "' LIMIT 1"
        );

        if (empty($query->row['sku'])) {
            $this->log->write(
                'ModelModuleCodeBlingNativeProduct_update_price_product() - Sem SKU, Produto ID: '
                . $product_id
            );

            return false;
        }

        // Não usa o Cache para puxar os dados mais atuais do Bling
        $productBling = $this->model_module_code_bling->getProductBling(
            $query->row['sku'],
            '',
            false
        );

        if (empty($productBling['codigo'])) {
            $this->log->write(
                'ModelModuleCodeBlingNativeProduct_update_price_product() - Retorno sem o Código - Produto ID: '
                . $product_id
            );

            return false;
        }

        /*
        $this->log->write(
            'ModelModuleCodeBlingNativeProduct_update_price_product() - Dados retorno - Produto ID: '
            .$product_id.' '.print_r($productBling, true)
        );
        */

        // Mudando o Preço e Promoção do Produto
        return $this->model_module_code_bling->addPriceProductStoreSQL(
            $productBling,
            $product_id
        );
    }

    /**
     * Atualizando o Preço do Produto
     *
     * @param $id ID da Opção informado para o Bling na relação com a Loja, composto pelo ID da opção e do seu valor
     * @param $price Preço da Opção
     *
     * @return bool
     */
    public function update_price_variation($id, $price)
    {
        //$this->log->write('ModelModuleCodeBlingNativeProduct_update_stock_variation() - ID: '.print_r($id, true));

        $idOption = explode('v', $id);
        if (!empty($idOption[1])) {
            $id = $idOption[1];
        }

        // Consultado o valor da opção
        $product_option_value = $this->db->query(
            "SELECT product_id FROM `" . DB_PREFIX . "product_option_value`
            WHERE  `product_option_value_id` = '" . (int) $id . "' LIMIT 1
        "
        );

        if (empty($product_option_value->row['product_id'])) {
            $this->log->write(
                'ModelModuleCodeBlingNativeProduct_update_stock_variation() - Não encontrado o Produto'
            );

            return true;
        }

        //Consultando o Produto para alterar o Preço da Opção conforme o valor do Produto principal
        $product = $this->db->query(
            "SELECT price FROM `" . DB_PREFIX . "product`
            WHERE  `product_id` = '" . (int) $product_option_value->row['product_id'] . "' LIMIT 1
        "
        );

        if (empty($product->row['price'])) {
            $this->log->write(
                'ModelModuleCodeBlingNativeProduct_update_stock_variation() - Sem Preço no Produto'
            );

            return true;
        }

        if ($product->row['price'] > $price) {
            $priceUpdate = (float) $product->row['price'] - (float) $price;
            $price_prefix = '-';
        } else {
            $priceUpdate = (float) $price - (float) $product->row['price'];
            $price_prefix = '+';
        }

        $this->log->write(
            'ModelModuleCodeBlingNativeProduct_update_price_variation() - Alterando o Preço da Opção, product_option_value_id: '
            . $id . ' Preço final: ' . $price . ' Prefixo: ' . $price_prefix . ' Valor: '
            . $priceUpdate
        );

        $update = $this->db->query(
            "UPDATE `" . DB_PREFIX . "product_option_value` SET `price`= '"
            . $priceUpdate . "', `price_prefix`= '" . $price_prefix
            . "' WHERE  `product_option_value_id` = '" . (int) $id . "' "
        );

        if ($update) {
            return true;
        } else {
            return false;
        }
    }
}
