<?php

/**
 * © Copyright 2013-2025 Codemarket - Todos os direitos reservados.
 * Model nativo do Bling, que ficava no order passado para este model e feita algumas melhorias
 * Class ModelModuleCodeBlingNativeOrder
 */
class ModelModuleCodeBlingNativeOrder extends Model
{
    private $conf;
    private $log;
    private $debugLog = false;

    /**
     * ModelModuleCodeBlingNativeOrder constructor.
     *
     * @param $registry
     */
    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->load->model('module/codemarket_module');
        $conf = $this->model_module_codemarket_module->getModulo('548');
        $this->log = new log('Code-Bling-' . date('m-Y') . '.log');

        if (
            empty($conf) || empty($conf->code_habilitar)
            || (int) $conf->code_habilitar === 2
            || empty($conf->code_payment_id)
            || empty($conf->code_chave)
            || empty($conf->code_sku_opcao)
        ) {
            $this->log->write(
                'ModelModuleCodeBlingNativeOrde_construct() - Bling desativado, verifique a configuração'
            );
            exit('Desativado ou sem configuração');
        }

        $this->conf = $conf;

        if (!empty($conf->code_debug_log) && $conf->code_debug_log == 1) {
            $this->debugLog = true;
        }

        return true;
    }

    //ORDERS BY FILTERS
    public function getAllOrdersFilters($start = 0, $limit = 20, $filters)
    {
        if ($start < 0) {
            $start = 0;
        }
        if ($limit < 1) {
            $limit = 1;
        }
        $filters = urldecode($filters);
        $filter = explode('|', $filters);

        $startDate = $filter[0];
        $finishDate = $filter[1];
        $status = $filter[2];

        //Codemarket - Status Filtro
        $this->load->model('module/codemarket_module');
        $conf = $this->model_module_codemarket_module->getModulo('548');

        if ($this->debugLog) {
            $this->log->write(
                'ModelModuleCodeBlingNativeOrder_getAllOrdersFilters() - Filtros: '
                . print_r($filter, true)
            );
        }

        if (!empty($conf->code_status_cancelado) && (int) $status === 7) {
            $status = $conf->code_status_cancelado;
        }

        if (!empty($conf->code_status_cancelamento) && (int) $status === 9) {
            $status = $conf->code_status_cancelamento;
        }

        if (!empty($conf->code_status_cancelado_operadora) && (int) $status === 13) {
            $status = $conf->code_status_cancelado_operadora;
        }

        if (!empty($conf->code_status_completo) && (int) $status === 5) {
            $status = $conf->code_status_completo;
        }

        if (!empty($conf->code_status_negado) && (int) $status === 8) {
            $status = $conf->code_status_negado;
        }

        if (!empty($conf->code_status_expirado) && (int) $status === 14) {
            $status = $conf->code_status_expirado;
        }

        if (!empty($conf->code_status_nao_aprovado) && (int) $status === 10) {
            $status = $conf->code_status_nao_aprovado;
        }

        if (!empty($conf->code_status_processado) && (int) $status === 15) {
            $status = $conf->code_status_processado;
        }

        if (!empty($conf->code_status_processando) && (int) $status === 2) {
            $status = $conf->code_status_processando;
        }

        if (!empty($conf->code_status_reembolsado) && (int) $status === 11) {
            $status = $conf->code_status_reembolsado;
        }

        if (!empty($conf->code_status_estornado) && (int) $status === 12) {
            $status = $conf->code_status_estornado;
        }

        if (!empty($conf->code_status_despachado) && (int) $status === 3) {
            $status = $conf->code_status_despachado;
        }

        if (!empty($conf->code_status_anulado) && (int) $status === 16) {
            $status = $conf->code_status_anulado;
        }

        if (!empty($conf->code_status_personalizado) && $status == 'pers') {
            $status = $conf->code_status_personalizado;
        }

        if (empty($startDate)) {
            $startDate = '1969-01-01';
        }
        if (empty($finishDate)) {
            $finishDate = date('Y-m-d');
        }
        $sql = "SELECT DISTINCT(o.order_id), o.firstname, o.lastname, o.email, o.telephone, o.total, o.currency_code, o.currency_value, o.customer_id,o.date_added,o.payment_firstname,o.payment_lastname,o.payment_company,o.payment_address_1,o.payment_address_2,o.payment_city,o.payment_postcode,o.payment_country,o.payment_zone,o.payment_method,o.payment_code,o.shipping_firstname,o.shipping_lastname,o.shipping_company,o.shipping_address_1,o.shipping_address_2,o.shipping_city,o.shipping_postcode,o.shipping_country,o.shipping_zone,o.shipping_method,o.shipping_code,o.comment, os.name as status
        FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_status os ON (o.order_status_id = os.order_status_id)
        WHERE ";

        if ($status == 'tds') {
            $sql .= "o.order_status_id > 0";
        } else {
            $sql .= "o.order_status_id = '" . (int) $status . "'";
        }
        $sql .= " AND ( o.date_added BETWEEN '" . $startDate . "' AND '" . $finishDate
            . "')  ORDER BY o.order_id DESC LIMIT " . (int) $start . "," . (int) $limit;

        $query = $this->db->query($sql);

        $this->log->write(
            'ModelModuleCodeBlingNativeOrder_getAllOrdersFilters() - Retornando Pedidos com filtro'
        );

        return $query->rows;
    }

    //ORDER BY ID
    public function getOrderId($order_id)
    {
        $query = $this->db->query(
            "SELECT o.order_id, o.firstname, o.customer_id, o.lastname,  o.email, o.telephone, os.name as status, o.date_added, o.total, o.currency_code, o.currency_value, o.payment_firstname,o.payment_lastname,o.payment_company,o.payment_address_1,o.payment_address_2,o.payment_city,o.payment_postcode,o.payment_country,o.payment_zone,o.payment_method,o.payment_code,o.shipping_firstname,o.shipping_lastname,o.shipping_company,o.shipping_address_1,o.shipping_address_2,o.shipping_city,o.shipping_postcode,o.shipping_country,o.shipping_zone,o.shipping_method,o.shipping_code,o.comment
        FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_status os ON (o.order_status_id = os.order_status_id)
        WHERE o.order_id = '" . $order_id . "'"
        );

        $this->log->write(
            'ModelModuleCodeBlingNativeOrder_getOrderId() - Retornando Pedido: '
            . $order_id
        );

        return $query->rows;
    }

    //GETTING COUPON BY ORDER
    public function getCouponByOrder($order_id)
    {
        $query = $this->db->query(
            "SELECT  ot.value as valueCoupon, ot.code as codeCoupon
        FROM " . DB_PREFIX . "order_total ot
        WHERE ot.order_id = '" . $order_id . "'  AND (ot.code = 'coupon' or  ot.code = 'cupom')
        GROUP BY ot.value"
        );

        return $query->rows;
    }

    //GETTING SHIPPING PRICE BY ORDER
    public function getShippingByOrder($order_id)
    {
        if (empty($order_id)) {
            return false;
        }

        $query = $this->db->query(
            "SELECT  ot.value as valueShipping, ot.code as codeShipping
        FROM `" . DB_PREFIX . "order` o
        INNER JOIN `" . DB_PREFIX . "order_total` ot ON (o.order_id = ot.order_id)
        WHERE ot.order_id = '" . $order_id . "' AND ( ot.code ='shipping' or ot.code = 'frete')
        GROUP BY ot.value"
        );

        return $query->rows;
    }

    //PRODUCTS BY ID ORDER
    public function getProductOrderId($order_id)
    {
        $query = $this->db->query(
            "SELECT o.order_product_id, o.product_id, o.order_id, o.name, o.model, o.quantity, o.price, o.total, o.tax, o.reward, p.sku
        FROM `" . DB_PREFIX . "order_product` o LEFT JOIN " . DB_PREFIX . "product p ON (o.product_id = p.product_id)
        WHERE `order_id` = '" . $order_id . "'"
        );

        return $query->rows;
    }

    //Get products variations in order
    public function getVariation($parameters)
    {
        $sku = 'opov.' . $this->conf->code_sku_opcao;
        $query = $this->db->query(
            "SELECT DISTINCT(" . $sku . ") as sku, op.name as variationName, oo.name as nomeTipoVariacao, oo.value as tipoVariacao, opov.price as precoVaricao,  opov.price_prefix as prefixPrecoVaricao,  opov.weight as pesoVaricao, opov.weight_prefix as prefixPesoVaricao
        FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX . "order_option oo ON (op.order_product_id = oo.order_product_id )
        LEFT JOIN " . DB_PREFIX . "product_option_value  opov ON (oo.product_option_value_id = opov.product_option_value_id)
        WHERE op.order_id = '" . $parameters['order_id']
            . "' AND op.order_product_id = '" . $parameters['order_product_id']
            . "' AND oo.order_product_id is not null"
        );

        return $query->rows;
    }

    public function getTotalOrderProductsByOrderId($order_id)
    {
        $query = $this->db->query(
            "SELECT COUNT(*) AS total FROM " . DB_PREFIX
            . "order_product WHERE order_id = '" . (int) $order_id . "'"
        );

        return $query->row['total'];
    }

    public function getTotalOrderVouchersByOrderId($order_id)
    {
        $query = $this->db->query(
            "SELECT COUNT(*) AS total FROM `" . DB_PREFIX
            . "order_voucher` WHERE order_id = '" . (int) $order_id . "'"
        );

        return $query->row['total'];
    }
}
