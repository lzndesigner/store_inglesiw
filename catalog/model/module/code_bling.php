<?php

/**
 * © Copyright 2013-2025 Codemarket - Todos os direitos reservados.
 * Métodos auxiliares para adicionar mais recursos ao Bling
 * Class ModelModuleCodeBling
 */
class ModelModuleCodeBling extends Model
{
    private $token;
    private $conf;
    private $urlAPI;
    private $status;
    private $log;
    private $languages;
    private $debugLog = false;
    private $viewData = false;
    private $cacheCategory;
    private $codeCache;

    /**
     * ModelModuleCodeBling constructor.
     *
     * @param $registry
     */
    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->load->model('checkout/order');
        $this->load->model('module/codemarket_module');
        $this->load->model('localisation/language');
        $this->load->model('tool/image');
        $this->load->model('module/code_bling_native_product');

        $conf = $this->model_module_codemarket_module->getModulo('548');
        $this->log = new log('Code-Bling-' . date('m-Y') . '.log');

        if (
            empty($conf) || empty($conf->code_habilitar)
            || (int)$conf->code_habilitar === 2
            || empty($conf->code_payment_id)
            || empty($conf->code_chave)
            || empty($conf->code_access_token)
        ) {
            $this->log->write(
                'ModelModuleCodeBling_construct() - Bling desativado, verifique a configuração'
            );
            $this->status = false;

            return false;
        }

        $this->languages = $this->model_localisation_language->getLanguages();
        $this->status = true;
        $this->conf = $conf;
        $this->token = trim($conf->code_access_token);

        // API V3 do Bling ERP https://developer.bling.com.br/migracao-v2-v3#migra%C3%A7%C3%A3o-api-v2/v3
        $this->urlAPI = 'https://bling.com.br/Api/v3';

        if (!empty($conf->code_debug_log) && (int)$conf->code_debug_log === 1) {
            $this->debugLog = true;
        }

        if (!empty($this->request->get['debug'])
            && (int)$this->request->get['debug'] === 1
        ) {
            $this->debugLog = true;
        }

        if (!empty($this->request->get['viewData'])
            && (int)$this->request->get['viewData'] === 1
        ) {
            $this->viewData = true;
        }

        if (version_compare(VERSION, '3.0.0.0') < 0) {
            $cache_type = $this->config->get('cache_type');
        } else {
            $cache_type = $this->config->get('cache_engine');
        }

        // 300s = 5 minutos - Cache para a Categoria
        $this->cacheCategory = new Cache($cache_type, 300);

        // 172800s = 48h - Cache
        //$this->codeCache = new Cache($cache_type, 172800);

        // 10 dias
        $this->codeCache = new Cache($cache_type, 864000);

        return true;
    }

    public function testAPI()
    {
        if (!$this->status) {
            return false;
        }

        // Busca um Pedido no Bling
        $order_id = !empty($this->request->get['order_id']) ? (int)$this->request->get['order_id'] : 1;
        $order = $this->getOrderBling($order_id);

        echo "<h2>Pedido " . $order_id . "</h2>";
        print_r($order);

        $cpf = !empty($this->request->get['cpf']) ? trim($this->request->get['cpf']) : 1;
        echo "<h2>Cliente Documento " . $cpf . "</h2>";
        $customer = $this->getCustomerBling($cpf);
        print_r($customer);

        $sku = !empty($this->request->get['sku']) ? trim($this->request->get['sku']) : 1;
        echo "<h1>Buscando um Produto</h1>";
        $productBling = $this->getProductBling($sku, '', false);
        print_r($productBling);

        $product_id_bling = !empty($this->request->get['product_id_bling']) ? trim(
            $this->request->get['product_id_bling']
        ) : 1;
        echo "<h1>Buscando o Estoque de um Produto</h1>";
        $stock = $this->getStockProductBling($product_id_bling);
        print_r($stock);
    }

    /**
     * Adiciona um Cliente e Pedido no Bling, antes verifica se já existe o Pedido
     *
     * @param $order
     *
     * @return bool
     * @uses addCustomerByOrderBling() Adiciona o Cliente no Bling com base nos dados do Pedido
     */
    public function addOrderBling($order)
    {
        //print_r($order);
        $orderNative = $this->db->query(
            "SELECT * FROM `" . DB_PREFIX . "order`
            WHERE order_id = '" . (int)$order['order_id'] . "' LIMIT 1"
        )->row;

        //print_r($orderNative);
        //exit();
        $zone = $this->getZoneCode($order['payment']['payment_zone']);

        // Cadastrando/Atualizando o Cliente
        $codigo_prefix = !empty($this->request->get['customer_id_prefix'])
            ? strtoupper(
                substr($this->request->get['customer_id_prefix'], 0, 2)
            )
            : "";

        $cutomerBling = $this->addCustomerByOrderBling(
            $order,
            $orderNative,
            $zone,
            $codigo_prefix
        );

        if (empty($cutomerBling)) {
            $this->log->write(
                'addOrderBling() - Falha no Cadastro/Atualização do cliente, Pedido: ' . $orderNative['order_id']
            );

            return false;
        }

        echo "<h2>Cadastrado/Atualizado Cliente: " . $orderNative['customer_id']
            . "</h2>";

        // Verificando se é apenas para Cadastrar/Atualizar Clientes
        if (!empty($this->request->get['only_customer'])
            && (int)$this->request->get['only_customer'] === 1
        ) {
            // Salvando para a última posição rodada
            $this->codeCache->set(
                "codebling_addorder_last",
                $order['order_id']
            );

            $this->codeCache->set(
                'codebling_addorder' . $order['order_id'],
                1
            );

            return true;
        }

        if (!empty($this->request->get['use_order_id'])
            && (int)$this->request->get['use_order_id'] === 1
        ) {
            $verify = $this->getOrderBling((int)$order['order_id']);

            // Sleep de 250ms = 0,25s, pois o Bling limita até 3 chamadas por s
            usleep(250000);
        }

        //print_r($verify);

        // Verifica se já tem Pedido
        if (!empty($verify)) {
            echo "<h3>Já enviado o Pedido: " . $order['order_id'] . "</h3>";
            $this->log->write(
                'ModelModuleCodeBling_addOrderBling() - Já enviado o Pedido: '
                . $order['order_id']
            );

            // Salvando para a última posição rodada
            $this->codeCache->set(
                "codebling_addorder_last",
                $order['order_id']
            );

            $this->codeCache->set('codebling_addorder' . $order['order_id'], 1);

            return true;
        }

        //HookAddOrderBling
        // https://developer.bling.com.br/referencia#/Pedidos%20-%20Vendas/post_pedidos_vendas
        $url = $this->urlAPI . '/pedidos/vendas';

        if (!empty($this->request->get['use_order_id'])
            && (int)$this->request->get['use_order_id'] === 1
        ) {
            $data['numero'] = (int)$order['order_id'];
        }

        $data['numeroLoja'] = (int)$order['order_id'];

        if (!empty($this->conf->code_store_id)) {
            $data['loja']['id'] = trim($this->conf->code_store_id);
        }

        $date_exit = isset($this->request->get['date_exit'])
            ? (int)$this->request->get['date_exit'] : 1;
        $date_preview = isset($this->request->get['date_preview'])
            ? (int)$this->request->get['date_preview'] : 1;

        $data['data'] = date(
            'Y-m-d',
            strtotime($orderNative['date_modified'])
        );

        $data['dataSaida'] = date(
            'Y-m-d',
            strtotime(
                $orderNative['date_modified'] . ' + ' . $date_exit . ' days'
            )
        );

        $data['dataPrevista'] = date(
            'Y-m-d',
            strtotime(
                $orderNative['date_modified'] . ' + ' . $date_preview . ' days'
            )
        );

        //https://ajuda.bling.com.br/hc/pt-br/articles/360036713173
        //https://ajuda.bling.com.br/hc/pt-br/articles/360034982693-Como-cadastrar-uma-Natureza-de-Opera%C3%A7%C3%A3o

        /*
         * Removido API V3
        $nat_operacao = !empty($this->request->get['nat_operacao']) ? trim(
            $this->request->get['nat_operacao']
        ) : "Venda de mercadorias";

        $xml .= '  <nat_operacao>'.$nat_operacao.'</nat_operacao>';
        $xml .= '  <cliente>';
        */

        if (!empty($this->request->get['use_customer_id'])
            && (int)$this->request->get['use_customer_id'] === 1
        ) {
            $data['contato']['codigo'] = $codigo_prefix . $orderNative['customer_id'];
        }

        // Importante passar o ID do Bling do Contato/Cliente
        $data['contato']['id'] = $cutomerBling;
        $data['contato']['tipoPessoa'] = $order['persontype'];
        $data['contato']['numeroDocumento'] = $this->onlyNumber($order['cpf_cnpj']);

        // Transporte

        /**
         * No campo Tipo Frete, podemos salvar com os seguintes valores com letra maiúscula para os caracteres.
         * 0 Contratação do Frete por conta do Remetente (CIF)
         * 1 Contratação do Frete por conta do Destinatário (FOB)
         * 2 Contratação do Frete por conta de Terceiros
         * 3 Transporte Próprio por conta do Remetente
         * 4 Transporte Próprio por conta do Destinatário
         * 9 Sem Ocorrência de Transporte.
         */
        $fretePorConta = !empty($this->request->get['shipping_type']) ? (int)$this->request->get['shipping_type'] : 1;
        $data['transporte']['fretePorConta'] = $fretePorConta;

        $shipping_price = !empty($order['shipping']['shipping_price'])
            ? (float)$order['shipping']['shipping_price'] : 0;

        if (!empty($order['shipping']['shipping_postcode'])) {
            $data['transporte']['frete'] = codeHelperNumberDecimal($shipping_price);
        }

        $data['transporte']['quantidadeVolumes'] = 1;
        //$data['transporte']['pesoBruto'] = $fretePorConta;
        //$data['transporte']['prazoEntrega'] = $fretePorConta;

        // Verificando se tem endereço de Entrega
        if (!empty($order['shipping']['shipping_postcode'])) {
            $data['transporte']['etiqueta'] = [
                'nome' => $order['shipping']['shipping_name'],
                'endereco' => $order['shipping']['shipping_address_1'],
                'numero' => $order['shipping']['shipping_number'],
                'complemento' => $order['shipping']['shipping_address_2'],
                'municipio' => $order['shipping']['shipping_city'],
                'uf' => $this->getZoneCode($order['shipping']['shipping_zone']),
                'cep' => $order['shipping']['shipping_postcode'],
                'bairro' => $order['shipping']['shipping_neighborhood'],
                'nomePais' => 'BRASIL',
            ];
        }

        // Itens
        foreach ($order['products'] as $product) {
            /*
            $orderProduct = $this->db->query("SELECT o.order_product_id, o.product_id, o.order_id, o.name, o.model, o.quantity, o.price, o.total, o.tax, o.reward, p.sku
                FROM `" . DB_PREFIX . "order_product` o
                WHERE `product_id` = '" . (int) $product['product_id'] . "'
                LIMIT 1
                "
            )->row;
            */

            // print_r($product); exit();

            // Verificando se tem opção, se não tiver fica os dados do order_product apenas
            if (empty($product['variation'])) {
                // Precisa usar o ID do Produto no Bling ERP
                $productBling = $this->getProductBling($product['sku'], '', true);

                if (empty($productBling['id'])) {
                    $this->log->write(
                        'addOrderBling() - Não encontrou no Bling - Produto: '
                        . $product['sku'] . ' ' . $product['product_id']
                    );

                    return false;
                }

                $data['itens'][] = [
                    //'codigo' => $product['sku'],
                    //'unidade' => 'UN',
                    'quantidade' => $product['quantity'],
                    'valor' => codeHelperNumberDecimal($product['price']),
                    //'descricao' => strip_tags($product['name']),
                    'produto' => ['id' => $productBling['id']],
                ];

                /*
                if (!empty($product['pricePromo'])) {
                    //Percentual de desconto do item
                    $vlr_desconto = (1 - ($product['pricePromo'] / $product['price'])) * 100;
                    //$xml .= '  <vlr_desconto>' . $product['pricePromo'] . '</vlr_desconto>';
                    $xml .= '  <vlr_desconto>' . $vlr_desconto . '</vlr_desconto>';
                }
                */
                continue;
            }

            /*
             * Tem Opções
             * Detalhe que no order_product fica 1 produto, mas pode ter N opções escolhidas
             * Exemplo: Camiseta Cor Azul e Tamanho M
             * Vamos lidar até 2 opções por Produto
             */

            $productNative = $this->db->query(
                "SELECT * FROM `" . DB_PREFIX . "product`
                WHERE product_id = '" . (int)$product['product_id'] . "' LIMIT 1"
            )->row;

            if (!empty($this->request->get['price_product'])) {
                // product
                $price = $productNative['price'];
            } else {
                // order_product
                $price = $product['price'];
            }

            $price_before = 0;
            foreach ($product['variation'] as $key => $option) {
                $sku = !empty($option['sku']) ? trim($option['sku'])
                    : $product['product_id'] . '_nosku';

                // Verificando o Preço da Opção
                if ($option['precoVaricao'] != 0) {
                    if ($option['prefixPesoVaricao'] == '+') {
                        if ($option['precoVaricao'] > 0
                            && !empty($this->request->get['option_more_to_equal'])
                        ) {
                            $price = $option['precoVaricao'];
                        } else {
                            $price = $price + $option['precoVaricao'];
                        }
                    } else {
                        if ($option['prefixPesoVaricao'] == '-') {
                            $price = $price - $option['precoVaricao'];
                        }
                    }
                }

                // Só tem uma opção, fica o preço do order_product
                if (empty($product['variation'][1])) {
                    $price = $product['price'];
                }

                if ($key == 0) {
                    $price_before = $price;
                } else {
                    if ($key == 1
                        && $product['price'] != ($price + $price_before)
                    ) {
                        // Normalizando o preço, devido a algum preço especial ou promoção
                        $price = $product['price'] - $price_before;
                    }
                }

                // Precisa usar o ID do Produto no Bling ERP
                $productBling = $this->getProductBling($sku, '', true);

                if (empty($productBling['id'])) {
                    $this->log->write(
                        'addOrderBling() - Não encontrou no Bling - Produto: '
                        . $sku . ' ' . $product['product_id']
                    );

                    return false;
                }

                $data['itens'][] = [
                    //'codigo' => $sku,
                    //'unidade' => 'UN',
                    'quantidade' => $product['quantity'],
                    'valor' => codeHelperNumberDecimal($price),
                    //'descricao' => strip_tags($product['name']).' Opção:'
                    //    .strip_tags($option['tipoVariacao']),
                    'produto' => ['id' => $productBling['id']],
                ];

                // Para depois de 2 Opções
                if ($key == 1) {
                    break;
                }
            }
        }

        // https://ajuda.bling.com.br/hc/pt-br/articles/360046309874-GET-formaspagamento
        // Vou deixar 1 por enquanto
        //$xml .= '  <idFormaPagamento>1915969</idFormaPagamento>';
        // parcelas
        //HookFormaPagamento

        /*
         *   [payment_method] => Cartão de Crédito
            [payment_code] => code_pagarme
         */

        //print_r($order); exit();
        // Agora é obrigatório Parcelas na API V3

        $data['parcelas'][] = [
            //'dataVencimento' => date('Y-m-d', strtotime('+1 days')),
            'dataVencimento' => date(
                'Y-m-d',
                strtotime(
                    $orderNative['date_modified'] . ' +1 days'
                )
            ),
            'valor' => codeHelperNumberDecimal($order['total']),
            'observacoes' => 'Pagamento: ' . $order['payment']['payment_method'] . ', Code: ' . $order['payment']['payment_code'],
            'formaPagamento' =>
                [
                    'id' => $this->conf->code_payment_id,
                ]
        ];
        //print_r($data); exit();

        // Desconto tem que passar como positivo, se passar negativo ele soma
        $discount = abs($order['discount']);

        $total_produto = $this->db->query(
            "SELECT SUM(price * quantity) as total FROM " . DB_PREFIX
            . "order_product WHERE order_id = '" . (int)$order['order_id'] . "'"
        )->row['total'];
        // Verificando se a soma dos Produtos - Desconto + Frete = Total do Pedido
        $newTotal = ($total_produto - $discount + $shipping_price);

        /*
         // Simulacoes
        $order['total'] = 2190.00;
        echo "Desconto: ".$discount;
        echo "<br>";
        echo "Total Produtos: ".$total_produto;
        echo "<br>";
        echo "Total novo: ".$newTotal;
        echo "<br>";
        echo "Total Pedido: ".$order['total'];
        */
        if ($newTotal > $order['total']) {
            $discount = $discount + ($newTotal - $order['total']);
        }

        // Desconto
        $data['desconto'] = [
            'valor' => codeHelperNumberDecimal(abs($discount)),
            'unidade' => 'REAL',
        ];

        // Verificando se tem taxa extra/despesas
        if ($newTotal < $order['total']) {
            $tax = $order['total'] - $newTotal;
            $data['outrasDespesas'] = abs($tax);
        }

        // ##### MONTANDO AS OBSERVAÇÕES DO PEDIDO #####
        // Usando o último comentário do Histórico
        $history = $this->db->query(
            "
            SELECT comment, date_added FROM " . DB_PREFIX . "order_history
            WHERE
            order_id = '" . (int)$orderNative['order_id'] . "'
            ORDER BY order_history_id DESC
            LIMIT 1
        "
        )->row;

        // Variáveis para usar nas Observações 09/06/2022 11:42
        $order_obs_store_name = !empty($orderNative['store_name']) ? trim(
            strip_tags($orderNative['store_name'])
        ) : '';
        $order_obs_order_id = $order['order_id'];
        $order_obs_comment = !empty($order['comment'])
            ? ", Comentários do Pedido: "
            . $order['comment'] : "";
        $order_obs_payment_method = !empty($order['payment']['payment_method'])
            ? trim($order['payment']['payment_method']) : '';
        $order_obs_payment_code = !empty($order['payment']['payment_code'])
            ? trim($order['payment']['payment_code']) : '';
        $order_obs_shipping_method
            = !empty($order['shipping']['shipping_method']) ? trim(
            $order['shipping']['shipping_method']
        ) : '';
        $order_obs_shipping_code = !empty($order['shipping']['shipping_code'])
            ? trim($order['shipping']['shipping_code']) : '';
        $order_obs_shipping_price = !empty($order['shipping']['shipping_price'])
            ? $order['shipping']['shipping_price'] : 0;
        $order_obs_discount = !empty($order['discount']) ? $order['discount']
            : 0;

        $order_obs_date_added = date(
            'd/m/Y H:i:s',
            strtotime(
                $orderNative['date_added']
            )
        );
        $order_obs_date_modified = date(
            'd/m/Y H:i:s',
            strtotime(
                $orderNative['date_modified']
            )
        );

        $date_now = new DateTime("now", new DateTimeZone('America/Sao_Paulo'));
        $date_now = $date_now->format('d/m/Y H:i:s');
        //$date_now = date('d/m/Y H:i:s');

        $last_comment = "";

        if (!empty($history['comment'])) {
            $last_comment = date(
                    'd/m/Y H:i:s',
                    strtotime(
                        $history['date_added']
                    )
                ) . " - Último comentário do Histórico do Pedido: "
                . $history['comment'];
        }

        // Observação

        if (empty($this->conf->code_order_obs)) {
            $obs = "Loja: " . $order_obs_store_name . ", Pedido: "
                . $order_obs_order_id . ", criado: " . $order_obs_date_added;
        } else {
            $obs = str_replace(
                [
                    '{nome_loja}',
                    '{id_pedido}',
                    '{comentario_pedido}',
                    '{nome_pagamento_pedido}',
                    '{codigo_pagamento_pedido}',
                    '{nome_entrega_pedido}',
                    '{codigo_entrega_pedido}',
                    '{valor_entrega_pedido}',
                    '{desconto_pedido}',
                    '{data_criado_pedido}',
                    '{data_modificado_pedido}',
                    '{data_chamada_api}',
                    '{ultimo_comentario_pedido}',
                ],
                [
                    $order_obs_store_name,
                    $order_obs_order_id,
                    $order_obs_comment,
                    $order_obs_payment_method,
                    $order_obs_payment_code,
                    $order_obs_shipping_method,
                    $order_obs_shipping_code,
                    $order_obs_shipping_price,
                    $order_obs_discount,
                    $order_obs_date_added,
                    $order_obs_date_modified,
                    $date_now,
                    $last_comment,
                ],
                trim($this->conf->code_order_obs)
            );
        }

        $data['observacoes'] = strip_tags($obs);

        // Observação Interna
        if (empty($this->conf->code_order_obs_private)) {
            $obs_private = $last_comment;
            $obs_private .= ", Pagamento Loja: " . $order_obs_payment_method
                . ", Código: " . $order_obs_payment_code;
            $obs_private .= $order_obs_comment;
            $obs_private .= ", Enviado ao ERP: " . $date_now;
            $obs_private .= ", pelo App Bling Opencart da Codemarket";
        } else {
            $obs_private = str_replace(
                [
                    '{nome_loja}',
                    '{id_pedido}',
                    '{comentario_pedido}',
                    '{nome_pagamento_pedido}',
                    '{codigo_pagamento_pedido}',
                    '{nome_entrega_pedido}',
                    '{codigo_entrega_pedido}',
                    '{valor_entrega_pedido}',
                    '{desconto_pedido}',
                    '{data_criado_pedido}',
                    '{data_modificado_pedido}',
                    '{data_chamada_api}',
                    '{ultimo_comentario_pedido}',
                ],
                [
                    $order_obs_store_name,
                    $order_obs_order_id,
                    $order_obs_comment,
                    $order_obs_payment_method,
                    $order_obs_payment_code,
                    $order_obs_shipping_method,
                    $order_obs_shipping_code,
                    $order_obs_shipping_price,
                    $order_obs_discount,
                    $order_obs_date_added,
                    $order_obs_date_modified,
                    $date_now,
                    $last_comment,
                ],
                trim($this->conf->code_order_obs_private)
            );
        }

        $data['observacoesInternas'] = strip_tags($obs_private);

        if ($this->debugLog) {
            $this->log->write(
                'ModelModuleCodeBling_addOrderBling() Dados ' . print_r($data, true)
            );
        }

        /*
        unset($data['numero']);
        unset($data['numeroLoja']);
        unset($data['observacoes']);
        unset($data['observacoesInternas']);
        unset($data['transporte']);
        */

        // Exibindo dados, modo de teste
        $this->viewData($data);

        /*
         * N da para definir a Categoria do Pedido
         *
         */

        // Sleep de 800ms = 0,8s, pois o Bling limita até 3 chamadas por s
        // Extra 29/10/2022 12:28
        usleep(800000);

        //$addOrder = $this->post($data, $url);
        $addOrder = $this->apiCall(
            $url,
            "POST",
            __FUNCTION__ . "_POST",
            $data,
            15,
            1
        );

        if (!empty($addOrder['error'])) {
            $this->log->write(
                'ModelModuleCodeBling_addOrderBling() - Rodado para Pedido: '
                . $order['order_id'] . ', erros: ' . print_r(
                    $addOrder['error'],
                    true
                )
            );

            echo "<h3>Rodado para o Pedido: " . $order['order_id'] . " com erros: "
                . print_r(
                    $addOrder['error'],
                    true
                ) . "</h3>";

            //  Numero do pedido na loja ja existente (65)
            if (!empty($addOrder['error']['fields']['code'])
                && (int)$addOrder['error']['fields']['code'] === 39
            ) {
                // Salvando para a última posição rodada
                $this->codeCache->set(
                    "codebling_addorder_last",
                    $order['order_id']
                );

                $this->codeCache->set(
                    'codebling_addorder' . $order['order_id'],
                    1
                );
            } elseif (isset($addOrder['error']['fields']['code'])
                && (int)$addOrder['error']['fields']['code'] === 0
            ) {
                //  CPF inválido
                $this->codeCache->set(
                    'codebling_addorder' . $order['order_id'],
                    1
                );
            }
        } else {
            if (!empty($addOrder['data'])) {
                // Salvando para a última posição rodada
                $this->codeCache->set(
                    "codebling_addorder_last",
                    $order['order_id']
                );

                $this->codeCache->set(
                    'codebling_addorder' . $order['order_id'],
                    1
                );
            }

            $this->log->write(
                'ModelModuleCodeBling_addOrderBling() - Rodado com sucesso para o Pedido: '
                . $order['order_id']
            );

            echo "<h3 style='color:#47d35d'>Rodado com sucesso para o Pedido: " . $order['order_id'] . "</h3>";
        }
    }

    public function verifyOrders()
    {
        if (!empty($this->request->get['interval_created'])
            && $this->request->get['interval_update']
        ) {
            $interval['created'] = (int)$this->request->get['interval_created'];
            $interval['update'] = (int)$this->request->get['interval_update'];
        } else {
            $interval = '';
        }

        $limit = !empty($this->request->get['pages'])
            ? (int)$this->request->get['pages'] : 10;

        //Cada consulta pode retornar até 100 Pedidos, logo ose o limit = 10 vai rodar até 1000 pedidos
        for ($i = 1; $i <= $limit; $i++) {
            $orders = $this->getOrdersBling($i, $interval);

            //print_r($orders); exit();

            if (empty($orders['data'])) {
                echo "<h3>Sem Pedidos - Horário: " . date('i:s') . "</h3>";
                $this->log->write(
                    "ModelModuleCodeBling_verifyOrders - Sem Pedidos retornados"
                );

                $this->log->write(
                    "ModelModuleCodeBling_verifyOrders - Retorno: "
                    . print_r($orders, true)
                );

                return true;
            }

            foreach ($orders['data'] as $order) {
                // Comparando os Preços
                if (empty($order['total'])) {
                    echo "<h3>Pedido com Preço 0, dados do Pedido Bling:</h3>";
                    echo "<pre>";
                    print_r($order);
                    echo "</pre>";
                } elseif (!empty($order['numeroLoja'])) {
                    $orderStore = $this->db->query(
                        "
                        SELECT o.order_id, o.total FROM `" . DB_PREFIX . "order` o
                        WHERE o.order_id = '"
                        . (int)$order['numeroLoja'] . "'
                        LIMIT 1
                        "
                    )->row;

                    // HookPriceVerify
                    if (!empty($orderStore['order_id'])
                        && $order['total'] == $orderStore['total']
                    ) {
                        echo "<h3>Pedido ID Loja " . $orderStore['order_id'] . " e
                        ID Bling " . $order['numero'] . " com Preço igual da Loja,
                        Preço Loja " . $orderStore['total'] . " e Preço Bling "
                            . $order['total'] . "</h3>";
                    } else {
                        if (!empty($orderStore['order_id'])
                            && $order['total'] != $orderStore['total']
                        ) {
                            echo "<h3>Pedido ID Loja " . $orderStore['order_id'] . " e
                        ID Bling " . $order['numero'] . " com Preço diferente da Loja,
                        Preço Loja " . $orderStore['total']
                                . " dados Pedido Bling: </h3>";
                            echo "<pre>";
                            print_r($order);
                            echo "</pre>";
                        }
                    }
                }

                // Verificando se tem Nota Fiscal
                /*
                $nfs = $this->getNFBling($interval);

                if(empty($nfs)){
                    continue;
                }

                foreach ($nfs as $nf){
                    if(empty($nf['notaservico'])){
                        continue;
                    }

                    $nf = $nf['notaservico'];

                }
                print_r($nfs); exit();
                */
            }

            //Até 100 pedidos retornados por consulta
            if (count($orders['data']) < 100) {
                $this->log->write(
                    "ModelModuleCodeBling_verifyOrders - Retornado menos de 100 pedidos, rodado até a página "
                    . $i
                );

                return true;
            }
        }

        return true;
    }

    /**
     * Mudando os Status dos Pedidos
     * Bling -> Opencart
     *
     * @return bool
     */
    public function changeStatusOrders()
    {
        //Consultando até 1000 pedidos
        for ($i = 1; $i <= 10; $i++) {
            $orders = $this->getOrdersBling($i);
            //print_r($orders); exit();

            if (empty($orders['data'])) {
                echo "<h3>Sem Pedidos - Horário: " . date('i:s') . "</h3>";
                $this->log->write(
                    "ModelModuleCodeBling_changeStatusOrders - Sem Pedidos retornados"
                );
                $this->log->write(
                    "ModelModuleCodeBling_changeStatusOrders - Retorno: "
                    . print_r($orders, true)
                );

                return true;
            }

            $this->runChangeStatusOrders($orders);

            //Até 100 pedidos retornados por consulta
            if (count($orders['data']) < 100) {
                $this->log->write(
                    "ModelModuleCodeBling_changeStatusOrders - Retornado menos de 100 pedidos, rodado até a página "
                    . $i
                );

                return true;
            }
        }

        return true;
    }

    /**
     * Retorna os Pedidos do Bling
     * Bling -> Opencart
     * https://developer.bling.com.br/referencia#/Pedidos%20-%20Compras/get_pedidos_compras
     *
     * @return bool|mixed
     */
    public function getOrdersBling($page, $interval = '')
    {
        if (!$this->status) {
            return false;
        }

        if (empty($interval)) {
            $date_start_created = date('Y-m-d', strtotime('-30 days'));
            //$date_start_update = date('Y-m-d', strtotime('-1 days'));
        } else {
            $date_start_created = date(
                'Y-m-d',
                strtotime('-' . $interval['created'] . ' days')
            );
            /*
            $date_start_update = date(
                'Y-m-d',
                strtotime('-'.$interval['update'].' days')
            );
            */
        }

        $date_end = date('Y-m-d');
        // &dataInicial=2023-01-01&dataFinal=2023-01-01, padrão YYYY-MM-DD
        $params = [
            'pagina' => $page,
            'limite' => 100,
            'dataInicial' => $date_start_created,
            'dataFinal' => $date_end,
            'idLoja' => $this->conf->code_store_id
        ];

        $url = $this->urlAPI . '/pedidos/vendas?' . http_build_query($params);
        //echo $url; exit();
        //return $this->get($url);

        return $this->apiCall(
            $url,
            "GET",
            __FUNCTION__ . "_GET",
            [],
            15,
            1
        );
    }

    /**
     * Retorna as Notas Fiscais do Bling
     * Bling -> Opencart
     * No momento sem uso e não foi atualizada para a API V3 11/2024
     *
     * @return bool|mixed
     */
    public function getNFBling($interval = '')
    {
        if (!$this->status) {
            return false;
        }

        if (empty($interval)) {
            $date_start_created = date('Y-m-d', strtotime('-30 days'));
        } else {
            $date_start_created = date(
                'Y-m-d',
                strtotime('-' . $interval['created'] . ' days')
            );
        }

        $date_end = date('Y-m-d');

        //&filters=dataEmissao[31/10/2020 TO 30/11/2020];dataAlteracao[29/11/2020 TO 30/11/2020]
        $filter
            = "&filters=dataEmissao[{$date_start_created} TO {$date_end}]";

        // Nota Fiscal de Serviço
        if (!empty($this->request->get['nf'])
            && $this->request->get['nf'] == '1'
        ) {
            $url = $this->urlAPI . '/notasservico/json&apikey='
                . $this->token . $filter;
        } else {
            $url = $this->urlAPI . '/notasfiscais/json&apikey='
                . $this->token . $filter;
        }

        //echo $url; exit();
        //$data = $this->get($url);
        $data = $this->apiCall(
            $url,
            "GET",
            __FUNCTION__ . "_GET",
            [],
            15,
            1
        );
        if (empty($data['notaservico'])) {
            return false;
        }

        return $data['notaservico'];
    }

    /**
     * Muda o Status do Pedido
     * Chamado pelo catalog/controller/feed/rest_api.php método callbackOrder
     * Bling -> Opencart
     *
     * @param $orders
     *
     * @return bool
     */
    public function runChangeStatusOrders($orders)
    {
        // Vindo do Webhook
        if (!empty($orders['retorno']['pedidos'])) {
            $orders['data'][0] = $orders['retorno']['pedidos'][0]['pedido'];
            unset($orders['retorno']);
        }

        $this->log->write(
            "ModelModuleCodeBling_runChangeStatusOrders - Verificando os Pedidos, total: "
            . count(
                $orders['data']
            )
        );
        echo "<h3>Verificando os Pedidos, total: " . count(
                $orders['data']
            ) . " - Horário: " . date(
                'i:s'
            ) . "</h3>";

        //Status dos Pedidos no Bling https://ajuda.bling.com.br/hc/pt-br/articles/360036457114-Situa%C3%A7%C3%B5es-dos-pedido-de-venda

        /*
         *
         *  Em aberto
            Em digitação
            Venda Agenciada
            Em andamento
            Cancelado
            Atendido
         */

        foreach ($orders['data'] as $order) {
            //print_r($order);

            $id = 0;

            // Vindo do Webhook
            if (!empty($order['numeroPedidoLoja'])) {
                $orderBling = $this->getOrderBling($order['numeroPedidoLoja']);

                if (empty($orderBling['id'])) {
                    $this->log->write(
                        "ModelModuleCodeBling_runChangeStatusOrders() - Sem o ID do Pedido no Bling"
                    );

                    continue;
                }

                $id = $orderBling['id'];
            }

            // Vindo da API do Bling
            $id = !empty($order['id']) ? $order['id'] : $id;

            if (empty($id)) {
                $this->log->write(
                    "ModelModuleCodeBling_runChangeStatusOrders() - Sem o ID do Pedido"
                );

                continue;
            }

            // Precisa buscar pelo ID do Bling para vir os dados de Transporte
            $orderBling = $this->getOrderIDBling($id);
            //print_r($orderBling); exit();

            $this->log->write(
                "ModelModuleCodeBling_runChangeStatusOrders() - Retorno: "
                . print_r($orderBling, true)
            );

            //Não é Pedido do Opencart
            if (empty($orderBling['numeroLoja'])) {
                echo "<h3>Não encontrado o Pedido na Loja, Número no Bling: "
                    . $orderBling['numero'] . " - Horário: " . date(
                        'i:s'
                    ) . "</h3>";

                $this->log->write(
                    "ModelModuleCodeBling_runChangeStatusOrders() - Não achou o Pedido na Loja, ID Pedido: "
                    . $orderBling['numero']
                );

                continue;
            }

            $rastreamento = '';
            $comment = '';
            $status = (int)$orderBling['situacao']['id'];

            /*
             * <option value="6">Em aberto</option>
             * <option value="9">Atendido</option>
             * <option value="12">Cancelado</option>
             * <option value="15">Em andamento</option>
             * <option value="18">Venda Agenciada</option>]
             * <option value="21">Em digitação</option>
             * <option value="24">Verificado</option></select>
             */

            switch ($status) {
                case 6:
                    $order_status_id
                        = (int)$this->conf->code_situacao_em_aberto;
                    break;
                case 21:
                    $order_status_id
                        = (int)$this->conf->code_situacao_em_digitacao;
                    break;
                case 18:
                    $order_status_id
                        = (int)$this->conf->code_situacao_venda_agenciada;
                    break;
                case 15:
                    $order_status_id
                        = (int)$this->conf->code_situacao_em_andamento;
                    break;
                case 12:
                    $order_status_id
                        = (int)$this->conf->code_situacao_cancelado;
                    break;
                case 9:
                    $order_status_id = (int)$this->conf->code_situacao_atendido;
                    break;
            }

            //Parte do Rastreamento do Envio
            //Pode ter um ou mais Volumes, logo mais de um Código de Rastreamento
            if (!empty($order['transporte']['volumes'])) {
                $volumes = $order['transporte']['volumes'];
                foreach ($volumes as $v) {
                    if (empty($v['volume'])) {
                        continue;
                    }

                    $v2 = $v['volume'];
                    if (!empty($v2['codigoRastreamento'])) {
                        $rastreamento .= 'Enviado por ' . $v2['servico'] . '<br>';
                        $rastreamento .= '<b>Código de Rastreio:</b> '
                            . $v2['codigoRastreamento'] . '<br><br>';
                    }

                    // Não tem a urlRastreamento na API V3
                    /*
                    if (!empty($v2['urlRastreamento'])) {
                        //Devido a alguns serviços vir faltando o http, colocamos ele se não tiver
                        $link = strpos($v2['urlRastreamento'], 'http') === 0
                            ? trim(
                                $v2['urlRastreamento']
                            ) : 'https://' . trim($v2['urlRastreamento']);

                        $rastreamento .= '<a href="' . $link
                            . '" target="_blank">Rastrear o seu Pedido</a><br>';
                    }
                    */
                }
            }
            //Teste Rastreamento
            //$rastreamento .= '<b>Enviado por:</b> Sedex<br>';
            //$rastreamento .= '<b>Código de Rastreio:</b> BR5968678704<br><br>';

            if (!empty($order_status_id)) {
                //Verificando se tem o texto do Rastreamento e se o Status é o do Rastreamento
                if (!empty($rastreamento)
                    && $order_status_id
                    == $this->conf->code_situacao_rastreamento
                ) {
                    $comment = $rastreamento;
                }

                $this->addOrderHistoryVerify(
                    $orderBling['numeroLoja'],
                    $order_status_id,
                    $comment,
                    true
                );
            }
        }

        return true;
    }

    /*
     * // Sem uso e comentado 12/11/2024, API V2
    private function addCustomerBling($customer_id)
    {
        $customer = $this->db->query("
            SELECT code FROM " . DB_PREFIX . "customer
            WHERE
            customer_id = '" . (int) $customer_id . "'
            LIMIT 1
        ")->row;

        $verify = $this->getCustomerBling($this->onlyNumber($order['cpf_cnpj']));

        // Cadastrando o cliente primeiro https://ajuda.bling.com.br/hc/pt-br/articles/360046378614-POST-contato
        $url = $this->urlAPI.'/contato';

        $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
        $xml .= '  <contato>';

        $xml .= '  <codigo>' . $orderNative['customer_id'] . '</codigo>';
        $xml .= '  <nome>' . $order['name'] . '</nome>';
        $xml .= '  <fantasia>' . $order['name'] . '</fantasia>';
        $xml .= '  <tipoPessoa>' . $order['persontype'] . '</tipoPessoa>';
        $xml .= '  <cpf_cnpj>' . $this->onlyNumber($order['cpf_cnpj']) . '</cpf_cnpj>';
        //$xml .= '  <ie>' . $order['customer_id'] . '</ie>';
        //$xml .= '  <rg>' . $order['customer_id'] . '</rg>';
        $xml .= '  <contribuinte>9</contribuinte>';
        $xml .= '  <endereco>' . $order['payment']['payment_address_1'] . '</endereco>';
        $xml .= '  <numero>' . $order['payment']['payment_number_address'] . '</numero>';
        $xml .= '  <complemento>' . $order['payment']['payment_address_2'] . '</complemento>';
        $xml .= '  <bairro>' . $order['payment']['payment_neighborhood'] . '</bairro>';
        $xml .= '  <cep>' . $order['payment']['payment_postcode'] . '</cep>';
        $xml .= '  <cidade>' . $order['payment']['payment_city'] . '</cidade>';
        $xml .= '  <uf>' . $zone . '</uf>';
        if (!empty($order['telephone'])) {
            $xml .= '  <fone>' . $order['telephone'] . '</fone>';
        }
        if (!empty($order['payment']['payment_cellphone'])) {
            $xml .= '  <celular>' . $order['payment']['payment_cellphone'] . '</celular>';
        }
        $xml .= '  <email>' . $order['email'] . '</email>';
        if (empty($verify)) {
            $xml .= '  <emailNfe>' . $order['email'] . '</emailNfe>';
        }

        if (!empty($this->request->get['information_contact']) && empty($verify)) {
            $xml .= '  <informacaoContato>' . trim($this->request->get['information_contact']) . '</informacaoContato>';
        }
        //$xml .= '  <site>' . $order['site'] . '</site>';

        if (!empty($this->request->get['type_contact']) && empty($verify)) {
            $xml .= '  <tipos_contatos>';
            $xml .= '  <tipo_contato>';
            $xml .= '  <descricao>' . trim($this->request->get['type_contact']) . '</descricao>';
            $xml .= '  </tipo_contato>';
            $xml .= '  </tipos_contatos>';
        }

        $xml .= '  </contato>';

        $data = [
            'apikey' => $this->token,
            'xml'    => rawurlencode($xml),
        ];

        if (!empty($verify)) {
            $url .= $verify['id'];
            return $this->post($data, $url, 'PUT');
        }

        return $this->post($data, $url);
    }
    */

    /**
     * Atualiza os estoques dos Produtos/Opções vindos do Webhook do Bling ERP
     * Chamado pelo catalog/controller/feed/rest_api.php método callbackStock
     * Bling -> Opencart
     */
    public function runChangeStockProduct($stock)
    {
        if (!isset($stock['estoqueAtual']) || empty($stock['codigo'])) {
            $this->log->write(
                'runChangeStockProduct() - Sem estoque ou código do Produto'
            );

            return false;
        }

        $this->log->write(
            'runChangeStockProduct() - Alterando a quantidade do Produto/Opção, Código/SKU: '
            . $stock['codigo'] . ' QTD: ' . $stock['estoqueAtual']
        );

        // Descobrindo o ID do Produto/Opção
        $data
            = $this->model_module_code_bling_native_product->getProductIdorOption(
            $stock['codigo']
        );
        //$this->log->write('runChangeStockProduct() - Dados: ' . print_r($data, true));

        if (!empty($data['product_option_value_id'])) {
            //Opção
            $this->model_module_code_bling_native_product->update_stock_variation(
                $data['product_option_value_id'],
                $stock['estoqueAtual']
            );
            $this->updateStockProductParent(
                $data['product_option_value_id'],
                0
            );
            $this->log->write(
                'runChangeStockProduct() - Alterado com sucesso o estoque da Opção'
            );

            return true;
        } else {
            if (!empty($data['product_id'])) {
                //Produto
                $this->model_module_code_bling_native_product->update_stock_product(
                    $data['product_id'],
                    $stock['estoqueAtual']
                );
                $this->updateStockProductParent('', $data['product_id']);
                $this->log->write(
                    'runChangeStockProduct() - Alterado com sucesso o estoque do Produto'
                );

                return true;
            }
        }

        $this->log->write(
            'runChangeStockProduct() - Produto não encontrado pelo Código/SKU: '
            . $stock['codigo']
        );

        return false;
    }

    /**
     * Melhoria extra para lidar com o detalhe na Quantidade do Produto Principal quando tem Opções, o Bling retorna
     * quantidade 0 Soma a quantidade das Opções do Produto
     *
     * @param string $product_option_value_id ID do valor da Opção do Produto contendo o ID da opção e do seu valor
     * @param int    $product_id              Id do Produto na Loja
     *                                        Bling -> Opencart
     */
    public function updateStockProductParent(
        string $product_option_value_id,
        int $product_id
    ) {
        $this->log->write(
            "updateStockProductParent() - Rodando para product_option_value_id " . $product_option_value_id . " e product_id " . $product_id
        );

        if (empty($product_id) && !empty($product_option_value_id)) {
            $idOption = explode('v', $product_option_value_id);
            if (!empty($idOption[1])) {
                $product_option_value_id = $idOption[1];
            }

            //Verificando o ID do Produto
            $query = $this->db->query(
                "SELECT product_id FROM " . DB_PREFIX . "product_option_value
                WHERE
                `product_option_value_id` = '" . (int)$product_option_value_id . "'
                LIMIT 1
            "
            )->row;

            if (!empty($query['product_id'])) {
                $product_id = $query['product_id'];
            }
        }

        if (empty($product_id)) {
            return false;
        }

        $query = $this->db->query(
            "SELECT SUM(quantity) as qtd FROM " . DB_PREFIX . "product_option_value
            WHERE
            `product_id` = '" . $product_id . "'
        "
        );

        if (isset($query->row['qtd'])) {
            $qtd = $query->row['qtd'];
            $update = $this->db->query(
                "UPDATE `" . DB_PREFIX . "product`
                SET `quantity`= '" . (int)$qtd . "'
                WHERE
                `product_id` = '" . (int)$product_id . "'
            "
            );

            $this->log->write(
                "updateStockProductParent() - Alterado com sucesso a quantidade do Produto: "
                . $product_id . " Qtd: " . $qtd
            );
        }
    }

    /**
     * Retorna o nome e descrição do Produto
     *
     * @param $product
     *
     * @return false
     */
    public function correctedProductStoreDescription($product)
    {
        if (!$this->status) {
            return false;
        }

        if (empty($product['product_id'])) {
            return $product;
        }

        $product_id = $product['product_id'];
        $query = $this->getProduct($product_id);

        //$this->log->write('ModelModuleCodeBling_correctedProductStoreDescription() Produto SQL ' . print_r($query, true));
        if (!empty($query['name'])) {
            $product['name'] = trim($query['name']);
        }

        if (!empty($query['description'])) {
            $product['description'] = trim($query['description']);
        }

        //print_r($product); exit();
        //$this->log->write('ModelModuleCodeBling_correctedProductStoreDescription() Array Final ' . print_r($product, true));
        return $product;
    }

    /**
     * Retorna o Produto no Opencart
     *
     * @param $product_id
     *
     * @return false
     */
    public function getProduct($product_id)
    {
        if (!$this->status) {
            return false;
        }

        $language_id = $this->languageId();

        $sql
            = "SELECT DISTINCT *, pd.name AS name, p.image, m.name AS manufacturer, (SELECT price FROM "
            . DB_PREFIX
            . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '"
            . (int)$this->config->get(
                'config_customer_group_id'
            )
            . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM "
            . DB_PREFIX
            . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '"
            . (int)$this->config->get(
                'config_customer_group_id'
            )
            . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special,
            (SELECT ss.name FROM " . DB_PREFIX
            . "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '"
            . (int)$language_id . "') AS stock_status, (SELECT wcd.unit FROM "
            . DB_PREFIX
            . "weight_class_description wcd WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = '"
            . (int)$language_id . "') AS weight_class, (SELECT lcd.unit FROM "
            . DB_PREFIX
            . "length_class_description lcd WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = '"
            . (int)$language_id
            . "') AS length_class, (SELECT AVG(rating) AS total FROM " . DB_PREFIX
            . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT COUNT(*) AS total FROM "
            . DB_PREFIX
            . "review r2 WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id) AS reviews, p.sort_order FROM "
            . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX
            . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN "
            . DB_PREFIX
            . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN "
            . DB_PREFIX
            . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) WHERE p.product_id = '"
            . (int)$product_id . "' AND pd.language_id = '" . (int)$language_id
            //."' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id >= 0 ORDER BY p2s.store_id ASC LIMIT 1"
            . "' AND p.date_available <= NOW() AND p2s.store_id >= 0 ORDER BY p2s.store_id ASC LIMIT 1";

        //echo "<h2>SQL Produto</h2>";
        //echo $sql;

        $query = $this->db->query(
            $sql
        )->row;

        // Verificando se tem atributos
        $this->load->model('catalog/product');

        $product_attributes
            = $this->model_catalog_product->getProductAttributes(
            $query['product_id']
        );

        $attributes = "";

        if (!empty($product_attributes)) {
            $attributes .= "<h2>Especificações</h2>";

            foreach ($product_attributes as $pa) {
                if (empty($pa['name']) || empty($pa['attribute'])) {
                    continue;
                }

                $attributes .= "<p><b>" . trim($pa['name']) . ":</b> <br>";

                foreach ($pa['attribute'] as $att) {
                    $attributes .= $att['name'] . ": " . $att['text'] . "<br>";
                }

                $attributes .= "</p><br>";
            }
        }

        //print_r($attributes);
        //print_r($product_attributes);

        if (!empty($query['description'])) {
            $query['description'] = trim($query['description']) . $attributes;
        }

        return $query;
    }

    /**
     * Retorna o ID da linguagem pt-br
     * Se não encontrar retorna o ID 1 da linguagem
     *
     * @return mixed
     */
    public function languageId()
    {
        foreach ($this->languages as $l) {
            if (empty($l['code']) || $l['code'] != 'pt-br'
                || empty($l['language_id'])
            ) {
                continue;
            }

            return $l['language_id'];
        }

        return 1;
    }

    /**
     * Retorna os Produtos do Bling ERP
     * Sem uso no momento 04/11/2024
     *
     * @param $page
     * @param $filtros
     *
     * @return bool|mixed
     */
    public function getProductsBling($page = 1, $filtros = '')
    {
        if (!$this->status) {
            return false;
        }

        $loja = '';

        //https://developer.bling.com.br/referencia#/Produtos/get_produtos
        $url = $this->urlAPI . '/produtos?pagina=' . $page;
        //echo $url;
        //$getProduct = $this->get($url);
        $getProduct = $this->apiCall(
            $url,
            "GET",
            __FUNCTION__ . "_GET",
            [],
            15,
            1
        );

        return $getProduct;
    }

    /**
     * Atualiza os dados do Produto no Bling usando a API V3
     * Envia as imagens, localização, observação e a URL do Produto
     * Opencart -> Bling
     * https://developer.bling.com.br/referencia#/Produtos/patch_produtos__idProduto_
     *
     * @param $product_id
     * @param $sku
     * @param $keyCache
     *
     * @return bool
     */
    public function updateProdutoBling($product_id, $sku, $keyCache)
    {
        if (!$this->status) {
            return false;
        }

        echo "<h2>Rodando para o Produto: " . $sku . " " . $product_id
            . " - Horário: " . date(
                'i:s'
            ) . "</h2>";

        $verify = $this->codeCache->get('updateProdutoBling' . $product_id);
        if (!empty($verify)) {
            $this->log->write(
                'ModelModuleCodeBling_updateProdutoBling() - Cache, já rodado
                para o Produto: ' . $sku . ' ' . $product_id
            );
            echo "<h4>Cache, já rodado para o Produto " . $sku . " " . $product_id
                . "</h4>";

            return true;
        }

        $product = $this->getProduct($product_id);
        $this->log->write(
            'ModelModuleCodeBling_updateProdutoBling() - Rodando - Produto: '
            . $sku . ' ' . $product_id
        );

        if (!$this->status || empty($product['sku'])) {
            $this->log->write(
                'ModelModuleCodeBling_updateProdutoBling() - Desativado ou sem SKU - Produto: '
                . $sku . ' ' . $product_id
            );

            return false;
        }

        // Precisa usar o ID do Produto no Bling ERP
        $productBling = $this->getProductBling($product['sku'], '', true);

        if (empty($productBling['id'])) {
            $this->log->write(
                'ModelModuleCodeBling_updateProdutoBling() - Não encontrou no Bling - Produto: '
                . $sku . ' ' . $product_id
            );

            return false;
        }

        $url = $this->urlAPI . '/produtos/' . $productBling['id'];

        $linkProduct = $this->url->link(
            'product/product',
            '&product_id=' . $product['product_id']
        );

        //$linkProduct = str_replace('&amp;', '&', $linkProduct);

        $data['codigo'] = $product['sku'];
        $data['linkExterno'] = $linkProduct;

        if (!empty($this->conf->code_image_exportar)
            && $this->conf->code_image_exportar == 1
            && !empty($product['image'])
            && (string)$keyCache === 'codebling_products_filters'
        ) {
            $linkImg = str_replace(
                ' ',
                '%20',
                $this->config->get('config_ssl') . 'image/' . $product['image']
            );

            $data['midia']['imagens']['externas'][]['link'] = $linkImg;

            $query = $this->db->query(
                "SELECT * FROM " . DB_PREFIX . "product_image
                WHERE product_id = '" . (int)$product['product_id'] . "'
                ORDER BY sort_order ASC
            "
            );

            if (!empty($query->rows[0]['image'])) {
                foreach ($query->rows as $img) {
                    if (!empty($img['image'])) {
                        $linkImg = str_replace(
                            ' ',
                            '%20',
                            $this->config->get('config_ssl') . 'image/'
                            . $img['image']
                        );

                        $data['midia']['imagens']['externas'][]['link'] = $linkImg;
                    }
                }
            }
        } else {
            $this->log->write(
                'ModelModuleCodeBling_Configurado para não exportar imagens ou sem imagens'
            );
        }

        $model_column = !empty($this->conf->code_produto_modelo) ? trim(
            $this->conf->code_produto_modelo
        ) : '';

        // Coluna personalizável para o Modelo do Produto, precisa existir no bling o model_column
        if (!empty($product['model']) && !empty($model_column)) {
            $data[$model_column] = substr($product['model'], 0, 64);
        }

        // NCM
        $ncm_column = !empty($this->conf->code_produto_ncm) ? trim(
            $this->conf->code_produto_ncm
        ) : '';
        if (!empty($product[$ncm_column])) {
            $data['tributacao']['ncm'] = substr(
                $this->onlyNumber($product[$ncm_column]),
                0,
                10
            );
        }

        //print_r($product); exit();

        /*
        // Já usando por padrão na API do Opencart, por isso comentado
        // Adicionando Marca
        if (!empty($product['manufacturer'])) {
            $xml .= '<marca>'.trim($product['manufacturer']).'</marca>';
        }
        */

        // CEST
        $cest_column = !empty($this->conf->code_produto_cest) ? trim(
            $this->conf->code_produto_cest
        ) : '';

        if (!empty($product[$cest_column])) {
            $data['tributacao']['cest'] = substr(
                $this->onlyNumber($product[$cest_column]),
                0,
                7
            );
        }

        // EAN/GTIN
        $ean_column = !empty($this->conf->code_produto_gtin) ? trim(
            $this->conf->code_produto_gtin
        ) : '';

        if (!empty($product[$ean_column])) {
            $data['gtin'] = substr(
                $this->onlyNumber($product[$ean_column]),
                0,
                14
            );
        }

        // Local do Estoque do Produto
        if (!empty($product['location'])) {
            $data['estoque']['localizacao'] = substr(trim($product['location']), 0, 50);
        }

        // Não tem a quantidade exata em estoque, apenas mínimo e máximo
        $data['estoque']['minimo'] = 1;

        // Setando um valor alto para o máximo
        if (isset($product['quantity'])) {
            $data['estoque']['maximo'] = 100000;
        }

        // Mudando no Estoque, precisa primeiro descobrir o ID do Estoque
        /*
         * Não está pegando, aguardando suporte Bling ERP @todo
        $stock = $this->getStockProductBling($productBling['id']);

        if(!empty($stock['data'][0]['depositos']['0']['id'])){
            $stock_id = $stock['data'][0]['depositos']['0']['id'];

            $this->updateStockProductBling($stock_id, (int)$product['quantity']);
        }
        */

        // Observações do Produto
        if (!empty($this->conf->code_produto_observacao)
            && !empty($product[$this->conf->code_produto_observacao])
        ) {
            $data['observacoes'] = trim(
                $product[$this->conf->code_produto_observacao]
            );
        }

        // Atualizando o peso, como o Opencart só tem 1 peso usamos o mesmo para o peso liquido e bruto
        if (!empty($product['weight']) && !empty($product['weight_class_id'])) {
            $data['pesoLiquido'] = $this->convertWeight(
                $product['weight'],
                $product['weight_class_id']
            );

            $data['pesoBruto'] = $data['pesoLiquido'];
        }

        //Atualizando as dimensões
        if (!empty($product['length']) && !empty($product['width'])
            && !empty($product['height'])
            && !empty($product['length_class_id'])
        ) {
            $data['dimensoes'] = [
                'largura' => $this->convertLength(
                    $product['width'],
                    $product['length_class_id']
                ),
                'altura' => $this->convertLength(
                    $product['height'],
                    $product['length_class_id']
                ),
                'profundidade' => $this->convertLength(
                    $product['length'],
                    $product['length_class_id']
                ),
                'unidadeMedida' => 1,
            ];
        }

        // Atualizando o Preço e Quantidade adicionado 07/06/2022 12:04
        if (isset($product['price'])) {
            $data['preco'] = (float)$product['price'];
        }

        // Fornecedor
        /*
         * Informe a estrutura do Fornecedor do Produto, exemplo:
            descricao:oc_product_description:name, id:oc_product:model, codigo:oc_product:sku,
            custo:oc_product:price, onde a primeira palavra representa o campo no Fabricante, a
            segunda a tabela com o prefixo se tiver e a terceira o nome da coluna no banco que deseja usar,
            separando cada palavra por : e , para cada campo do Fabricante.
         *
         */

        /*
         *
        $xml .= '  <descricaoFornecedor>Teste</descricaoFornecedor>';
        $xml .= '  <idFabricante>13995237530</idFabricante>';
        $xml .= '  <codigoFabricante>Codigo Fabricante 32</codigoFabricante>';
        $xml .= '  <preco_custo>35</preco_custo>';
        */
        /*
        * Comentado por enquanto o código do Fornecedor, devido a grandes mudanças 12/11/2024
        if (!empty($this->conf->code_fornecedor)) {
            $sql = trim($this->conf->code_fornecedor);
            $sql = explode(",", $sql);

            foreach ($sql as $fornecedor) {
                $fornecedor = explode(":", $fornecedor);
                if (empty($fornecedor[0]) || empty($fornecedor[1])
                    || empty($fornecedor[2])
                ) {
                    continue;
                }

                $fornecedor[0] = trim($fornecedor[0]);
                $fornecedor[1] = trim($fornecedor[1]);
                $fornecedor[2] = trim($fornecedor[2]);

                // CONSULTANDO NO BANCO
                // Verificando se existe a tabela e coluna
                $query = $this->db->query(
                    "SHOW TABLES LIKE '".$fornecedor[1]."'"
                )->row;
                if (empty($query)) {
                    continue;
                }

                $query = $this->db->query(
                    "SHOW COLUMNS FROM `".$fornecedor[1]."` LIKE '"
                    .$fornecedor[2]."'"
                )->row;
                //print_r($query); exit();

                if (empty($query)) {
                    continue;
                }

                // Verificando o campo usado na busca
                if (!empty($fornecedor[3])) {
                    $fornecedor[3] = trim($fornecedor[3]);
                } else {
                    $fornecedor[3] = 'product_id';
                }

                if (strstr($fornecedor[1], 'manufacturer')) {
                    $query[$fornecedor[2]] = $product['manufacturer'];
                } else {
                    $query = $this->db->query(
                        "SELECT ".$fornecedor[2]." FROM `".$fornecedor[1]."`
                        WHERE
                        ".$fornecedor[3]." = '".(int)$product['product_id']."'
                        LIMIT 1
                    "
                    )->row;
                }

                if (empty($query[$fornecedor[2]])) {
                    continue;
                }

                $fornecedor_value = trim($query[$fornecedor[2]]);

                // Montando o XML
                if ((string)$fornecedor[0] === "descricao") {
                    $xml .= '  <descricaoFornecedor>'.substr(
                            $fornecedor_value,
                            0,
                            150
                        ).'</descricaoFornecedor>';
                } elseif ((string)$fornecedor[0] === "id") {
                    $xml .= '  <idFabricante>'.substr(
                            $fornecedor_value,
                            0,
                            150
                        ).'</idFabricante>';
                } elseif ((string)$fornecedor[0] === "codigo") {
                    $xml .= '  <codigoFabricante>'.substr(
                            $fornecedor_value,
                            0,
                            150
                        ).'</codigoFabricante>';
                } elseif ((string)$fornecedor[0] === "custo") {
                    $xml .= '  <preco_custo>'.(float)$fornecedor_value
                        .'</preco_custo>';
                }
            }
        }
        */

        // HookArrayProductEnd

        if ($this->debugLog) {
            $this->log->write(
                'ModelModuleCodeBling_updateProdutoBling() Dados Produto '
                . print_r($data, true)
            );
        }

        // Exibindo dados, modo de teste
        $this->viewData($data);

        //$this->log->write('ModelModuleCodeBling_updateProdutoBling() Data ' . print_r($data, true));

        //Espera 500ms/0.5s para evitar o limite do Bling https://ajuda.bling.com.br/hc/pt-br/articles/360046302394-Limites
        //Limite de 3 chamadas por segundo
        usleep(500000);

        //$updateProduct = $this->post($data, $url, 'PATCH');
        $updateProduct = $this->apiCall(
            $url,
            "PATCH",
            __FUNCTION__ . "_PATCH",
            $data,
            15,
            1
        );

        if (!empty($updateProduct['error'])) {
            $this->log->write(
                "ModelModuleCodeBling_updateProdutoBling() - Falhou ao atualizar para o Produto "
                . $sku . " " . $product_id . ", erros: " . print_r(
                    $updateProduct['error'],
                    true
                )
            );

            echo "<h3>Falhou ao atualizar para o Produto: "
                . $sku . " " . $product_id . "</h3>";
            print_r($updateProduct['error']);

            return false;
        } else {
            $this->log->write(
                'ModelModuleCodeBling_updateProdutoBling() - Rodado - Produto: '
                . $sku . ' ' . $product_id
            );

            echo "<h3>Rodado para o Produto: " . $sku . " " . $product_id
                . " - Horário: " . date(
                    'i:s'
                ) . "</h3>";
        }

        //$updateProduct2 = $this->post($data, $url, 'PATCH');
        // print_r($updateProduct2); exit();

        // VARIAÇÕES/OPÇÕES
        $variations
            = $this->model_module_code_bling_native_product->getVariation(
            $product
        );

        if (empty($variations)) {
            $this->log->write(
                'ModelModuleCodeBling_updateProdutoBling() - Sem opções: '
                . $sku . ' ' . $product_id
            );
            $this->codeCache->set('updateProdutoBling' . $product_id, 1);

            echo "<h2 style='color:#47d35d'>Rodado com sucesso para o Produto: " . $sku . " " . $product_id
                . " - Horário: " . date(
                    'i:s'
                ) . "</h2>";

            return true;
        }

        $qts_products = count($variations);

        echo "<h3>Rodando para " . $qts_products . " variações - Horário: "
            . date('i:s')
            . "</h3>";

        foreach ($variations as $v) {
            // Precisa usar o ID do Produto no Bling ERP
            $productBling = $this->getProductBling($v['sku'], '', true);

            if (empty($productBling['id'])) {
                $this->log->write(
                    'ModelModuleCodeBling_updateProdutoBling() - Não encontrou no Bling - Produto Variação/Opção: '
                    . $v['sku']
                );

                return false;
            }

            $url = $this->urlAPI . '/produtos/' . $productBling['id'];
            $prefix = $v['prefixPrecoVaricao'];
            $preco = $v['precoVaricao'];

            if ($prefix == '+') {
                $preco = $product['price'] + $preco;
            } else {
                if ($prefix == '-') {
                    $preco = $product['price'] - $preco;
                }
            }

            //HookArrayVariation
            $data['codigo'] = $v['sku'];
            $data['linkExterno'] = $linkProduct;
            $data['descricaoCurta'] = $product['description'];
            $data['descricaoComplementar'] = $product['description'];

            // Só atualiza se o preço da opção for diferente do preço do Produto
            if ($preco != $product['price']) {
                $data['preco'] = (float)$preco;
            }

            // Atualiza a quantidade 08/06/2022 10:57
            // Não tem a quantidade exata em estoque, apenas mínimo e máximo, vou deixar comentado para as Opções
            // 15/11/2024
            /*
            if (isset($product['quantidadeVariacao'])) {
                $data['estoque']['maximo'] = (int)$product['quantidadeVariacao'];
            }
            */

            // Adicionando imagens
            if (!empty($this->conf->code_image_exportar)
                && $this->conf->code_image_exportar == 1
                && !empty($product['image'])
                && (string)$keyCache === 'codebling_products_filters'
            ) {
                $linkImg = str_replace(
                    ' ',
                    '%20',
                    $this->config->get('config_ssl') . 'image/' . $product['image']
                );

                $data['midia']['imagens']['externas'][]['link'] = $linkImg;

                $query = $this->db->query(
                    "SELECT * FROM " . DB_PREFIX . "product_image
                WHERE product_id = '" . (int)$product['product_id'] . "'
                ORDER BY sort_order ASC
            "
                );

                if (!empty($query->rows[0]['image'])) {
                    foreach ($query->rows as $img) {
                        if (!empty($img['image'])) {
                            $linkImg = str_replace(
                                ' ',
                                '%20',
                                $this->config->get('config_ssl') . 'image/'
                                . $img['image']
                            );

                            $data['midia']['imagens']['externas'][]['link'] = $linkImg;
                        }
                    }
                }
            } else {
                $this->log->write(
                    'ModelModuleCodeBling_Configurado para não exportar imagens ou sem imagens'
                );
            }

            // Fornecedor
            /*
             * Informe a estrutura do Fornecedor do Produto, exemplo:
                descricao:oc_product_description:name, id:oc_product:model, codigo:oc_product:sku,
                custo:oc_product:price, onde a primeira palavra representa o campo no Fabricante, a
                segunda a tabela com o prefixo se tiver e a terceira o nome da coluna no banco que deseja usar,
                separando cada palavra por : e , para cada campo do Fabricante.
             *
             */

            /*
             *
            $xml .= '  <descricaoFornecedor>Teste</descricaoFornecedor>';
            $xml .= '  <idFabricante>13995237530</idFabricante>';
            $xml .= '  <codigoFabricante>Codigo Fabricante 32</codigoFabricante>';
            $xml .= '  <preco_custo>35</preco_custo>';
            */
            /*
             * Comentado por enquanto o código do Fornecedor, devido a grandes mudanças 12/11/2024
            if (!empty($this->conf->code_fornecedor)) {
                $sql = trim($this->conf->code_fornecedor);
                $sql = explode(",", $sql);

                foreach ($sql as $fornecedor) {
                    $fornecedor = explode(":", $fornecedor);
                    if (empty($fornecedor[0]) || empty($fornecedor[1])
                        || empty($fornecedor[2])
                    ) {
                        continue;
                    }

                    $fornecedor[0] = trim($fornecedor[0]);
                    $fornecedor[1] = trim($fornecedor[1]);
                    $fornecedor[2] = trim($fornecedor[2]);

                    // CONSULTANDO NO BANCO
                    // Verificando se existe a tabela e coluna
                    $query = $this->db->query(
                        "SHOW TABLES LIKE '".$fornecedor[1]."'"
                    )->row;
                    if (empty($query)) {
                        continue;
                    }

                    $query = $this->db->query(
                        "SHOW COLUMNS FROM `".$fornecedor[1]."` LIKE '"
                        .$fornecedor[2]."'"
                    )->row;
                    //print_r($query); exit();

                    if (empty($query)) {
                        continue;
                    }

                    // Verificando o campo usado na busca
                    if (!empty($fornecedor[3])) {
                        $fornecedor[3] = trim($fornecedor[3]);
                    } else {
                        $fornecedor[3] = 'product_id';
                    }

                    if (strstr($fornecedor[1], 'manufacturer')) {
                        $query[$fornecedor[2]] = $product['manufacturer'];
                    } else {
                        $query = $this->db->query(
                            "SELECT ".$fornecedor[2]." FROM `".$fornecedor[1]."`
                        WHERE
                        ".$fornecedor[3]." = '".(int)$product['product_id']."'
                        LIMIT 1
                    "
                        )->row;
                    }

                    if (empty($query[$fornecedor[2]])) {
                        continue;
                    }

                    $fornecedor_value = trim($query[$fornecedor[2]]);
                    //HookArrayVariationFornecedor

                    // Montando o XML
                    if ((string)$fornecedor[0] === "descricao") {
                        $xml .= '  <descricaoFornecedor>'.substr(
                                $fornecedor_value,
                                0,
                                150
                            ).'</descricaoFornecedor>';
                    } elseif ((string)$fornecedor[0] === "id") {
                        $xml .= '  <idFabricante>'.substr(
                                $fornecedor_value,
                                0,
                                150
                            ).'</idFabricante>';
                    } elseif ((string)$fornecedor[0] === "codigo") {
                        $xml .= '  <codigoFabricante>'.substr(
                                $fornecedor_value,
                                0,
                                150
                            ).'</codigoFabricante>';
                    } elseif ((string)$fornecedor[0] === "custo") {
                        $xml .= '  <preco_custo>'.(float)$fornecedor_value
                            .'</preco_custo>';
                    }
                }
            }
            */

            // HookArrayVariationEnd

            if ($this->debugLog) {
                $this->log->write(
                    'ModelModuleCodeBling_updateProdutoBling() Dados ' . print_r(
                        $data,
                        true
                    )
                );
            }

            //$this->log->write('ModelModuleCodeBling_updateProdutoBling() Data ' . print_r($data, true));

            //Espera 500ms/0.5s para evitar o limite do Bling https://ajuda.bling.com.br/hc/pt-br/articles/360046302394-Limites
            //Limite de 3 chamadas por segundo
            usleep(500000);

            //$updateProduct = $this->post($data, $url, 'PATCH');
            $updateProduct = $this->apiCall(
                $url,
                "PATCH",
                __FUNCTION__ . "_PATCH",
                $data,
                15,
                1
            );

            if (!empty($updateProduct['error'])) {
                $this->log->write(
                    "ModelModuleCodeBling_updateProdutoBling() - Falhou ao atualizar para Opção SKU: "
                    . $v['sku'] . ", erros: " . print_r(
                        $updateProduct['error'],
                        true
                    )
                );

                echo "<h3>Falhou ao atualizar para a Opção SKU: "
                    . $v['sku'] . "</h3>";
                print_r($updateProduct['error']);

                return false;
            } else {
                $this->log->write(
                    'ModelModuleCodeBling_updateProdutoBling() - Rodado para Opção SKU: '
                    . $v['sku']
                );

                echo "<h3>Rodado para a Opção SKU: " . $v['sku'] . " - Horário: "
                    . date(
                        'i:s'
                    ) . "</h3>";
            }
        }


        $this->codeCache->set('updateProdutoBling' . $product_id, 1);

        echo "<h2 style='color:#47d35d'>Rodado com sucesso para as Opções e Produto: " . $sku . " "
            . $product_id
            . " - Horário: " . date(
                'i:s'
            ) . "</h2>";

        return true;
    }

    /**
     * Alterando os Produtos com novos dados, como: model novo, localização, imagens, meta titulo, seo_url...
     * Bling -> Opencart usa o cache do Produto vindo do Bling
     * https://ajuda.bling.com.br/hc/pt-br/articles/360046422734-GET-produto-codigo-
     *
     * @param $product_id
     *
     * @return bool
     * @uses updateProdutoStoreSQL Atualizado os dados do produto na Loja
     * @uses getProductBling() Retorna o Produto no Bling sem uso do cache
     */
    public function updateProductStore($product_id)
    {
        if (!$this->status) {
            return false;
        }

        $this->log->write(
            'ModelModuleCodeBling_updateProductStore() - Rodando - Produto ID: '
            . $product_id
        );

        $query = $this->db->query(
            "SELECT sku FROM " . DB_PREFIX . "product WHERE product_id = '"
            . (int)$product_id . "' LIMIT 1"
        );

        if (empty($query->row['sku'])) {
            return false;
        }

        $codigo = $query->row['sku'];

        // Não usa o Cache para puxar os dados mais atuais do Bling
        $getProduct = $this->getProductBling($codigo, '', false);

        if (empty($getProduct['codigo'])) {
            $this->log->write(
                'ModelModuleCodeBling_updateProductStore() - Retorno sem o Código - Produto ID: '
                . $product_id
            );

            return false;
        }

        $product = $getProduct;

        if ($this->debugLog) {
            $this->log->write(
                'ModelModuleCodeBling_updateProductStore() - Dados do Produto vindo do Bling ERP - '
                . print_r(
                    $product,
                    true
                )
            );
        }

        $this->updateProdutoStoreSQL($product, $product_id);

        /*
         *  [midia] => Array
        (
            [video] => Array
                (
                    [url] =>
                )

            [imagens] => Array
                (
                    [externas] => Array
                        (
                        )

                    [internas] => Array
                        (
                            [0] => Array
                                (
                                    [link] => https://orgbling.s3.amazonaws.com/bd9e3c397844c62960da0b6033c185c3/40e35322a9eb3e5f2cef0ffb57975a06?AWSAccessKeyId=AKIATCLMSGFX4J7TU445&Expires=1733404757&Signature=nKYqaFwHKEePJ6pikveyNdVOxn4%3D
                                    [linkMiniatura] => https://orgbling.s3.amazonaws.com/bd9e3c397844c62960da0b6033c185c3/t/40e35322a9eb3e5f2cef0ffb57975a06?AWSAccessKeyId=AKIATCLMSGFX4J7TU445&Expires=1732801757&Signature=l7W0mq35DYhe84elXq%2BUlLf1sd0%3D
                                    [validade] => 2024-12-05 10:19:17
                                )

                        )

                )

        )
        */

        if (!empty($product['midia']['imagens']['internas'])) {
            $this->saveImageStore(
                $product['nome'],
                // Imagens dentro do Bling ERP
                $product['midia']['imagens']['internas'],
                $product_id
            );
        } else {
            $this->log->write(
                'ModelModuleCodeBling_updateProductStore() - Sem imagens Internas no Bling para o Produto ID: '
                . $product_id
            );
        }

        $this->log->write(
            'ModelModuleCodeBling_updateProductStore() - Rodado - Produto ID: '
            . $product_id
        );

        return true;
    }

    /**
     * Busca os dados de um Produto ligado a Loja pelo seu ID na Loja
     *
     * @param $product_id do Produto na Loja
     *
     * @return false|mixed
     */
    public function getProductStoreBling($product_id)
    {
        if (!$this->status) {
            return false;
        }

        /*
        // https://developer.bling.com.br/referencia#/Produtos%20-%20Lojas/get_produtos_lojas__idProdutoLoja_
        $url = $this->urlAPI . '/produtos/lojas/'.$product_id;

        // Retornou RESOURCE_NOT_FOUND
        */

        $params = [
            'pagina' => 1,
            'limite' => 1,
            'idProduto' => $product_id,
            'idLoja' => $this->conf->code_store_id
        ];

        $url = $this->urlAPI . '/produtos/lojas?' . http_build_query($params);

        $getProduct = $this->apiCall(
            $url,
            "GET",
            __FUNCTION__ . "_GET",
            [],
            15,
            1
        );

        //print_r($getProduct); exit();

        /*
        $this->log->write(
            "ModelModuleCodeBling_getProductStoreBling() - Retorno: ".print_r($getProduct,true)
        );
        */

        if (empty($getProduct['data'][0])) {
            return false;
        }

        return $getProduct['data'][0];
    }

    /**
     * Retorna os dados do Produto do Bling com base no Código/SKU ou no ID do Produto no Bling ERP
     *
     * @param $codigo Código/SKU do Produto
     * @param $id     ID do Produto no Bling ERP
     * @param $cache  Se true usa o Cache se tiver
     *
     * @return bool|mixed|string
     */
    public function getProductBling($codigo = '', $id = '', $cache = true)
    {
        if (!$this->status) {
            return false;
        }

        if (!empty($codigo)) {
            $params = [
                'pagina' => 1,
                'limite' => 1,
                'codigos' => [$codigo]
            ];
        } elseif (!empty($id)) {
            $this->log->write(
                "ModelModuleCodeBling_getProductBling() - Buscando pelo ID do Produto no Bling: "
                . $id
            );

            $codigo = $id;

            $params = [
                'pagina' => 1,
                'limite' => 1,
                'idsProdutos' => [$codigo]
            ];
        } else {
            $this->log->write(
                "ModelModuleCodeBling_getProductBling() - Código/SKU e ID em branco"
            );

            return false;
        }

        if (!empty($cache)
            && !empty($this->codeCache->get('codebling_product_' . $codigo))
        ) {
            $this->log->write(
                "ModelModuleCodeBling_getProductBling() - Usando Cache Produto, SKU ou ID: "
                . $codigo
            );

            return $this->codeCache->get('codebling_product_' . $codigo);
        }

        // https://developer.bling.com.br/referencia#/Produtos/get_produtos
        $url = $this->urlAPI . '/produtos?' . http_build_query($params);

        /*
         * Não pode passar idLoja, pois se o Cadastro do Produto não foi pelo ID da Loja Opencart, não vai
         * retornar o Produto
        if (!empty($this->conf->code_store_id)) {
            $url = $url.'&idLoja='.$this->conf->code_store_id;
        }
        */

        //echo $url;
        //$getProduct = $this->get($url);
        $getProduct = $this->apiCall(
            $url,
            "GET",
            __FUNCTION__ . "_GET",
            [],
            15,
            1
        );
        //print_r($getProduct); exit();

        // Buscando mais detalhes do Produto
        if (empty($getProduct['data'][0]['id'])) {
            $this->log->write(
                "ModelModuleCodeBling_getProductBling() - Sem os dados do Produto com SKU: "
                . $codigo
            );

            return false;
        }

        // https://developer.bling.com.br/referencia#/Produtos/get_produtos__idProduto_
        $url = $this->urlAPI . '/produtos/' . $getProduct['data'][0]['id'];

        //$getProduct = $this->get($url);
        $getProduct = $this->apiCall(
            $url,
            "GET",
            __FUNCTION__ . "_GET",
            [],
            15,
            1
        );
        //print_r($getProduct); exit();

        if (empty($getProduct['data'])) {
            $this->log->write(
                "ModelModuleCodeBling_getProductBling() - Sem os dados completos do Produto com SKU: "
                . $codigo
            );

            return false;
        }

        $getProduct = $getProduct['data'];

        // Colocando no Cache
        $this->codeCache->set('codebling_product_' . $codigo, $getProduct);

        /*
        if ($this->debugLog) {
            $this->log->write(
                'ModelModuleCodeBling_getProductBling_getAPI() - Retorno, URL: '
                .$url
                .' Retorno: '.print_r($getProduct, true)
            );
        }
        */

        return $getProduct;
    }

    /**
     * Busca uma Categoria pelo seu ID
     *
     * @param $id
     *
     * @return false|mixed
     */
    public function getCategoryBling($id)
    {
        if (!$this->status) {
            return false;
        }

        if (!empty($this->cacheCategory->get('codebling_category_' . $id))) {
            $this->log->write(
                "ModelModuleCodeBling_getCategoryBling() - Usando Cache Categoria, ID: "
                . $id
            );

            return $this->cacheCategory->get('codebling_category_' . $id);
        }

        $url = $this->urlAPI . '/categorias/produtos/' . $id;
        //echo $url;
        //$getCategory = $this->get($url);
        $getCategory = $this->apiCall(
            $url,
            "GET",
            __FUNCTION__ . "_GET",
            [],
            15,
            1
        );

        if (!empty($getCategory['data']['id'])) {
            $this->cacheCategory->set(
                'codebling_category_' . $id,
                $getCategory['data']
            );
        }

        return $getCategory['data'];
    }

    /**
     * Adiciona as Opções no Produto Pai/principal para Produtos com Variação no Bling
     * Precisa já estar cadastrada as Opções no Opencart, faz a relação dela
     *
     * @param $productBling Produto vindo do Bling
     * @param $product_id   ID do Produto no Opencart
     */
    public function addOptionsProduct($productBling, $product_id)
    {
        $this->log->write(
            'addOptionsProduct -  Iniciado, produto pai: ' . $product_id
            . ' Código variação: ' . $productBling['codigo']
        );

        /*
         * Temos já o Produto da Variação Bling ID do Produto pai no Opencart
         * Descobrir o nome da Opção e Valor, padrão
         */

        //Descobrindo o Nome e Valor da Opção
        $option = explode(":", $productBling['variacao']['nome']);
        if (empty($option[0]) || empty($option[1])) {
            $this->log->write(
                'addOptionsProduct - Alerta, não está no formato Nome:Valor, ex: Cor:Azul'
            );

            return false;
        }

        $option[0] = explode(" ", $option[0]);
        //$this->log->write('addOptionsProduct - Debug ' . print_r($option, true));
        $option[0] = end($option[0]);
        //$this->log->write('addOptionsProduct - Debug 2' . print_r($option, true));

        //Cor
        $optionName = trim($option[0]);
        //Azul
        $optionValue = trim($option[1]);

        //Descobrir o ID da Opção
        $language_id = $this->languageId();

        // Usado BINARY para er sensibilidade a maiúsculas e minúscula, busca exata
        $query = $this->db->query(
            "SELECT * FROM `" . DB_PREFIX . "option_description`
            WHERE
            BINARY name = '" . $this->db->escape($optionName) . "'
            AND language_id = '" . (int)$language_id . "'
            ORDER BY option_id ASC LIMIT 1
        "
        )->row;

        if (empty($query['option_id'])) {
            $this->log->write(
                'addOptionsProduct - Alerta, não encontrado o ID da Opção, cadastre no Opencart a Opção, exemplo: Cor e valor Azul'
            );

            return false;
        }

        $option_id = $query['option_id'];

        $query = $this->db->query(
            "SELECT * FROM `" . DB_PREFIX . "option_value_description`
            WHERE
            BINARY name = '" . $this->db->escape($optionValue) . "'
            AND option_id = '" . (int)$option_id . "'
            AND language_id = '" . (int)$language_id . "'
            ORDER BY option_value_id ASC LIMIT 1
        "
        )->row;

        if (empty($query['option_value_id'])) {
            $this->log->write(
                'addOptionsProduct - Alerta, não encontrado o valor da Opção, cadastre no Opencart o valor da Opção, ex: Azul, P, M...'
            );

            return false;
        }
        $option_value_id = $query['option_value_id'];

        $this->log->write(
            'addOptionsProduct - Encontrado os option_id: ' . $option_id
            . ' e option_value_id: ' . $option_value_id
        );

        //Verificando se já não tem o product_option
        $query = $this->db->query(
            "SELECT * FROM `" . DB_PREFIX . "product_option`
            WHERE
            option_id = '" . (int)$option_id . "'
            AND product_id = '" . (int)$product_id . "'
            ORDER BY product_option_id ASC LIMIT 1
        "
        )->row;

        $this->log->write('addOptionsProduct - Debug 3' . print_r($query, true));

        if (empty($query['product_option_id'])) {
            $this->db->query(
                "INSERT INTO `" . DB_PREFIX . "product_option`
                SET product_id = '" . (int)$product_id . "',
                option_id = '" . (int)$option_id . "',
                required = 1
            "
            );

            $product_option_id = $this->db->getLastId();

            $this->log->write(
                'addOptionsProduct - Cadastrado o product_optin id: '
                . $product_option_id
            );
        } else {
            $product_option_id = $query['product_option_id'];
        }

        //Salvando o valor da Opção no Produto
        $this->log->write(
            'addOptionsProduct -  Adicionando no Banco, option_value_id: '
            . $option_value_id . ' e product_option_id: ' . $product_option_id
        );

        $quantity = isset($productBling['estoque']['saldoVirtualTotal'])
            ? $productBling['estoque']['saldoVirtualTotal'] : 0;
        $subtract = 1;
        $price = 0;
        $price_prefix = '+';
        $points = 0;
        $points_prefix = '+';
        $weight = 0;
        $weight_prefix = '+';
        $sku = !empty($productBling['codigo']) ? trim($productBling['codigo'])
            : '';
        $sku_column = !empty($this->conf->code_sku_opcao) ? trim(
            $this->conf->code_sku_opcao
        ) : 'code_sku';

        $this->db->query(
            "INSERT INTO " . DB_PREFIX . "product_option_value SET
            product_option_id = '" . (int)$product_option_id . "',
            product_id = '" . (int)$product_id . "',
            option_id = '" . (int)$option_id . "',
            option_value_id = '" . (int)$option_value_id . "',
            quantity = '" . (int)$quantity . "',
            subtract = '" . (int)$subtract . "',
            price = '" . (float)$price . "',
            price_prefix = '" . $this->db->escape($price_prefix) . "',
            points = '" . (int)$points . "',
            points_prefix = '" . $this->db->escape($points_prefix) . "',
            weight = '" . (float)$weight . "',
            weight_prefix = '" . $this->db->escape($weight_prefix) . "',
            " . $sku_column . " = '" . $this->db->escape($sku) . "'
        "
        );

        $product_option_value_id = $this->db->getLastId();

        //Usado no ID da Opção produto_idvproduto_opcao_id
        $id = $product_id . 'v' . $product_option_value_id;
        $this->log->write(
            'addOptionsProduct -  Adicionado no Banco a Opção, ID retornado: '
            . $id
        );

        return ['option_id' => $id];
    }

    /*
     * Busca o Produto no Bling pelo Código
     *
     */

    /**
     * Retorna um Pedido no Bling pelo seu ID na Loja
     * https://developer.bling.com.br/referencia#/Pedidos%20-%20Vendas/get_pedidos_vendas
     *
     * @param int $order_id = ID do Pedido na Loja
     *
     * @return false|mixed
     */
    private function getOrderBling(int $order_id)
    {
        //$order_id = 11;
        $url = $this->urlAPI . "/pedidos/vendas?pagina=1&limite=1&numerosLojas%5B%5D=" . $order_id;

        //$order = $this->get($url);
        $order = $this->apiCall(
            $url,
            "GET",
            __FUNCTION__ . "_GET",
            [],
            15,
            1
        );

        //echo $url;
        //print_r($order); exit();

        if (!empty($order['data'][0]['id'])) {
            return $order['data'][0];
        }

        return false;
    }

    /**
     * Retorna os dados do Estoque de um Produto
     * https://developer.bling.com.br/referencia#/Estoques/get_estoques_saldos
     *
     * @param int $product_id_bling ID do Produto no Bling ERP
     *
     * @return false|mixed
     */
    private function getStockProductBling(int $product_id_bling)
    {
        $params = [
            'idsProdutos' => [$product_id_bling]
        ];

        $url = $this->urlAPI . '/estoques/saldos?' . http_build_query($params);
        //$url = $this->urlAPI . '/estoques/saldos?idsProdutos%5B%5D=' . $product_id_bling;

        //$response = $this->get($url);

        $response = $this->apiCall(
            $url,
            "GET",
            __FUNCTION__ . "_GET",
            [],
            15,
            1
        );

        /*
      * {
 "data": [
     {
         "produto": {
             "id": 16355856232,
             "codigo": "Opencart-Bling1"
         },
         "saldoFisicoTotal": 500,
         "saldoVirtualTotal": 500,
         "depositos": [
             {
                 "id": 14887789788,
                 "saldoFisico": 500,
                 "saldoVirtual": 500
             }
         ]
     }
 ]
}
      */

        if (!empty($response['data'][0])) {
            return $response;
        }

        return false;
    }

    /**
     * Muda a quantidade de Estoque de um Produto no seu Estoque
     * https://developer.bling.com.br/referencia#/Estoques/put_estoques__idEstoque_
     *
     * @param $stock_id
     * @param $quantity
     *
     * @return void
     */
    private function updateStockProductBling($stock_id, $quantity)
    {
        /*
         * Operação
            B Balanço
            E Entrada
            S Saída
         */

        $data = [
            'operacao' => 'B',
            'quantidade' => $quantity,
            'observacoes' => 'Editado pela Loja: ' . $this->config->get('config_name'),
            'data' => date('Y-m-d H:i:s'),
        ];

        $url = $this->urlAPI . '/estoques/' . $stock_id;
        //print_r($data);
        //echo $url; exit();
        //$response = $this->post($data, $url, 'PUT');

        $response = $this->apiCall(
            $url,
            "PUT",
            __FUNCTION__ . "_PUT",
            $data,
            15,
            1
        );

        if (!empty($response['error'])) {
            $this->log->write(
                'updateStockProduct() - Não atualizado o Estoque: ' . $stock_id . ', retorno: ' . print_r(
                    $response,
                    true
                )
            );
            echo '<h3>Não atualizado o Estoque: ' . $stock_id . '</h3>';
            print_r($response);
            return false;
        }

        return true;
    }

    /**
     * Retorna um Pedido no Bling pelo seu ID no Bling ERP
     * https://developer.bling.com.br/referencia#/Pedidos%20-%20Compras/get_pedidos_compras__idPedidoCompra_
     * Pegava para o ID do Pedido na Loja na API V2
     *
     * @param $order_id_bling = ID do Pedido no Bling ERP
     *
     * @return false|mixed
     */
    private function getOrderIDBling($order_id_bling)
    {
        //$order_id = 11;
        $url = $this->urlAPI . "/pedidos/vendas/" . $order_id_bling;
        //$order = $this->get($url);
        $order = $this->apiCall(
            $url,
            "GET",
            __FUNCTION__ . "_GET",
            [],
            15,
            1
        );

        if (!empty($order['data']['id'])) {
            return $order['data'];
        }

        return false;
    }

    /**
     * Retorna o código da Zona/Estado com base no Nome, exemplo: São Paulo retorna SP
     *
     * @param $name
     *
     * @return mixed|string
     */
    private function getZoneCode($name)
    {
        $zone = $this->db->query(
            "
            SELECT code FROM " . DB_PREFIX . "zone
            WHERE
            name = '" . $this->db->escape($name) . "' AND
            status = 1
            LIMIT 1
        "
        )->row;

        return !empty($zone['code']) ? $zone['code'] : '';
    }

    /**
     * Cria/Atualiza o Cliente no Bling ERP
     * https://developer.bling.com.br/referencia#/Contatos/post_contatos
     *
     * @param $order         Dados do Pedido tratados pelo App
     * @param $orderNative   Dados padrões do Pedido
     * @param $zone          Zona/Estado
     * @param $codigo_prefix Prefixo do Contato
     *
     * @return bool|mixed
     */
    private function addCustomerByOrderBling(
        $order,
        $orderNative,
        $zone,
        $codigo_prefix
    ) {
        echo '<h2>Cadastrando/Atualizando o Cliente: ' . $order['customer_id'] . '</h2>';

        // Verificando se já tem o cliente pelo seu CPF/CNPJ
        $verify = $this->getCustomerBling($this->onlyNumber($order['cpf_cnpj']));
        //print_r($verify);exit();

        // Cadastrando o cliente primeiro https://developer.bling.com.br/referencia#/Contatos/post_contatos
        $url = $this->urlAPI . '/contatos';
        $data = [];

        if (!empty($this->request->get['use_customer_id'])
            && (int)$this->request->get['use_customer_id'] === 1
        ) {
            $codigo = $codigo_prefix . $orderNative['customer_id'];
            $data['codigo'] = $codigo;
        }

        $data['nome'] = trim($order['name']);
        $data['fantasia'] = trim($order['name']);

        /*
         *  Situação do contato
            A Ativo
            E Excluído
            I Inativo
            S Sem movimentação
         */
        $data['situacao'] = 'A';
        $data['numeroDocumento'] = $this->onlyNumber($order['cpf_cnpj']);
        //$data['ie'] = trim($order['ie']);
        //$data['rg'] = trim($order['rg']);
        $data['tipo'] = trim($order['persontype']);
        $data['email'] = trim($order['email']);

        if (!empty($order['payment']['payment_cellphone'])) {
            $data['telefone'] = trim($order['payment']['payment_cellphone']);
            $data['celular'] = trim($order['payment']['payment_cellphone']);
        }

        /*
        if (empty($verify)) {
            //$data['emailNfe'] = trim($order['email']);
            // Sem o recurso na API V3
        }
        */

        $number = !empty($order['payment']['payment_number_address']) ? trim(
            $order['payment']['payment_number_address']
        ) : 'Não informado';

        $data['endereco'] = [
            'geral' => [
                'endereco' => $order['payment']['payment_address_1'],
                'numero' => $number,
                'complemento' => $order['payment']['payment_address_2'],
                'bairro' => $order['payment']['payment_neighborhood'],
                'cep' => $order['payment']['payment_postcode'],
                'municipio' => $order['payment']['payment_city'],
                'uf' => $zone,

            ],
            // Replica do acima
            'cobranca' => [
                'endereco' => $order['payment']['payment_address_1'],
                'numero' => $number,
                'complemento' => $order['payment']['payment_address_2'],
                'bairro' => $order['payment']['payment_neighborhood'],
                'cep' => $order['payment']['payment_postcode'],
                'municipio' => $order['payment']['payment_city'],
                'uf' => $zone,

            ]
        ];

        $data['pais'] = [
            'nome' => 'Brasil'
        ];

        /*
         * Removido informacaoContato $this->request->get['information_contact'], usado quando o verify estava vazio
         * Analisar o uso do $this->request->get['type_contact'] em tipos de Contato, usado quando o verifiy estava
         * vazio
         *
         * Mudou demais na API V3, ficou:
         *  "contato": {
    "id": 12345678,
    "tipoPessoa": "J",
    "numeroDocumento": "30188025000121"
  },
         */

        /*
        if (!empty($this->request->get['type_contact']) && empty($verify)) {
            $xml .= '  <tipos_contatos>';
            $xml .= '  <tipo_contato>';
            $xml .= '  <descricao>'.trim($this->request->get['type_contact'])
                .'</descricao>';
            $xml .= '  </tipo_contato>';
            $xml .= '  </tipos_contatos>';
        }
        */

        $data['dadosAdicionais'] = [
            //'dataNascimento' => '"1990-08-24',
            //'sexo' => 'M',
            'naturalidade' => 'Brasileira',
        ];

        /*
        // Exibindo dados, modo de teste
        $this->viewData($data);
        */

        // Verificando se é para atualizar
        if (!empty($verify['id'])) {
            // https://developer.bling.com.br/referencia#/Contatos/put_contatos__idContato_
            // Alterando um contato
            $url .= '/' . $verify['id'];
            //$response = $this->post($data, $url, 'PUT');
            //print_r(json_encode($data)); exit();

            $response = $this->apiCall(
                $url,
                "PUT",
                __FUNCTION__ . "_PUT",
                $data,
                15,
                1
            );

            $response = $this->responseAddCustomerByOrderBling($orderNative['order_id'], $response, $url, $data, false);

            // O response do PUT chega vazio, porém sem erro quando concluído com sucesso e HTTP 204
            if ($response) {
                return $verify['id'];
            }

            return false;
        }

        //$response = $this->post($data, $url);
        $response = $this->apiCall(
            $url,
            "POST",
            __FUNCTION__ . "_POST",
            $data,
            15,
            1
        );

        return $this->responseAddCustomerByOrderBling($orderNative['order_id'], $response, $url, $data, true);
    }

    /**
     * Trata o retorno do addCustomerByOrderBling() e é chamado por ele
     *  Em caso de erro e for problema ligado ao Código, faz mais uma tentativa mudando o Código do Cliente
     *
     * @param int    $order_id ID do Pedido na Loja
     * @param array  $response Resposta do Request
     * @param string $url      URL do Request
     * @param array  $data     Dados do Request
     * @param bool   $insert   Se true é para o Cadastro, se não Atualização
     *
     * @return bool|mixed
     */
    private function responseAddCustomerByOrderBling(
        int $order_id,
        array $response,
        string $url,
        array $data,
        bool $insert = true
    ) {
        if (!empty($response['error'])) {
            if (empty($data['codigo']) || (!empty($response['error']['fields'][0]['element']) && $response['error']['fields'][0]['element'] != 'codigo')) {
                if ($insert) {
                    $this->log->write(
                        "responseAddCustomerByOrderBling() - Falhou o Cadastro do Cliente do Pedido: " . $order_id . ", Erros: " . print_r
                        (
                            $response['error'],
                            true
                        )
                    );
                } else {
                    $this->log->write(
                        "responseAddCustomerByOrderBling() - Falhou a Atualização do Cliente do Pedido: " . $order_id .
                        ", Erros: " . print_r
                        (
                            $response['error'],
                            true
                        )
                    );
                }

                return false;
                //exit('PARANDO O CÓDIGO');
            } else {
                echo "<h3>Nova Tentativa - Gerando com um código aleatório ao lado do ID do Cliente</h3>";

                // Coloca 3 caracteres depois do código
                $data['codigo'] = $data['codigo'] . '-' . substr(bin2hex(random_bytes(3)), 0, 3);
                // Tentando de novo com outro Código

                if ($insert) {
                    $response = $this->apiCall(
                        $url,
                        "POST",
                        "addCustomerByOrderBling_Retentativa_POST",
                        $data,
                        15,
                        1
                    );
                } else {
                    $response = $this->apiCall(
                        $url,
                        "PUT",
                        "addCustomerByOrderBling_Retentativa_PUT",
                        $data,
                        15,
                        1
                    );
                }

                if (!empty($response['error'])) {
                    if ($insert) {
                        $this->log->write(
                            "responseAddCustomerByOrderBling() - Falhou a tentativa 2 do Cadastro do Cliente do Pedido: " . $order_id . ", Erros: " . print_r
                            (
                                $response['error'],
                                true
                            )
                        );
                    } else {
                        $this->log->write(
                            "responseAddCustomerByOrderBling() - Falhou a tentativa 2 da Atualização do Cliente do Pedido: " .
                            $order_id .
                            ", Erros: " . print_r
                            (
                                $response['error'],
                                true
                            )
                        );
                    }

                    return false;
                    //exit('PARANDO O CÓDIGO');
                }
            }
        }

        if (!empty($response['data']['id'])) {
            $this->log->write(
                "responseAddCustomerByOrderBling() - Rodado com sucesso o Cadastro do Cliente, Pedido: " .
                $order_id .
                ", ID do Cliente no Bling" . $response['data']['id']
            );

            return $response['data']['id'];
        }

        // A atualização não retorna dados no sucesso, porém sem erro quando concluído com sucesso e HTTP 204
        if ($insert === false) {
            return true;
        }

        return false;
    }

    /**
     * Buscar um cliente no Bling pelo seu CPF ou CNPJ
     * https://developer.bling.com.br/referencia#/Contatos/get_contatos
     *
     * @param $cpf_cnpj CPF ou CNPJ do cliente
     *
     * @return false|mixed
     */
    private function getCustomerBling($cpf_cnpj)
    {
        $url = $this->urlAPI . '/contatos?pagina=1&limit=1&numeroDocumento=' . $cpf_cnpj;

        //$customer = $this->get($url);
        $customer = $this->apiCall(
            $url,
            "GET",
            __FUNCTION__ . "_GET",
            [],
            15,
            1
        );

        //print_r($customer); exit();
        if (!empty($customer['data'][0]['id'])) {
            return $customer['data'][0];
        }

        return false;
    }

    /**
     * Salva os dados de Pagamento
     *
     * @param $order_id
     * @param $data
     * @param $service
     *
     * @return bool
     */
    private function salvarDadosNF($order_id, $data, $service)
    {
        //Precisa vir os dados payment, order_id, link e barCode
        if (empty($data['id']) || empty($data['qrCodeLink'])
            || empty($data['deepLink'])
        ) {
            //$this->log->write('salvarDadosNF() - Erro sem os dados para salvar o Pagamento');
            //return false;
        }

        $consulta = $this->db->query(
            "
            SELECT code_nf_id FROM " . DB_PREFIX . "code_nf
            WHERE
            order_id = '" . (int)$order_id . "' AND
            service = '" . $this->db->escape($service) . "'
            LIMIT 1
        "
        )->row;

        if (!empty($consulta['code_nf_id'])) {
            $this->db->query(
                "
                UPDATE " . DB_PREFIX . "code_nf
                SET
                data = '" . json_encode($data) . "',
                date_created = NOW()
                WHERE
                code_nf_id = '" . (int)$consulta['code_nf_id'] . "'
            "
            );
        } else {
            $this->db->query(
                "
                INSERT INTO " . DB_PREFIX . "code_nf
                SET
                order_id = '" . (int)$order_id . "',
                data = '" . json_encode($data) . "',
                service = '" . $this->db->escape($service) . "',
                date_created = NOW()
            "
            );
        }

        $this->log->write(
            "salvarDadosNF() - Salvo os dados do Pagamento, serviço: " . $service
            . " Pedido: " . $order_id
        );

        return true;
    }

    /**
     * Consulta os dados do Pagamento ou o Status dos serviços
     *
     * @param $order_id
     * @param $service
     *
     * @return false|array
     */
    private function consultarDadosNF($order_id, $service)
    {
        $consulta = $this->db->query(
            "
            SELECT * FROM " . DB_PREFIX . "code_nf
            WHERE
            order_id = '" . (int)$order_id . "' AND
            service =   '" . $this->db->escape($service) . "'
            LIMIT 1
        "
        )->row;

        $this->log->write(
            "consultarDadosNF() - Consultando o Pedido: " . $order_id
            . ", Serviço: " . $service
        );

        if (empty($consulta['data'])) {
            $this->log->write("consultarDadosNF() - Sem dados retornados");

            return false;
        }

        return json_decode($consulta['data'], true);
    }

    /**
     * Salva no banco de dados o Status do Pedido
     * Bling -> Opencart
     *
     * @param int    $order_id
     * @param int    $order_status_id
     * @param string $comment
     * @param bool   $verifyStatus
     *
     * @return true
     */
    private function addOrderHistoryVerify(
        int $order_id,
        int $order_status_id,
        string $comment = '',
        bool $verifyStatus = true
    ): bool {
        $order = $this->db->query(
            "SELECT order_id, order_status_id, currency_code  FROM `" . DB_PREFIX . "order`
            WHERE order_id = '" . (int)$order_id . "' LIMIT 1
        "
        )->row;

        if (empty($order['order_id'])) {
            echo "<h3>Consultando banco, não encontrado o Pedido na Loja: "
                . $order_id . " - Horário: " . date(
                    'i:s'
                ) . "</h3>";

            $this->log->write(
                "ModelModuleCodeBling_addOrderHistoryVerify() - Não achou o Pedido na Loja, ID Pedido Loja: "
                . $order_id
            );

            return true;
        }

        $query = $this->db->query(
            "SELECT order_id FROM " . DB_PREFIX . "order_history WHERE order_id = '"
            . (int)
            $order_id . "' AND order_status_id = '" . (int)$order_status_id . "'"
        );

        if (!empty($query->row['order_id']) && $verifyStatus == true) {
            echo "<h3>Pedido: " . $order_id . " e Status " . $order_status_id
                . ", já notificado - Horário: " . date(
                    'i:s'
                ) . "</h3>";

            $this->log->write(
                "ModelModuleCodeBling_addOrderHistoryVerify() - Pedido: "
                . $order_id . " e Status " . $order_status_id . ", já notificado"
            );

            return true;
        }

        if (!empty($this->conf->code_alertar_status)
            and $this->conf->code_alertar_status == 1
        ) {
            $status_alertar = true;
        } else {
            $status_alertar = false;
        }

        // Mudando no Pedido o Status
        $this->model_checkout_order->addOrderHistory(
            (int)$order_id,
            (int)$order_status_id,
            $comment,
            $status_alertar
        );

        echo "<h3>Pedido: " . $order_id . " e Status " . $order_status_id
            . ", notificado com sucesso - Horário: " . date(
                'i:s'
            ) . "</h3>";

        $this->log->write(
            "ModelModuleCodeBling_addOrderHistoryVerify() - Pedido: " . $order_id
            . " e Status " . $order_status_id . ", notificado com sucesso"
        );

        return true;
    }

    /**
     * Retorna uma String com apenas número
     *
     * @param $string
     *
     * @return string
     */
    private function onlyNumber($string)
    {
        return preg_replace("/[^0-9]/", "", trim($string));
        //return trim($string);
    }

    /**
     * Converte o peso para Kg
     *
     * @param $weight
     * @param $weight_class
     *
     * @return mixed
     */
    private function convertWeight($weight, $weight_class)
    {
        $weight_class_id = !empty($this->conf->code_weight_kg)
            ? (int)$this->conf->code_weight_kg : 1;

        return $this->weight->convert($weight, $weight_class, $weight_class_id);
    }

    /**
     * Converte as Dimensões para cm
     *
     * @param $length
     * @param $length_class
     *
     * @return mixed
     */
    private function convertLength($length, $length_class)
    {
        $length_class_id = !empty($this->conf->code_length_cm)
            ? (int)$this->conf->code_length_cm : 1;

        return $this->length->convert($length, $length_class, $length_class_id);
    }

    /**
     * Salvando no banco as alterações do Produto
     * Bling -> Opencart
     *
     * @param $productBling    Produto no Bling
     * @param $product_id      Id do Produto na Loja
     *
     * @return bool
     * @uses    addPriceProductStoreSQL Atualizado o Preço e Promoção do Produto
     * @used-by updateProductStore Atualizado os dados do Produto
     */
    private function updateProdutoStoreSQL($productBling, $product_id)
    {
        if (!$this->status) {
            return false;
        }

        $this->log->write(
            'ModelModuleCodeBling_updateProdutoStoreSQL() - Rodando - Produto ID: '
            . $product_id
        );
        //$this->log->write('ModelModuleCodeBling_updateProdutoStoreSQL() - Produto dados: ' . print_r($productBling, true));

        // Inserindo a Categoria e relacionando com o Produto
        $this->insertCategoryStore($productBling, $product_id);

        $descriptionHTML = !empty($productBling['descricaoCurta']) ? trim(
            urldecode($productBling['descricaoCurta'])
        ) : trim(urldecode($productBling['descricaoComplementar']));

        $title = trim($productBling['nome']);
        $model = trim($productBling['codigo']);
        if (!empty($this->conf->code_produto_modelo)
            && !empty(
            $productBling[trim(
                $this->conf->code_produto_modelo
            )]
            )
        ) {
            $model = trim($productBling[trim($this->conf->code_produto_modelo)]);
        }
        $model = substr($model, 0, 64);
        $location = !empty($productBling['estoque']['localizacao']) ? substr(
            trim($productBling['estoque']['localizacao']),
            0,
            64
        ) : '';

        $sqlProduct = "UPDATE " . DB_PREFIX . "product SET
            model = '" . $this->db->escape($model) . "'
        ";

        // Adicionando a Localização
        if (!empty($location)) {
            $sqlProduct .= ", location =   '" . $this->db->escape($location) . "' ";
        }

        // Adicionando a Observação
        $obs_column = !empty($this->conf->code_produto_observacao) ? trim(
            $this->conf->code_produto_observacao
        ) : '';
        if (!empty($productBling['observacoes']) && !empty($obs_column)) {
            $sqlProduct .= ", " . $obs_column . " =   '" . $this->db->escape(
                    trim($productBling['observacoes'])
                ) . "' ";
        }

        // Adicionando o GTIN/EAN
        $ean_column = !empty($this->conf->code_produto_gtin) ? trim(
            $this->conf->code_produto_gtin
        ) : '';
        if (!empty($productBling['gtin']) && !empty($ean_column)) {
            $sqlProduct .= ", " . $ean_column . " =   '" . $this->db->escape(
                    trim($productBling['gtin'])
                ) . "' ";
        }

        // Adicionando Tributação NCM
        $ncm_column = !empty($this->conf->code_produto_ncm) ? trim(
            $this->conf->code_produto_ncm
        ) : '';
        if (!empty($productBling['tributacao']['ncm']) && !empty($ncm_column)) {
            $sqlProduct .= ", " . $ncm_column . " =   '" . $this->db->escape(
                    trim($productBling['tributacao']['ncm'])
                ) . "' ";
        }

        // Adicionando o CEST
        $cest_column = !empty($this->conf->code_produto_cest) ? trim(
            $this->conf->code_produto_cest
        ) : '';
        if (!empty($productBling['tributacao']['cest']) && !empty($cest_column)) {
            $sqlProduct .= ", " . $cest_column . " =   '" . $this->db->escape(
                    trim($productBling['tributacao']['cest'])
                ) . "' ";
        }

        $product_class = $this->db->query(
            "SELECT weight_class_id, length_class_id FROM " . DB_PREFIX . "product
            WHERE product_id = '" . (int)$product_id . "' LIMIT 1
        "
        )->row;

        // Atualizando o peso
        if (!empty($productBling['pesoLiquido'])
            && !empty($product_class['weight_class_id'])
        ) {
            $sqlProduct .= ", weight = '" . $this->db->escape(
                    $this->convertWeightBling(
                        $productBling['pesoLiquido'],
                        $product_class['weight_class_id']
                    )
                ) . "' ";
        }

        //Atualizando as dimensões
        if (!empty($productBling['dimensoes']['profundidade'])
            && !empty($productBling['dimensoes']['largura'])
            && !empty($productBling['dimensoes']['altura'])
            && !empty($product_class['length_class_id'])
        ) {
            $sqlProduct .= ", length = '" . $this->db->escape(
                    $this->convertLengthBling(
                        $productBling['dimensoes']['profundidade'],
                        $product_class['length_class_id']
                    )
                ) . "' ";
            $sqlProduct .= ", width = '" . $this->db->escape(
                    $this->convertLengthBling(
                        $productBling['dimensoes']['largura'],
                        $product_class['length_class_id']
                    )
                ) . "' ";
            $sqlProduct .= ", height = '" . $this->db->escape(
                    $this->convertLengthBling(
                        $productBling['dimensoes']['altura'],
                        $product_class['length_class_id']
                    )
                ) . "' ";
        }

        $sqlProduct .= "WHERE product_id = '" . (int)$product_id . "' ";
        //$this->log->write('ModelModuleCodeBling_updateProdutoStoreSQL() - SQL gerado: ' . $sqlProduct);
        $this->db->query($sqlProduct);

        $this->log->write(
            'ModelModuleCodeBling_updateProdutoStoreSQL() - Atualizado - Produto ID: '
            . $product_id
        );
        $language_id = $this->languageId();

        //Título, Descrição em HTML e Meta Título
        foreach ($this->languages as $l) {
            $this->db->query(
                "UPDATE " . DB_PREFIX . "product_description SET
                name = '" . $this->db->escape($title) . "',
                description = '" . $this->db->escape($descriptionHTML) . "',
                meta_title =   '" . $this->db->escape($title) . "'
                WHERE product_id = '" . (int)$product_id . "' AND
                language_id = '" . (int)$l['language_id'] . "'
            "
            );
        }

        //SEO URL
        if (version_compare(VERSION, '3.0.0.0', '>=')) {
            foreach ($this->languages as $l) {
                $this->db->query(
                    "DELETE FROM " . DB_PREFIX
                    . "seo_url WHERE query = 'product_id=" . (int)$product_id
                    . "' AND language_id =  '" . (int)$l['language_id'] . "'"
                );

                //Se a linguagem for diferente de PT-BR adicionar o code no slug
                //$this->log->write('ModelModuleCodeBling_updateProdutoStoreSQL() - language_id Brasil: ' . $language_id . ', language_id: ' . $l['language_id']);
                $slug = $this->slug($title);

                if ($language_id != $l['language_id']) {
                    $slug = $slug . "-" . $l['code'];
                }
                $this->log->write(
                    'ModelModuleCodeBling_updateProdutoStoreSQL() - Slug:' . $slug
                );

                $this->db->query(
                    "INSERT INTO " . DB_PREFIX . "seo_url
                   SET
                   store_id = 0,
                   language_id = '" . (int)$l['language_id'] . "',
                   query = 'product_id=" . (int)$product_id . "',
                   keyword = '" . $this->db->escape($slug) . "'
               "
                );
            }
        } else {
            $slug = $this->slug($title);
            $this->log->write(
                'ModelModuleCodeBling_updateProdutoStoreSQL() - Slug:' . $slug
            );
            $this->db->query(
                "DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id="
                . (int)$product_id . "'"
            );
            $this->db->query(
                "INSERT INTO " . DB_PREFIX . "url_alias
               SET
               query = 'product_id=" . (int)$product_id . "',
               keyword = '" . $this->db->escape($slug) . "'
           "
            );
        }

        //Salvando na Loja 0 o Produto
        $query = $this->db->query(
            "SELECT product_id FROM " . DB_PREFIX
            . "product_to_store WHERE product_id = '" . (int)$product_id
            . "' AND store_id = 0 LIMIT 1"
        );
        if (empty($query->row['product_id'])) {
            $this->db->query(
                "INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '"
                . (int)$product_id . "', store_id = 0"
            );
        }

        /*
       // Várias lojas
       $stores = $this->db->query("SELECT store_id FROM " . DB_PREFIX . "store
           ORDER BY store_id ASC
       ")->rows;

       if(empty($stores['0']['store_id'])){
           //Salvando na Loja 0 o Produto
           $query = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int) $product_id . "' AND store_id = 0 LIMIT 1");
           if (empty($query->row['product_id'])) {
               $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int) $product_id . "', store_id = 0");
           }
       }else{
           //Várias lojas
           foreach ($$stores as $store) {
               $query = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int) $product_id . "' AND store_id =  '" . (int) $store['store_id'] . "' LIMIT 1");

               if (empty($query->row['product_id'])) {
                   $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int) $product_id . "', store_id =  '" . (int) $store['store_id'] . "' ");
               }
           }
       }
       */

        // Mudando o Preço e Promoção do Produto
        $this->addPriceProductStoreSQL($productBling, $product_id);

        $this->log->write(
            'ModelModuleCodeBling_updateProdutoStoreSQL() - Rodado - Produto ID: '
            . $product_id
        );

        return true;
    }

    /**
     * Cada imagem é salva novamente nos arquivos e alterada no produto
     * Na tabela product_image é removida as imagens do produto e salvo novamente as referências
     * Leva 1-2s por imagem salva, ideal evitar mais de 30 imagens
     *
     * @param $title
     * @param $images
     * @param $product_id
     *
     * @return bool
     */
    private function saveImageStore($title, $images, $product_id)
    {
        if (!$this->status) {
            return false;
        }

        if (empty($title) || empty($images)) {
            return false;
        }

        $this->log->write(
            'ModelModuleCodeBling_saveImageStore() - Rodando - Produto ID: '
            . $product_id
        );

        if (!empty($this->conf->code_image_importar)
            && $this->conf->code_image_importar == 2
        ) {
            $this->log->write(
                'ModelModuleCodeBling_saveImageStore() - Configurado para não importar imagens'
            );

            return false;
        }

        $title = $this->slug($title);

        $folder = 'catalog/bling/';
        @mkdir(DIR_IMAGE . $folder, 0755);
        $path = DIR_IMAGE . $folder;

        $image_types_allowed = [
            'image/gif' => '.gif',
            'image/jpeg' => '.jpg',
            'image/png' => '.png',
            'image/webp' => '.webp',
        ];

        foreach ($images as $key => $img) {
            if (empty($img['link'])) {
                if ($this->debugLog) {
                    $this->log->write(
                        'ModelModuleCodeBling_saveImageStore() - Sem o Link da imagem, '
                        . print_r($img, true)
                    );
                }
                continue;
            }

            $url = $img['link'];
            $extension = '';

            //$type = image_type_to_mime_type( exif_imagetype( $url ) );
            //print_r($image_info);
            /*
            if (empty($image_info['mime']) || !in_array($image_info['mime'], $image_types_allowed)) {
                continue;
            }*/

            /*
            // Modo anterior, tem poucas Hospedagens que não pega
            $image_info = getimagesize($url);

            foreach ($image_types_allowed as $type => $ext) {
                if ($type == $image_info['mime']) {
                    $extension = $ext;
                }
            }

            if (empty($extension)) {
                if ($this->debugLog) {
                    $this->log->write('ModelModuleCodeBling_saveImageStore() - Extensão inválida, image_info: ' . print_r($image_info, true));
                }
               continue;
            }
            */

            $imgDownload = $this->getImage($url);

            if (empty($imgDownload['image'])) {
                if ($this->debugLog) {
                    $this->log->write(
                        'ModelModuleCodeBling_saveImageStore() - Não conseguiu baixar a imagem, url: '
                        . print_r(
                            $url,
                            true
                        )
                    );
                }
                continue;
            }

            // Verificando a extensão
            foreach ($image_types_allowed as $type => $ext) {
                if (!empty($imgDownload['type'])
                    && $type == $imgDownload['type']
                ) {
                    $extension = $ext;
                    break;
                }
            }

            if (empty($extension)) {
                if ($this->debugLog) {
                    $this->log->write(
                        'ModelModuleCodeBling_saveImageStore() - Extensão inválida, imgDownload: '
                        . print_r(
                            $imgDownload,
                            true
                        )
                    );
                }
                continue;
            }

            file_put_contents(
                $path . $title . $key . $extension,
                $imgDownload['image']
            );
            $filePath = $folder . $title . $key . $extension;

            if ($key == 0) {
                //Primeira imagem, usar como capa
                $this->db->query(
                    "
                    UPDATE " . DB_PREFIX . "product SET
                    image = '" . $this->db->escape($filePath) . "'
                    WHERE product_id = '" . (int)$product_id . "'"
                );

                if ($this->debugLog) {
                    $this->log->write(
                        'ModelModuleCodeBling_saveImageStore() - Salvo a imagem do Produto: '
                        . $product_id . ', Imagem: ' . $filePath
                    );
                }
            } else {
                /**
                 * Remover a tabela das imagens
                 * Inserir no banco
                 */

                if ($key == 1) {
                    $this->db->query(
                        "DELETE FROM " . DB_PREFIX
                        . "product_image WHERE product_id = '" . (int)$product_id
                        . "'"
                    );
                }

                $this->db->query(
                    "INSERT INTO " . DB_PREFIX . "product_image SET
                    product_id = '" . (int)$product_id . "',
                    image = '" . $this->db->escape($filePath) . "',
                    sort_order = '" . (int)$key . "'
                "
                );

                if ($this->debugLog) {
                    $this->log->write(
                        'ModelModuleCodeBling_saveImageStore() - Salvo as imagens do Produto: '
                        . $product_id . ', Imagem: ' . $filePath
                    );
                }
            }
        }

        $this->log->write(
            'ModelModuleCodeBling_saveImageStore() - Atualizado imagens - Produto ID: '
            . $product_id
        );

        return true;
    }

    /**
     * Adiciona a categoria pai e filha na Loja
     * Usa o addCategory()
     *
     * @param $product
     * @param $product_id
     *
     * @return bool
     */
    private function insertCategoryStore($product, $product_id)
    {
        // CATEGORIA
        if (empty($product['categoria']['id'])) {
            return false;
        }

        $getCategory = $this->getCategoryBling($product['categoria']['id']);
        /*
          [id] => 5613559
        [descricao] => Categoria padrão
        [categoriaPai] => Array
        (
            [id] => 0
        )
         */

        if ($this->debugLog) {
            $this->log->write(
                'ModelModuleCodeBling_insertCategoryStore() - Categoria - '
                . print_r($getCategory, true)
            );
        }

        // Consultando a categoria na Loja, se tiver o categoriaPai procurar por ele e inserir também
        $categoryParent = !empty($getCategory['categoriaPai']['id'])
            ? (int)$getCategory['categoriaPai'][['id']] : false;
        $categoryParentId = 0;

        if ($categoryParent) {
            $getCategory2 = $this->getCategoryBling($categoryParent);
            if ($this->debugLog) {
                $this->log->write(
                    'ModelModuleCodeBling_insertCategoryStore() - Categoria Pai - '
                    . print_r($getCategory2, true)
                );
            }

            $categoryParentId = $this->addCategory($product_id, $getCategory2);
        }

        $this->addCategory($product_id, $getCategory, $categoryParentId);

        return true;
    }

    /**
     * Converte o peso para Kg
     *
     * @param $weight
     * @param $weight_class
     *
     * @return mixed
     */
    private function convertWeightBling($weight, $weight_class)
    {
        $weight_class_id = !empty($this->conf->code_weight_kg)
            ? (int)$this->conf->code_weight_kg : 1;

        return $this->weight->convert($weight, $weight_class_id, $weight_class);
    }

    /**
     * Converte as Dimensões para cm
     *
     * @param $length
     * @param $length_class
     *
     * @return mixed
     */
    private function convertLengthBling($length, $length_class)
    {
        $length_class_id = !empty($this->conf->code_length_cm)
            ? (int)$this->conf->code_length_cm : 1;

        return $this->length->convert($length, $length_class_id, $length_class);
    }

    /**
     * AUXILIARES SLUG, POST E GET
     */

    /**
     * Exibe os Dados
     *
     * @param $data Dados a serem exibidos
     * @param $stop Se true vai parar o código
     *
     * @return void
     */
    private function viewData($data, $stop = true)
    {
        // Exibindo dados, modo de teste
        if (!empty($this->viewData)) {
            echo "<h2>Exibindo os dados</h2>";
            print_r($data);

            echo '<h2>Json</h2>';
            print_r(json_encode($data));

            if ($stop) {
                exit('PARANDO O CÓDIGO');
            }
        }
    }

    /** Cria o Slug
     *
     * @param        $str
     * @param array  $replace
     * @param string $delimiter
     *
     * @return string $slug
     */
    private function slug($str, $replace = [], $delimiter = '-')
    {
        $table = [
            'Š' => 'S',
            'š' => 's',
            'Đ' => 'Dj',
            'đ' => 'dj',
            'Ž' => 'Z',
            'ž' => 'z',
            'Č' => 'C',
            'č' => 'c',
            'Ć' => 'C',
            'ć' => 'c',
            'À' => 'A',
            'Á' => 'A',
            'Â' => 'A',
            'Ã' => 'A',
            'Ä' => 'A',
            'Å' => 'A',
            'Æ' => 'A',
            'Ç' => 'C',
            'È' => 'E',
            'É' => 'E',
            'Ê' => 'E',
            'Ë' => 'E',
            'Ì' => 'I',
            'Í' => 'I',
            'Î' => 'I',
            'Ï' => 'I',
            'Ñ' => 'N',
            'Ò' => 'O',
            'Ó' => 'O',
            'Ô' => 'O',
            'Õ' => 'O',
            'Ö' => 'O',
            'Ø' => 'O',
            'Ù' => 'U',
            'Ú' => 'U',
            'Û' => 'U',
            'Ü' => 'U',
            'Ý' => 'Y',
            'Þ' => 'B',
            'ß' => 'Ss',
            'à' => 'a',
            'á' => 'a',
            'â' => 'a',
            'ã' => 'a',
            'ä' => 'a',
            'å' => 'a',
            'æ' => 'a',
            'ç' => 'c',
            'è' => 'e',
            'é' => 'e',
            'ê' => 'e',
            'ë' => 'e',
            'ì' => 'i',
            'í' => 'i',
            'î' => 'i',
            'ï' => 'i',
            'ð' => 'o',
            'ñ' => 'n',
            'ò' => 'o',
            'ó' => 'o',
            'ô' => 'o',
            'õ' => 'o',
            'ö' => 'o',
            'ø' => 'o',
            'ù' => 'u',
            'ú' => 'u',
            'û' => 'u',
            'ý' => 'y',
            'ý' => 'y',
            'þ' => 'b',
            'ÿ' => 'y',
            'Ŕ' => 'R',
            'ŕ' => 'r',
        ];

        $str = strtr($str, $table);

        //Script criado por chluehr https://gist.github.com/chluehr/1632883
        if (!empty($replace)) {
            $str = str_replace((array)$replace, ' ', $str);
        }
        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

        return $clean;
        //Fim do script por chluehr
    }

    /**
     * Alterando o Preço e Promoção do Produto
     *
     * @param array $productBling Produto do Bling
     * @param int   $product_id   ID do Produto na Loja
     *
     * @return bool
     * @used-by updateProdutoStoreSQL() Atualizado os dados do Produto na Loja
     * @used-by \ModelModuleCodeBlingNativeProduct::update_price_product()
     */
    public function addPriceProductStoreSQL(
        array $productBling,
        int $product_id
    ): bool {
        $this->log->write(
            'ModelModuleCodeBling_addPriceProductStoreSQL() - Produto ID: '
            . $product_id
        );

        // Preço vindo da relação do Bling com a Loja - Mini Carrinho verde na lista dos Produtos
        $productStoreBling = $this->getProductStoreBling($productBling['id']);

        if (!isset($productStoreBling['preco'])) {
            $this->log->write(
                'ModelModuleCodeBling_addPriceProductStoreSQL() - Não encontrou o Preço do Produto ' . $product_id . ' para Loja'
            );

            return false;
        }

        $price = !empty($productStoreBling['preco']) ? (float)$productStoreBling['preco'] : (float)$productBling['preco'];
        $price_special = !empty($productStoreBling['precoPromocional']) ?
            (float)$productStoreBling['precoPromocional'] :
            (float)$price;

        // Muda o Preço do Produto
        $sql = "UPDATE " . DB_PREFIX . "product SET
            price = '" . (float)$price . "'
            WHERE product_id = '" . (int)$product_id . "'
        ";
        $this->db->query($sql);

        $this->log->write(
            'ModelModuleCodeBling_addPriceProductStoreSQL() - Alterado o Preço, SQL: '
            . $sql
        );

        // Muda o Preço de Custo do Produto
        // Caso use o Módulo Super Preço com Fórmula - Custo e Dólar - Opencart
        // https://www.codemarket.com.br/produto/super-preco-com-formula-custo-e-dolar-opencart

        $verifyCusto = $this->db->query(
            "SHOW COLUMNS FROM `" . DB_PREFIX . "product` LIKE 'code_preco_custo'"
        );

        // https://developer.bling.com.br/referencia#/Produtos/get_produtos__idProduto_
        if ($verifyCusto->num_rows && isset($productBling['fornecedor']['precoCusto'])) {
            $sql = "UPDATE " . DB_PREFIX . "product SET
                code_preco_custo = '" . (float)$productBling['fornecedor']['precoCusto'] . "'
                WHERE product_id = '" . (int)$product_id . "'
            ";
            $this->db->query($sql);

            $this->log->write(
                'ModelModuleCodeBling_addPriceProductStoreSQL() - Alterado o Custo, SQL: '
                . $sql
            );
        }

        // Sem preço promocional ou igual ao preço normal
        if (empty($this->conf->code_promocao) || (int)$this->conf->code_promocao === 2
            || $price_special == $price
        ) {
            return true;
        }

        // Remove as Promoções se o produto tiver precoPromocional
        $this->db->query(
            "DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '"
            . $product_id . "'"
        );

        $this->log->write(
            'ModelModuleCodeBling_addPriceProductStoreSQL() - Removido as Promoções, Produto ID: ' . $product_id
        );

        if ($price_special > 0.01) {
            $customer_group = !empty($this->conf->code_promocao_grupo)
                ? (int)$this->conf->code_promocao_grupo : 1;

            $date_start = date('Y-m-d', strtotime('-2 days'));
            $date_end = date('Y-m-d', strtotime('+90 days'));

            $sql = "INSERT INTO " . DB_PREFIX . "product_special SET
            product_id = '" . $product_id . "',
            customer_group_id = '" . $customer_group . "',
            priority = 0,
            price = '"
                . $price_special . "',
            date_start = '" . $this->db->escape($date_start) . "',
            date_end = '" . $this->db->escape($date_end) . "'
        ";
            $this->db->query($sql);

            $this->log->write(
                'ModelModuleCodeBling_addPriceProductStoreSQL() - Alterado o Preço Promocional, SQL: '
                . $sql
            );
        }

        return true;
    }

    /**
     * @param $url string
     *
     * @return bool|mixed
     */
    private function getImage($url)
    {
        $curl = curl_init();

        curl_setopt_array(
            $curl,
            [
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 3,
                CURLOPT_TIMEOUT => 80,
                CURLOPT_CONNECTTIMEOUT => 20,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTPHEADER => [
                    "Cache-Control: no-cache",
                ],
            ]
        );

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $type = curl_getinfo($curl, CURLINFO_CONTENT_TYPE);
        curl_close($curl);

        if (!empty($err) || empty($response) || empty($type)) {
            $this->log->write(
                'ModelModuleCodeBling_getImage() - Error Curl, URL: ' . $url
                . ' Erro: ' . print_r($err, true)
            );
            $this->log->write(
                'ModelModuleCodeBling_getImage() - Retorno: ' . print_r(
                    $response,
                    true
                )
            );
            $this->log->write(
                'ModelModuleCodeBling_getImage() - Type: ' . print_r($type, true)
            );

            return [];
        } else {
            if ($this->debugLog) {
                $this->log->write(
                    'ModelModuleCodeBling_getImage() - Retorno, URL: ' . $url
                    . ', Type : ' . print_r($type, true)
                );
            }

            return ['image' => $response, 'type' => $type];
        }
    }

    /**
     *  Verfica se existe a caregoria e cria ela
     *
     * @param     $product_id
     * @param     $category
     * @param int $parent_id
     *
     * @return false|int|mixed
     */
    private function addCategory($product_id, $category, $parent_id = 0)
    {
        if (empty($category)) {
            return false;
        }

        //Verificar se existe a Categoria
        //Criar a Categoria
        //Ligar o Produto a Categoria
        $query = $this->db->query(
            "SELECT c . category_id FROM `" . DB_PREFIX . "code_bling_category` bc
            INNER JOIN " . DB_PREFIX . "category c ON(c.category_id = bc.category_id)
            WHERE  bc.bling_category_id = '" . (int)$category['id'] . "' LIMIT 1
        "
        );

        // Já tem a Categoria
        if (!empty($query->row['category_id'])) {
            $this->log->write(
                'ModelModuleCodeBling_addCategory - Categoria: '
                . $query->row['category_id'] . ', nome: ' . $category['descricao']
                . ' já existe'
            );
            $category_id = $query->row['category_id'];
        } else {
            foreach ($this->languages as $l) {
                $lin[$l['language_id']] = [
                    'name' => trim($category['descricao']),
                    'description' => trim($category['descricao']),
                    'meta_title' => trim($category['descricao']),
                    'meta_description' => trim($category['descricao']),
                    'meta_keyword' => trim($category['descricao']),
                ];
            }

            $path = '';
            //$parent_id = 0;
            $top = 1;

            $data = [
                'category_description' => $lin,
                'category_bling_id' => $category['id'],
                'path' => $path,
                'parent_id' => $parent_id,
                'filter' => '',
                'category_store' => [0],
                'keyword' => $this->slug(trim($category['descricao'])),
                'name' => trim($category['descricao']),
                'image' => '',
                'top' => $top,
                'column' => 1,
                'sort_order' => 1,
                'status' => !empty($this->conf->code_habilitar_categorias)
                && $this->conf->code_habilitar_categorias == 1 ? 1 : 0,
                'category_layout' => [''],
            ];

            $category_id = $this->addCategoryDatabase($data);
            $this->log->write(
                'ModelModuleCodeBling_addCategory - Categoria: ' . $category_id
                . ', nome:' . $category['descricao'] . ' criada com sucesso'
            );
        }

        // Adicionado a Categoria criada no Produto
        if (!empty($category_id)) {
            $this->db->query(
                "INSERT IGNORE INTO " . DB_PREFIX . "product_to_category SET
                 product_id = '" . (int)$product_id . "',
                 category_id = '" . (int)$category_id . "'
            "
            );
        }

        return $category_id;
    }

    /**
     * Salva no Banco a Categoria e retorna o ID
     *
     * @param $data
     *
     * @return mixed
     */
    private function addCategoryDatabase($data)
    {
        $this->log->write(
            'ModelModuleCodeBling_addCategoryDatabase() - Criando Categoria no Banco'
        );

        $this->db->query(
            "INSERT INTO " . DB_PREFIX . "category SET parent_id = '"
            . (int)$data['parent_id'] . "', `top` = '" . (isset($data['top'])
                ? (int)$data['top'] : 0) . "', `column` = '" . (int)$data['column']
            . "', sort_order = '" . (int)$data['sort_order'] . "', status = '"
            . (int)$data['status'] . "', date_modified = NOW(), date_added = NOW()"
        );

        $category_id = $this->db->getLastId();

        $this->log->write(
            'ModelModuleCodeBling_addCategoryDatabase() - Criada a Categoria, ID: '
            . $category_id
        );

        if (isset($data['image'])) {
            $this->db->query(
                "UPDATE " . DB_PREFIX . "category SET image = '" . $this->db->escape(
                    $data['image']
                ) . "' WHERE category_id = '" . (int)$category_id . "'"
            );
        }

        foreach ($data['category_description'] as $language_id => $value) {
            $this->db->query(
                "INSERT INTO " . DB_PREFIX
                . "category_description SET category_id = '" . (int)$category_id
                . "', language_id = '" . (int)$language_id . "', name = '"
                . $this->db->escape(
                    $value['name']
                ) . "', description = '" . $this->db->escape(
                    $value['description']
                ) . "', meta_title = '" . $this->db->escape(
                    $value['meta_title']
                ) . "', meta_description = '" . $this->db->escape(
                    $value['meta_description']
                ) . "', meta_keyword = '" . $this->db->escape(
                    $value['meta_keyword']
                ) . "'"
            );

            if (isset($data['keyword'])) {
                if (version_compare(VERSION, '3.0.0.0', '>=')) {
                    $this->db->query(
                        "INSERT INTO " . DB_PREFIX
                        . "seo_url SET store_id = 0, language_id = '"
                        . (int)$language_id . "', query = 'category_id="
                        . (int)$category_id . "', keyword = '" . $this->db->escape(
                            $data['keyword']
                        ) . "'"
                    );
                } else {
                    $this->db->query(
                        "INSERT INTO " . DB_PREFIX
                        . "url_alias SET query = 'category_id=" . (int)$category_id
                        . "', keyword = '" . $this->db->escape(
                            $data['keyword']
                        ) . "'"
                    );
                }
            }
        }

        if ($this->debugLog) {
            $this->log->write(
                'ModelModuleCodeBling_addCategoryDatabase() - Salvo a descrição da Categoria, ID: '
                . $category_id
            );
        }

        // MySQL Hierarchical Data Closure Table Pattern
        $level = 0;

        $query = $this->db->query(
            "SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '"
            . (int)$data['parent_id'] . "' ORDER BY `level` ASC"
        );

        foreach ($query->rows as $result) {
            $this->db->query(
                "INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '"
                . (int)$category_id . "', `path_id` = '" . (int)$result['path_id']
                . "', `level` = '" . (int)$level . "'"
            );

            $level++;
        }

        $this->db->query(
            "INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '"
            . (int)$category_id . "', `path_id` = '" . (int)$category_id
            . "', `level` = '" . (int)$level . "'"
        );

        if (isset($data['category_store'])) {
            foreach ($data['category_store'] as $store_id) {
                $this->db->query(
                    "INSERT INTO " . DB_PREFIX
                    . "category_to_store SET category_id = '" . (int)$category_id
                    . "', store_id = '" . (int)$store_id . "'"
                );
            }
        }

        if ($this->debugLog) {
            $this->log->write(
                'ModelModuleCodeBling_addCategoryDatabase() - Salvo no path e no store a Categoria, ID: '
                . $category_id
            );
        }

        if ($this->debugLog) {
            $this->log->write(
                'ModelModuleCodeBling_addCategoryDatabase() - SQL' . print_r(
                    "INSERT INTO " . DB_PREFIX . "code_bling_category SET
            bling_category_id = '" . $data['category_bling_id'] . "',
            category_id = '" . (int)$category_id . "',
            name = '" . $this->db->escape($data['name']) . "'
            ",
                    true
                )
            );
        }

        // Salvando na Tabela da Categoria do Bling
        $this->db->query(
            "INSERT INTO " . DB_PREFIX . "code_bling_category SET
            bling_category_id = '" . $data['category_bling_id'] . "',
            category_id = '" . (int)$category_id . "',
            name = '" . $this->db->escape($data['name']) . "'
        "
        );

        $this->log->write(
            'ModelModuleCodeBling_addCategoryDatabase() - Salvo a Categoria no Bling, ID: '
            . $category_id
        );

        return $category_id;
    }

    // ##### CHAMADA NA API #####

    /**
     * Faz a chamada na API usando Curl
     *
     * @param string $url
     * @param string $method
     * @param string $function_name
     * @param array  $data
     * @param int    $timeout
     * @param int    $debug
     * @param array  $data_header
     * @param bool   $response_header
     *
     * @return void
     */
    private function apiCall(
        string $url,
        string $method = "POST",
        string $function_name = '',
        array $data = [],
        int $timeout = 8,
        int $debug = 0,
        array $data_header = [],
        bool $response_header = false
    ) {
        // Sleep de 300ms para evitar o limite da API do Bling
        usleep(300000);

        // Ativando o Debug no Log caso tenha a variável debugLog como true
        if ($debug != 2 && $this->debugLog) {
            $debug = 1;
        }

        $response = $this->codeHelperDB->curl(
            $url,
            $method,
            $function_name,
            $data,
            $data_header,
            $this->token,
            $timeout,
            $debug,
            $this->log,
            $response_header
        );

        /*
        $backtrace = debug_backtrace();
        $caller = isset($backtrace[1]) ? $backtrace[1] : null;
        $function_name = !empty($caller['function']) ? $caller['function']: '';
        */

        if (!empty($response['error']['type']) && $response['error']['type'] == 'invalid_token') {
            echo '<h1 style="color:#ec0000">Token Inválido, refaça a Permissão na Configuração do App!</h1>';
            exit('PARANDO O CÓDIGO');
        } elseif (!empty($response['error']['type']) && $response['error']['type'] == 'INVALID_REQUEST_BODY') {
            echo '<h1 style="color:#ec0000">Formato do corpo da requisição inválido, ' . $function_name . ' URL: ' . $url .
                ',  
                JSON:</h1>';
            print_r($data);
            print_r(json_encode($data));
            exit('PARANDO O CÓDIGO');
        }

        return $response;
    }
}
