<?php

use Vindi\Customer;
use Vindi\Exceptions\ValidationException;
use Vindi\Subscription;

require_once DIR_SYSTEM . '/library/code_vindi/vendor/autoload.php';

/**
 * Class ModelExtensionPaymentCodeVindi
 *
 * @author Felipo Antonoff Araújo e Victor Henrique Ramos
 * @version 1.0
 * @package code_vindi
 */
class ModelExtensionPaymentCodeVindi extends Model
{
    /**
     * @var \stdClass
     */
    private $conf;
    private $customerService;
    private $log;
    private $status;

    /**
     * Definicoes iniciais de boot
     *
     * @param $registry
     *
     * @throws \Exception
     */
    public function __construct($registry)
    {
        parent::__construct($registry);

        $this->load->model('checkout/order');
        $this->load->model('module/codemarket_module');
        $this->conf = $this->model_module_codemarket_module->getModulo('570');
        $this->log = new Log('Code-Vindi.log');

        $this->status = $this->config();
    }

    public function config()
    {
        if (empty($this->conf) || empty($this->conf->code_sandbox_private) || empty($this->conf->code_production_private) || empty($this->conf->code_production_public)
            || empty($this->conf->code_sandbox_public) || empty($this->conf->code_title_cartao) || empty($this->conf->code_botao_cartao) || empty($this->conf->code_mensagem_cartao)
            || !isset($this->conf->code_geo_zones_cartao) || !isset($this->conf->code_sort_order_cartao) || empty($this->conf->code_env) || empty($this->conf->code_status_cartao)
            || $this->conf->code_status_cartao != 1) {
            $this->log->write('config - Módulo desativado ou faltando dados na configuração');

            return false;
        }

        /*
            Modo Cliente de Teste
            Desativa se não for o cliente de teste e estiver habilitado o modo de teste
        */
        if (!empty($this->conf->code_env) && $this->conf->code_env == 'sandbox' && !empty($this->conf->code_test_email)) {
            if (!empty($this->customer->getEmail()) && $this->conf->code_test_email != $this->customer->getEmail()) {
                return false;
            } else if (empty($this->customer->getEmail())) {
                return false;
            }
        }

        if ($this->conf->code_env === 'production') {
            $this->conf->code_env_mensagem = '';
        } else {
            $this->conf->code_env_mensagem = " - <b>Modo de Teste, não compre!</b>";
        }

        if ($this->conf->code_env === 'sandbox') {
            putenv('VINDI_API_KEY=' . $this->conf->code_sandbox_private);
            putenv('VINDI_API_URI=https://sandbox-app.vindi.com.br/api/v1/');
        } else {
            putenv('VINDI_API_KEY=' . $this->conf->code_production_private);
            putenv('VINDI_API_URI=https://app.vindi.com.br/api/v1/');
        }

        $this->customerService = new Customer();
        return true;
    }

    public function region($address)
    {
        //Região
        if (!empty($this->conf->code_geo_zones_cartao)) {
            $query = $this->db->query("
                SELECT geo_zone_id FROM " . DB_PREFIX . "zone_to_geo_zone WHERE 
                geo_zone_id = '" . (int) $this->conf->code_geo_zones_cartao . "' 
                AND country_id = '" . (int) $address['country_id'] . "' 
                AND (zone_id = '" . (int) $address['zone_id'] . "' OR zone_id = '0')
                LIMIT 1
            ");

            if (empty($query->row['geo_zone_id'])) {
                $this->log->write('region - Região Geográfica não encontrada' . print_r($query->row, true));

                return false;
            }
        }

        return true;
    }

    /**
     * Metodo padrao pra exibicao na listagem
     *
     * @param $address
     * @param $total
     *
     * @return array
     */
    public function getMethod($address, $total)
    {
        if (empty($this->region($address)) || empty($this->status)) {
            return [];
        }

        return [
            'code'       => 'code_vindi',
            'title'      => $this->conf->code_title_cartao . $this->conf->code_env_mensagem,
            'terms'      => '',
            'sort_order' => $this->conf->code_sort_order_cartao,
        ];
    }

    /**
     * Informa se o Pagamento suporta assinatura
     *
     * @return bool
     */
    public function recurringPayments()
    {
        return true;
    }

    /**
     *
     * Cadastra e atualiza o cliente na Vindi
     *
     * @param $order
     *
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Vindi\Exceptions\RateLimitException
     * @throws \Vindi\Exceptions\RequestException
     */
    public function addCliente($order)
    {
        $query = $this->db->query('SELECT vindi_customer_id FROM ' . DB_PREFIX . 'customer WHERE customer_id = ' . $order['customer_id']);
        $oc_customer = $query->row;

        if (!empty($this->conf->code_custom_cpf) && !empty($order['custom_field'][$this->conf->code_custom_cpf])) {
            //Removendo máscaras e adicionando máscara correta no CPF
            $cpf = preg_replace("/[^0-9]/", "", $order['custom_field'][$this->conf->code_custom_cpf]);
            $cpf = trim($cpf);
        } else {
            $cpf = '';
        }

        if (!empty($oc_customer['vindi_customer_id'])) {
            $cid = $oc_customer['vindi_customer_id'];
            try {
                $this->customerService->update($cid, [
                    'name'          => implode(' ', [$order['firstname'], $order['lastname']]),
                    'email'         => $order['email'],
                    'registry_code' => $cpf,
                    'code'          => 'loja_' . $order['customer_id'],
                    'address'       => [
                        'street'       => $order['payment_address_1'],
                        'number'       => isset($order['payment_custom_field'][$this->conf->code_custom_number]) ? preg_replace("/[^0-9]/", "", $order['payment_custom_field'][$this->conf->code_custom_number]) : '',
                        'zipcode'      => preg_replace("/[^0-9]/", "", $order['payment_postcode']),
                        'neighborhood' => $order['payment_address_2'],
                        'city'         => $order['payment_city'],
                        'state'        => $order['payment_zone_code'],
                        'country'      => $order['payment_iso_code_2'],
                    ],
                ]);

                return $cid;
            } catch (ValidationException $v) {
                $this->log->write('addCliente - Atualizar cliente falhou: ' . print_r($v, true));
                return false;
            }
        } else {
            try {
                $customer = $this->customerService->create([
                    'name'          => implode(' ', [$order['firstname'], $order['lastname']]),
                    'email'         => $order['email'],
                    'registry_code' => $cpf,
                    'code'          => 'loja_' . $order['customer_id'],
                    'address'       => [
                        'street'       => $order['payment_address_1'],
                        'number'       => isset($order['payment_custom_field'][$this->conf->code_custom_number]) ? preg_replace("/[^0-9]/", "", $order['payment_custom_field'][$this->conf->code_custom_number]) : '',
                        'zipcode'      => preg_replace("/[^0-9]/", "", $order['payment_postcode']),
                        'neighborhood' => $order['payment_address_2'],
                        'city'         => $order['payment_city'],
                        'state'        => $order['payment_zone_code'],
                        'country'      => $order['payment_iso_code_2'],
                    ],
                ]);

                if (!empty($customer->id)) {
                    $this->db->query('UPDATE ' . DB_PREFIX . 'customer SET vindi_customer_id = ' . $customer->id . ' WHERE customer_id = ' . $order['customer_id']);
                    return $customer->id;
                }

                $this->log->write('addCliente - Criar cliente falhou, não foi retornado o ID do Cliente na Vindi, retorno: ' . print_r($customer, true));
                return false;
            } catch (ValidationException $v) {
                $this->log->write('addCliente - Criar cliente falhou: ' . print_r($v, true));
                return false;
            }
        }
    }

    /**
     *
     * Cria o Produto na Vindi e retorna ele
     *
     * @param $order
     *
     * @return bool|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Vindi\Exceptions\RateLimitException
     * @throws \Vindi\Exceptions\RequestException
     */
    public function addProduto($order)
    {
        //Criando o Produto, usando o total e o id do Pedido
        $productService = new Vindi\Product();

        try {
            $pv = $productService->create([
                'name'           => 'Pedido ' . $order['order_id'] . ' - Loja Virtual',
                'pricing_schema' => [
                    'price'       => $order['total'],
                    'schema_type' => 'flat',
                ],
            ]);

            return $pv;
        } catch (ValidationException $v) {
            $this->log->write('addProduto - Criar produto falhou: ' . print_r($v, true));
            return false;
        }
    }

    public function returnFalse()
    {
        return $this->response->setOutput(json_encode([
            'success' => false,
        ]));
    }

    /**
     *
     * Mudanças no Status do Pedido e adição do Order ID da Vindi no Pedido
     *
     * @param $bill
     * @param $orderId
     *
     * @return bool
     */
    public function updateOrder($bill, $orderId)
    {
        if (empty($bill->id) || empty($bill->status)) {
            $this->log->write('updateOrder - Sem os dados da Trasanção ' . print_r($bill, true));
            return false;
        }

        $this->db->query('UPDATE ' . DB_PREFIX . 'order SET vindi_order_id = ' . (int) $bill->id . ' WHERE order_id = ' . (int) $orderId);

        $this->log->write('updateOrder - Atualizado com o id da vindi');

        //Se for Boleto e tiver retornado a URL
        if (!empty($bill->charges[0]->print_url)) {
            /*
             * [id] => 2831496
                            [amount] => 88.0
                            [status] => pending
                            [due_at] => 2019-12-23T23:59:59.000-03:00
                            [paid_at] =>
                            [installments] => 1
                            [attempt_count] => 0
                            [next_attempt] =>
                            [print_url] => https://sandbox-app.vindi.com.br/customer/charges/2831496?b=2878376&m=3662&t=a1dadd3b-3528-4bbe-be72-4a5679dcf8ee
                            [created_at] => 2019-12-23T10:03:11.000-03:00
                            [updated_at] => 2019-12-23T10:03:11.000-03:00
                            [last_transaction] => stdClass Object
                                (
                                    [id] => 1686940
                                    [transaction_type] => charge
                                    [status] => waiting
                                    [amount] => 88.0
                                    [installments] =>
                                    [gateway_message] =>
                                    [gateway_response_code] =>
                                    [gateway_authorization] =>
                                    [gateway_transaction_id] => 1
                                    [gateway_response_fields] => stdClass Object
                                        (
                                            [barcode] => 34197811200000088001750000000151111111116000
                                            [typable_barcode] => 34191.75009 00000.151118 11111.160005 7 81120000008800
                                        )

                                    [fraud_detector_score] =>
                                    [fraud_detector_status] =>
                                    [fraud_detector_id] =>
                                    [created_at] => 2019-12-23T10:03:11.000-03:00
                                    [gateway] =>
                                    [payment_profile] =>
                                )
             */

            $this->log->write('updateOrder - Salvando o comentário e sessão');

            $urlBoleto = $bill->charges[0]->print_url;
            $barcodeBoleto = !empty($bill->charges[0]->last_transaction->gateway_response_fields->typable_barcode) ? $bill->charges[0]->last_transaction->gateway_response_fields->typable_barcode : '';

            $comment = 'Para concluir o pagamento, clique no botão "abrir boleto" para gerar e visualizar o boleto.<br>';
            $comment .= '<b><a href="' . $urlBoleto . '" target="_blank">Abrir Boleto</a></b>';
            if (!empty($barcodeBoleto)) {
                $comment .= "<br> Código de Barras do Boleto: " . $barcodeBoleto;
            }
            $comment .= "<br> Caso já tenha pago, aguarde a confirmação do pagamento";

            $boletoSuccess = 'Para concluir o pagamento, clique no botão "abrir boleto" para gerar e visualizar o boleto.<br>';
            $boletoSuccess .= '<a class="btn btn-primary code_mp_boleto_abrir" href="' . $urlBoleto . '" target="_blank">Abrir Boleto</a>';
            if (!empty($barcodeBoleto)) {
                $boletoSuccess .= "<br> Código de Barras do Boleto: " . $barcodeBoleto;
            }
            $boletoSuccess .= "<br><br>";

            $this->session->data['code_vindi_boleto'][$this->session->data['order_id']] = $boletoSuccess;
        }

        if (empty($comment) && !empty($bill->charges[0]->last_transaction->payment_profile->card_number_first_six)) {
            $bandeira = $bill->charges[0]->last_transaction->payment_profile->payment_company->name;
            $numero = $bill->charges[0]->last_transaction->payment_profile->card_number_last_four;
            $nome = $bill->charges[0]->last_transaction->payment_profile->holder_name;

            $parcelas = $bill->charges[0]->last_transaction->installments;
            $total = $bill->charges[0]->last_transaction->amount;
            $total = $this->currency->format(($total), $this->session->data['currency']);

            if ($parcelas == 1) {
                $pagamento = $parcelas . ' parcela, total: ' . $total;
            } else {
                $pagamento = $parcelas . ' parcelas, total: ' . $total;
            }

            $comment = "Pagamento por Cartão " . $bandeira;
            $comment .= "<br>";
            $comment .= "Os 4 últimos números do Cartão: " . $numero;
            $comment .= "<br>";
            $comment .= $pagamento;
            $comment .= "<br><br>";
            $comment .= "Dados do Titular do Cartão:";
            $comment .= "<br>";
            $comment .= "Nome: " . $nome;
        }

        //Pedido Criado
        /*
        $this->model_checkout_order->addOrderHistory(
            $this->session->data['order_id'],
            $this->conf->code_charge_created
        );
        */

        $statusAlert = false;
        if (!empty($this->conf->code_status_notify) and $this->conf->code_status_notify == 1) {
            $statusAlert = true;
        }

        $b = $bill;

        if (!empty($b->charges[0]->last_transaction->status)) {
            $last_transaction_status = $b->charges[0]->last_transaction->status;
        } else {
            $this->log->write('updateOrder - Sem Status definido na Vindi');
            return false;
        }

        $status = $b->status;

        if ($status == 'canceled' && $last_transaction_status == 'success') {
            $status = 'code_charge_canceled';
        } else if ($status == 'canceled' && $last_transaction_status == 'rejected') {
            $status = 'code_charge_rejected';
        } else if ($status == 'pending' && $last_transaction_status == 'rejected') {
            $status = 'code_charge_rejected';
        } else if ($status == 'canceled' && $b->charges[0]->last_transaction->transaction_type == 'refund' && $last_transaction_status == 'success') {
            $status = 'code_charge_refunded';
        } else if ($status == 'pending' && $last_transaction_status == 'waiting') {
            $status = 'code_charge_created';
        } else if ($status == 'paid') {
            $status = 'code_charge_paid';
        } else {
            $this->log->write('updateOrder - Sem Status, usando o Criado/Aguardando');
            $status = 'code_charge_created';
        }

        if (empty($this->conf->{$status})) {
            $log->write('updateOrder - Sem Status definido: ' . $status);
            return false;
        }

        $this->log->write('updateOrder - Adicionado Status' . $status . 'ao histórico do Pedido' . $orderId);

        //Status Atual do Pedido (Transação)
        $this->model_checkout_order->addOrderHistory(
            (int) $orderId,
            $this->conf->{$status},
            $comment,
            $statusAlert
        );

        $this->log->write('updateOrder - Retornando sucesso, Pedido: ' . $orderId);

        return true;
    }

    public function getTransactions($query, $page = 1)
    {
        /*
        https://vindi.github.io/api-docs/dist/#/bills/getV1Bills
        https://atendimento.vindi.com.br/hc/pt-br/articles/204163150
        */
        $billService = new \Vindi\Bill();

        //$query = 'subscription_id=' . $subscriptionId;

        try {
            $bills = $billService->all(
                [
                    'page'       => $page,
                    'per_page'   => 1000,
                    'query'      => $query,
                    'sort_by'    => 'created_at',
                    'sort_order' => 'asc',
                ]
            );

            if (!empty($bills)) {
                return $bills;
                /*
                foreach ($bills as $bill) {
                    if ($bill->subscription->id != $subscriptionId) {
                        break;
                    }

                    echo $bill->id;
                    echo "<br>";
                    echo $bill->status;
                    echo "<br>";
                    echo $bill->url;

                    echo $bill->created_at;
                    echo $bill->updated_at;
                    print_r($bill);
                }
                */
            }
            //print_r($bills);
        } catch (ValidationException $v) {
            $this->log->write('getTransactions - Buscar fatura falhou: ' . print_r($v, true));
            //print_r($v);

            return false;
        }
    }

    public function getTransactionsWebhooks()
    {
        if (empty($this->status)) {
            return false;
        }

        $log = new Log('Code-Vindi-Webhook-CronJob-' . date("m-Y") . '.log');

        //Limite de 50 bills
        $bill = $this->getTransactions("updated_at>=2020-11-26T08:27:04.000-03:00", 1);
        $billAll = [];
        $customerLog = [];

        for ($i = 2; $i <= 3; $i++) {
            if (!empty($bill) && count($bill) == 50) {
                $billAll[] = $bill;
                $bill = $this->getTransactions("updated_at>=2020-11-26T08:27:04.000-03:00", $i);
            } else if (!empty($bill)) {
                $billAll[] = $bill;
                break;
                //return $billAll;
            }
        }

        if (empty($billAll)) {
            exit("Sem Pedidos");
        }

        //print_r($billAll);
        //exit();

        foreach ($billAll as $bills) {
            foreach ($bills as $b) {
                if (empty($b->code)) {
                    continue;
                }

                if (!empty($b->charges[0]->last_transaction->status)) {
                    $last_transaction_status = $b->charges[0]->last_transaction->status;
                } else {
                    $this->log->write('getTransactionsWebhooks - Sem Status definido na Vindi');
                    continue;
                }

                $status = $b->status;

                if ($status == 'canceled' && $last_transaction_status == 'success') {
                    $status = 'code_charge_canceled';
                } else if ($status == 'canceled' && $last_transaction_status == 'rejected') {
                    $status = 'code_charge_rejected';
                } else if ($status == 'pending' && $last_transaction_status == 'rejected') {
                    $status = 'code_charge_rejected';
                } else if ($status == 'canceled' && $b->charges[0]->last_transaction->transaction_type == 'refund' && $last_transaction_status == 'success') {
                    $status = 'code_charge_refunded';
                } else if ($status == 'pending' && $last_transaction_status == 'waiting') {
                    $status = 'code_charge_created';
                } else if ($status == 'paid') {
                    $status = 'code_charge_paid';
                } else {
                    $this->log->write('getTransactionsWebhooks - Sem Status 2 definido na Vindi');
                    continue;
                }

                if (empty($this->conf->{$status})) {
                    $log->write('Sem Status definido: ' . $status);
                    continue;
                }

                $log->write('Pedido: ' . $b->code . ' Status: ' . $status);
                $status = $this->conf->{$status};

                // Dados estranhos, pedido não pareado (provavelmente veio de outra plataforma)
                $queryOrder = $this->db->query('SELECT * FROM ' . DB_PREFIX . 'order WHERE order_id = ' . (int) $b->code . ' LIMIT 1');

                if (!$queryOrder->num_rows) {
                    $log->write("getTransactionsWebhooks - Nenhum Pedido encontrado com o ID da Vindi, provável que seja Transações da Assinatura ou criada em outra plataforma");
                    continue;
                }

                $order_id = $b->code;

                /*
                $queryS = $this->db->query('SELECT * FROM ' . DB_PREFIX . 'order_recurring WHERE order_id = ' . (int) $order_id . ' LIMIT 1');

                //Assinatura, mudando o Status
                if (!empty($queryS->row['vindi_assinatura_id'])) {
                    $log->write('Chamando o webhookAssinatura()');
                    $this->webhookAssinatura($queryS->row['vindi_assinatura_id']);
                }
                */

                // Status já trabalhado apenas retorna OK para o webhook nao tentar novamente
                if ($queryOrder->row['order_status_id'] === $status) {
                    $log->write("Pedido: " . (int) $order_id . ", Status igual, já notificado");
                    continue;
                }

                //Verificando se já tem o Status no Pedido
                $query = $this->db->query("SELECT order_id FROM " . DB_PREFIX . "order_history 
                    WHERE 
                    order_id = '" . (int) $order_id . "' 
                    AND 
                    order_status_id = '" . (int) $status . "'
                    LIMIT 1
                 ");

                $log->write("getTransactionsWebhooks - Retorno da verificação: " . print_r($query->row, true));

                if (!empty($query->row['order_id'])) {
                    $log->write("Pedido: " . (int) $order_id . ", já notificado");
                    continue;
                }

                $this->load->model('checkout/order');

                $statusAlert = false;
                if (!empty($this->conf->code_status_notify) and $this->conf->code_status_notify == 1) {
                    $statusAlert = true;
                }

                //Tirar o Bloqueio
                exit();

                $this->model_checkout_order->addOrderHistory(
                    (int) $order_id,
                    $status,
                    '',
                    $statusAlert
                );

                $log->write('getTransactionsWebhooks - Pedido ' . (int) $order_id . ' notificado com sucesso, Status: ' . $status);
            }
        }

        exit();

        $log2 = new Log('Code-Vindi-Webhook-Verificar-Clientes.log');

        //Log dos Clientes com mais de 1 Pagamento
        if (!empty($billAll)) {
            foreach ($billAll as $bills) {
                foreach ($bills as $b) {
                    if (empty($b->code)) {
                        continue;
                    }

                    $customerLog[$b->customer->id]['dados'] = $b->customer;
                    $customerLog[$b->customer->id]['pagamentos'][$b->id]['pedido_loja'] = $b->code;
                    $customerLog[$b->customer->id]['pagamentos'][$b->id]['id'] = $b->id;
                    $customerLog[$b->customer->id]['pagamentos'][$b->id]['status'] = $b->status;
                }
            }
        }

        $log = '';
        if (!empty($customerLog)) {
            foreach ($customerLog as $key => $c) {
                if (count($c['pagamentos']) == 1) {
                    unset($customerLog[$key]);
                    continue;
                }

                $log .= "
                Cliente:
                Clinte Nome: {$c['dados']->name}
                Cliente ID: {$c['dados']->id}
                Cliente E-mail: {$c['dados']->email}
                Cliente Code: {$c['dados']->code}
                Pagamentos:
                ";

                foreach ($c['pagamentos'] as $key => $b) {
                    $log .= "
                    ID Pedido: {$b['pedido_loja']}
                    ID: {$b['id']}
                    Status: {$b['status']}
                    ";
                }

                $log .= "-------------------------------------------------------";
            }
        }

        $log2->write($log);
        print_r($customerLog);
        exit();
    }

    public function getTransactionsArray($vindi_assinatura_id)
    {
        if (empty($this->status)) {
            return false;
        }

        $bill = $this->getTransactions("subscription_id=$vindi_assinatura_id", 1);
        $sub = $this->getAssinaturas($vindi_assinatura_id);

        //print_r($bill);
        //print_r($sub);

        if (empty($bill) || empty($sub)) {
            $this->log->write('getTransactionsArray - Sem transações ou assinatura');

            return false;
        }
        //print_r($bill);
        //print_r($sub);
        //exit();

        //echo   date("d/m/Y", strtotime($bill[0]->charges[0]->created_at));
        //print_r($this->conf);
        //print_r($bill);

        $created_at = date("d/m/Y", strtotime($sub->created_at));
        $updated_at = date("d/m/Y", strtotime($sub->updated_at));

        $sub = [
            'plan'       => $sub->plan->name,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
        ];

        //Faturas
        $bills = [];
        foreach ($bill as $b) {
            if (!empty($b->charges[0]->last_transaction->status)) {
                $last_transaction_status = $b->charges[0]->last_transaction->status;
            } else {
                $this->log->write('getTransactionsArray - Sem Status definido na Vindi');
                continue;
            }

            $status = $b->status;

            if ($status == 'canceled' && $last_transaction_status == 'success') {
                $status = 'code_charge_canceled';
            } else if ($status == 'canceled' && $last_transaction_status == 'rejected') {
                $status = 'code_charge_rejected';
            } else if ($status == 'pending' && $last_transaction_status == 'rejected') {
                $status = 'code_charge_rejected';
            } else if ($status == 'canceled' && $b->charges[0]->last_transaction->transaction_type == 'refund' && $last_transaction_status == 'success') {
                $status = 'code_charge_refunded';
            } else if ($status == 'pending' && $last_transaction_status == 'waiting') {
                $status = 'code_charge_created';
            } else if ($status == 'paid') {
                $status = 'code_charge_paid';
            } else {
                $this->log->write('getTransactionsArray - Sem Status 2 definido na Vindi');
                continue;
            }

            if (empty($this->conf->{$status})) {
                $this->log->write('getTransactionsArray - Sem Status definido: ' . $status);
                return false;
            }

            $order_status_id = $this->conf->{$status};
            $order_status = $this->db->query("SELECT name FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int) $order_status_id . "' LIMIT 1");

            $created_at = date("d/m/Y", strtotime($b->charges[0]->created_at));
            $updated_at = date("d/m/Y", strtotime($b->charges[0]->updated_at));
            $amount = $this->currency->format(($b->charges[0]->amount), $this->session->data['currency']);

            if (!empty($b->charges[0]->print_url)) {
                $urlBoleto = $b->charges[0]->print_url;
                $barcodeBoleto = !empty($b->charges[0]->last_transaction->gateway_response_fields->typable_barcode) ? $b->charges[0]->last_transaction->gateway_response_fields->typable_barcode : '';

                $comment = "Pagamento por Boleto<br>";
                $comment .= '<b><a href="' . $urlBoleto . '" target="_blank">Abrir Boleto</a></b>';
            }

            if (empty($comment) && !empty($b->charges[0]->last_transaction->payment_profile->card_number_first_six)) {
                $bandeira = $b->charges[0]->last_transaction->payment_profile->payment_company->name;
                $numero = $b->charges[0]->last_transaction->payment_profile->card_number_last_four;
                $nome = $b->charges[0]->last_transaction->payment_profile->holder_name;

                $parcelas = $b->charges[0]->last_transaction->installments;
                $total = $b->charges[0]->last_transaction->amount;
                $total = $this->currency->format(($total), $this->session->data['currency']);

                if ($parcelas == 1) {
                    $pagamento = $parcelas . ' parcela, total: ' . $total;
                } else {
                    $pagamento = $parcelas . ' parcelas, total: ' . $total;
                }

                $comment = "Pagamento por Cartão " . $bandeira;
                $comment .= "<br>";
                $comment .= "Os 4 últimos números do Cartão: " . $numero;
                $comment .= "<br>";
                $comment .= $pagamento;
                $comment .= "<br><br>";
                $comment .= "Dados do Titular do Cartão:";
                $comment .= "<br>";
                $comment .= "Nome: " . $nome;
            }

            if (empty($comment)) {
                $comment = 'Pagamento por Cartão';
            }

            $bills[] = [
                'created_at' => $created_at,
                'updated_at' => $updated_at,
                'amount'     => $amount,
                'status'     => $order_status->row['name'],
                'comment'    => $comment,
            ];
        }

        //print_r($bills); exit();
        return ['sub' => $sub, 'bills' => $bills];
    }

    public function getAssinaturas($id)
    {
        $subscriptionService = new \Vindi\Subscription();
        return $subscriptionService->get($id);
    }

    public function addAssinatura($customer_id, $product_id, $order, $payment)
    {
        //Puxando o Produto com assinatura
        $product = $this->cart->getRecurringProducts();
        if (empty($product[0])) {
            return false;
        }
        $product = $product[0];

        $assinatura_id = $product['recurring']['recurring_id'];
        $query_r = $this->db->query("SELECT * FROM `" . DB_PREFIX . "recurring` WHERE recurring_id = '" . (int) $assinatura_id . "'");

        if (isset($query_r->row) and ($query_r->row['vindi_plano_id'])) {
            $vindi_plano_id = $query_r->row['vindi_plano_id'];
        }

        if (empty($vindi_plano_id)) {
            $this->log->write('addAssinatura - Sem o ID do Plano da Vindi no Produto');
            return false;
        }

        $subscriptionService = new Subscription();
        $payment_method_code = $payment['payment_method_code'];

        try {
            if ($payment_method_code == 'credit_card') {
                $sub = $subscriptionService->create([
                    'plan_id'             => $vindi_plano_id,
                    'code'                => $order['order_id'],
                    'customer_id'         => $customer_id,
                    'payment_method_code' => $payment_method_code,
                    'product_items'       => [
                        [
                            'product_id' => $product_id,
                            'quantity'   => 1,
                        ],
                    ],
                    'payment_profile'     => [
                        'payment_method_code' => 'credit_card',
                        'gateway_token'       => $payment['token'],
                    ],
                ]);
            } else {
                $sub = $subscriptionService->create([
                    'plan_id'             => $vindi_plano_id,
                    'code'                => $order['order_id'],
                    'customer_id'         => $customer_id,
                    'payment_method_code' => $payment_method_code,
                    'product_items'       => [
                        [
                            'product_id' => $product_id,
                            'quantity'   => 1,
                        ],
                    ],
                ]);
            }

            //$lastResponse = $subscriptionService->getLastResponse()->getBody();
            //$sub = json_decode($lastResponse, false);
            $this->log->write('addAssinatura - Assinatura, dados: ' . print_r($sub, true));

            $bill = $this->getTransactions("subscription_id=$sub->id");
            $this->updateOrder($bill[0], $order['order_id']);

            //Salvando a Assinatura
            /*
             * Array ( [0] => Array ( [cart_id] => 155 [product_id] => 29 [name] => Produto Diário - Vindi [model] => Product 2 [shipping] => 1
             * [image] => catalog/demo/palm_treo_pro_1.jpg [option] => Array ( ) [download] => Array ( ) [quantity] => 1 [minimum] => 1
             * [subtract] => 1 [stock] => 1 [price] => 8 [total] => 8 [reward] => 0 [points] => 0 [tax_class_id] => 9 [weight] => 0.3
             *  [weight_class_id] => 1 [length] => 30.00000000 [width] => 20.00000000 [height] => 20.00000000 [length_class_id] => 1 [
             * recurring] => Array ( [recurring_id] => 3 [name] => Diário Vindi [frequency] => day [price] => 8.0000 [cycle] => 1
             * [duration] => 0 [trial] => 0 [trial_frequency] => day [trial_price] => 0.0000 [trial_cycle] => 1 [trial_duration] => 0 ) ) )

             */
            $data = [
                'product_id'      => $product['product_id'],
                'name'            => $product['name'],
                'quantity'        => 1,
                'recurring_id'    => $product['recurring']['recurring_id'],
                'frequency'       => $product['recurring']['frequency'],
                'cycle'           => $product['recurring']['cycle'],
                'duration'        => $product['recurring']['duration'],
                'price'           => $product['recurring']['price'],
                'trial'           => $product['recurring']['trial'],
                'trial_frequency' => $product['recurring']['trial_frequency'],
                'trial_price'     => $product['recurring']['trial_price'],
                'trial_cycle'     => $product['recurring']['trial_cycle'],
                'trial_duration'  => $product['recurring']['trial_duration'],
            ];

            //Status 6 como padrão, ou seja, Pendente
            $this->load->model('checkout/recurring');
            $orderRecurringId = $this->model_checkout_recurring->addRecurring($order['order_id'], 'Assinatura Criada', $data);
            $this->webhookAssinatura($sub->id, $orderRecurringId);

            return true;
        } catch (ValidationException $v) {
            $this->log->write('addAssinatura - Erro na assiantura: ' . print_r($v, true));
            //print_r($v);
            return false;
        }
    }

    /**
     *
     * Retorna a configuração do Status
     *
     * @param $status
     *
     * @return |null
     */
    private function getStatus($status)
    {
        $result = null;

        switch ($status) {
            case 'review':
                $result = $this->conf->code_charge_review;
                break;
            case 'pending':
                $result = $this->conf->code_charge_pending;
                break;
            case 'paid':
                $result = $this->conf->code_charge_paid;
                break;
            case 'canceled':
                $result = $this->conf->code_charge_canceled;
                break;
            case 'scheduled':
                $result = $this->conf->code_charge_scheduled;
                break;
        }

        return $result;
    }

    public function webhook()
    {
        sleep(5);
        //Dados
        $json = file_get_contents('php://input');
        $data = json_decode($json, true);

        $log = new Log('Code-Vindi-Webhook-' . date("m-Y") . '.log');
        $log->write("webhook - Dentro do Webhook");
        $log->write("webhook - Dados do Post: " . print_r(json_encode($data, JSON_PRETTY_PRINT), true));

        if (empty($data['event']['type'])) {
            $log->write("webhook - Sem evento definido");
            return false;
        }

        $type = $data['event']['type'];
        $status = '';
        if (!empty($data['event']['data']['charge']['last_transaction']['status'])) {
            $status = $data['event']['data']['charge']['last_transaction']['status'];
        }

        $log->write('Type: ' . $type . " Status: " . $status);

        // Status não pareado
        if ($type == 'bill_paid') {
            $status = 'code_charge_paid';
        } else if (!empty($status)) {
            if ($type == 'charge_canceled' && $status == 'success') {
                $status = 'code_charge_canceled';
            } else if ($type == 'charge_canceled' && $status == 'rejected') {
                $status = 'code_charge_rejected';
            } else {
                $status = 'code_charge_' . $status;
            }
        } else {
            $log->write('AVISO - Sem Status definido no evento da Vindi');
            return false;
        }

        $log->write('Status Loja: ' . $status);

        if ($status == 'code_charge_waiting' || $status == 'code_charge_created') {
            $log->write('Status Aguardando ou Criado - Já usado na Criação da Cobrança');
            return false;
        }

        if ($status == 'code_charge_success') {
            $status = 'code_charge_paid';
        }

        if (empty($this->conf->{$status})) {
            $log->write('Sem Status definido: ' . $status);
            return false;
        }

        $status = $this->conf->{$status};

        if (!empty($data['event']['data']['charge']['bill']['id'])) {
            $bill_id = $data['event']['data']['charge']['bill']['id'];
        } else if (!empty($data['event']['data']['bill']['id'])) {
            $bill_id = $data['event']['data']['bill']['id'];
        } else {
            //Assinatura, mudando o Status
            if (!empty($data['event']['data']['subscription']['id'])) {
                $log->write('Chamando o webhookAssinatura()');
                $this->webhookAssinatura($data['event']['data']['subscription']['id']);
            }

            $log->write('Sem o ID da Fatura Vindi');
            return false;
        }

        // Dados estranhos, pedido não pareado (provavelmente veio de outra plataforma)
        $queryOrder = $this->db->query('SELECT * FROM ' . DB_PREFIX . 'order WHERE vindi_order_id = ' . (int) $bill_id . ' LIMIT 1');

        if (!$queryOrder->num_rows) {
            $log->write("Nenhum Pedido encontrado com o ID da Vindi, provável que seja Transações da Assinatura ou criada em outra plataforma");
            return true;
        }

        $order_id = $queryOrder->row['order_id'];

        $queryS = $this->db->query('SELECT * FROM ' . DB_PREFIX . 'order_recurring WHERE order_id = ' . (int) $order_id . ' LIMIT 1');

        //Assinatura, mudando o Status
        if (!empty($queryS->row['vindi_assinatura_id'])) {
            $log->write('Chamando o webhookAssinatura()');
            $this->webhookAssinatura($queryS->row['vindi_assinatura_id']);
        }

        // Status já trabalhado apenas retorna OK para o webhook nao tentar novamente
        if ($queryOrder->row['order_status_id'] === $status) {
            $log->write("Pedido: " . (int) $order_id . ", Status igual, já notificado");
            return true;
        }

        //Verificando se já tem o Status no Pedido
        $query = $this->db->query("SELECT order_id FROM " . DB_PREFIX . "order_history 
            WHERE 
            order_id = '" . (int) $order_id . "' 
            AND 
            order_status_id = '" . (int) $status . "'
            LIMIT 1
         ");

        $log->write("Retorno da verificação: " . print_r($query->row, true));

        if (!empty($query->row['order_id'])) {
            $log->write("Pedido: " . (int) $order_id . ", já notificado");

            return true;
        }

        $this->load->model('checkout/order');

        $statusAlert = false;
        if (!empty($this->conf->code_status_notify) and $this->conf->code_status_notify == 1) {
            $statusAlert = true;
        }

        $this->model_checkout_order->addOrderHistory(
            (int) $order_id,
            $status,
            '',
            $statusAlert
        );

        $log->write('Pedido ' . (int) $order_id . ' notificado com sucesso');

        return true;
    }

    public function webhookAssinatura($subId, $orderRecurringId = null)
    {
        /*
           admin/language/pt-br/sale/recurring.php
           $_['text_status_1']                        = 'Ativa';
           $_['text_status_2']                        = 'Inativa';
           $_['text_status_3']                        = 'Cancelada';
           $_['text_status_4']                        = 'Suspensa';
           $_['text_status_5']                        = 'Expirada';
           $_['text_status_6']                        = 'Pendente';
           */

        //Adicionado o da Assinatura da Vindi no order_recurring
        $status = 6;

        //Buscando a Assinatura pelo ID
        $sub = $this->getAssinaturas($subId);
        if ($subId != $sub->id) {
            $this->log->write('webhookAssinatura - ID da Assinatura diferente do retornado na busca');
            return false;
        }

        //status (string) = ['active' ou 'canceled' ou 'future' ou 'expired']: Status da assinatura,
        $subStatus = $sub->status;
        //$this->log->write('webhookAssinatura - Dados Assinatura: ' . print_r($sub, true));

        //Data do vencimento do Pagamento da assinatura
        $subOverdueSince = $sub->overdue_since;

        //Listando as transações da assinatura pelo ID da Assinatura
        $transactions = $this->getTransactions("subscription_id=$subId");

        //Se assiantura diferente de canceled ou expired
        if ($subStatus != 'canceled' || $subStatus != 'expired') {
            $this->log->write('webhookAssinatura - Status diferente de Cancelado ou Expirado');

            foreach ($transactions as $b) {
                //$this->log->write('webhookAssinatura - Dados Transação: ' . print_r($b, true));

                if (empty($b->subscription->id) || $b->subscription->id != $subId) {
                    $this->log->write('webhookAssinatura - Sem o ID da Assinatura ou diferente');
                    break;
                }

                if (!empty($b->charges[0]->last_transaction->status)) {
                    $status = 'code_charge_' . $b->charges[0]->last_transaction->status;
                } else {
                    $this->log->write('webhookAssinatura - Sem Status definido na Vindi, assiantura: ' . $subId);
                    continue;
                }

                $status = 'code_charge_' . $b->charges[0]->last_transaction->status;

                if ($status == 'code_charge_waiting') {
                    $status = 'code_charge_created';
                }

                if ($status == 'code_charge_success') {
                    $status = 'code_charge_paid';
                }

                if (empty($this->conf->{$status})) {
                    $this->log->write('Sem Status definido: ' . $status);
                    return false;
                }

                switch ($status) {
                    case 'code_charge_paid':
                        //Só se não expirou
                        if (empty($subOverdueSince)) {
                            $status = 1;
                        }
                        break;
                    case 'code_charge_canceled':
                        $status = 2;
                        break;
                    case 'code_charge_rejected':
                        $status = 2;
                        break;
                    case 'code_charge_refunded':
                        $status = 2;
                        break;
                }

                //Se tiver uma Fatura Cancelada, para o laço
                if ($status == 3) {
                    break;
                }
            }

            //Assinatura existente, se a Vindi diz que está ativa e tem data de expirado, então mudar para inativa
            if ($status == 6 && $subStatus == 'active' && !empty($subOverdueSince)) {
                $status = 2;
            }
        } else {
            $this->log->write('webhookAssinatura - Status Cancelado ou Expirado');

            switch ($subStatus) {
                case 'canceled':
                    $status = 3;
                    break;
                case 'expired':
                    $status = 5;
                    break;
            }
        }

        //Salvando os dados da Assinatura criada e Status
        if ($orderRecurringId) {
            //Mudando o Status da Assinatura e setando o ID do vindi assinatura
            $this->db->query("
                UPDATE " . DB_PREFIX . "order_recurring 
                SET 
                status = '" . (int) $status . "',
                vindi_assinatura_id = '" . (int) $subId . "' 
                WHERE 
                order_recurring_id = '" . (int) $orderRecurringId . "'
            ");

            $this->log->write('webhookAssinatura - Assinatura salva com sucesso');
        } else if ($status != 6) {
            //Mudando o Status da Assinatura
            $this->db->query("
                UPDATE " . DB_PREFIX . "order_recurring 
                SET 
                status = '" . (int) $status . "'
                WHERE 
                vindi_assinatura_id = '" . (int) $subId . "' 
            ");
        }

        $this->log->write('webhookAssinatura - Assinatura modificada o Status com sucesso, Status: ' . $status);

        return true;
    }
}
