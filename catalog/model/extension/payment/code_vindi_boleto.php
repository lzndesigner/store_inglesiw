<?php

class ModelExtensionPaymentCodeVindiBoleto extends Model
{
    /**
     * @var \stdClass
     */
    private $conf;
    private $log;
    private $status;

    /**
     * Definicoes iniciais de boot
     *
     * @param $registry
     *
     * @throws \Exception
     */
    public function __construct($registry)
    {
        parent::__construct($registry);

        $this->load->model('checkout/order');
        $this->load->model('module/codemarket_module');
        $this->conf = $this->model_module_codemarket_module->getModulo('570');
        $this->log = new Log('Code-Vindi-Boleto.log');

        $this->status = $this->config();
    }

    public function config()
    {
        if (empty($this->conf) || empty($this->conf->code_sandbox_private) || empty($this->conf->code_production_private) || empty($this->conf->code_production_public)
            || empty($this->conf->code_sandbox_public) || empty($this->conf->code_title_boleto) || empty($this->conf->code_botao_boleto) || empty($this->conf->code_mensagem_boleto)
            || !isset($this->conf->code_geo_zones_boleto) || !isset($this->conf->code_sort_order_boleto) || empty($this->conf->code_env) || empty($this->conf->code_status_boleto)
            || $this->conf->code_status_boleto != 1 || empty($this->conf->code_vencimento)) {
            $this->log->write('Model config() - Módulo desativado ou faltando dados na configuração');

            return false;
        }

        /*
           Modo Cliente de Teste
           Desativa se não for o cliente de teste e estiver habilitado o modo de teste
       */
        if (!empty($this->conf->code_env) && $this->conf->code_env == 'sandbox' && !empty($this->conf->code_test_email)) {
            if (!empty($this->customer->getEmail()) && $this->conf->code_test_email != $this->customer->getEmail()) {
                return false;
            } else if (empty($this->customer->getEmail())) {
                return false;
            }
        }

        if ($this->conf->code_env === 'production') {
            $this->conf->code_env_mensagem = '';
        } else {
            $this->conf->code_env_mensagem = " - <b>Modo de Teste, não compre!</b>";
        }

        return true;
    }

    public function region($address)
    {
        //Região
        if (!empty($this->conf->code_geo_zones_boleto)) {
            $query = $this->db->query("
                SELECT geo_zone_id FROM " . DB_PREFIX . "zone_to_geo_zone WHERE 
                geo_zone_id = '" . (int) $this->conf->code_geo_zones_boleto . "' 
                AND country_id = '" . (int) $address['country_id'] . "' 
                AND (zone_id = '" . (int) $address['zone_id'] . "' OR zone_id = '0')
                LIMIT 1
            ");

            if (empty($query->row['geo_zone_id'])) {
                $this->log->write('Model region() - Região Geográfica não encontrada' . print_r($query->row, true));

                return false;
            }
        }

        return true;
    }

    /**
     * Metodo padrao pra exibicao na listagem
     *
     * @param $address
     * @param $total
     *
     * @return array
     */
    public function getMethod($address, $total)
    {
        if (empty($this->region($address)) || empty($this->status)) {
            return [];
        }

        return [
            'code'       => 'code_vindi_boleto',
            'title'      => $this->conf->code_title_boleto . $this->conf->code_env_mensagem,
            'terms'      => '',
            'sort_order' => $this->conf->code_sort_order_boleto,
        ];
    }

    /**
     * Informa se o Pagamento suporta assinatura
     *
     * @return bool
     */
    public function recurringPayments()
    {
        return true;
    }
}
