<?php

error_reporting(0);

/**
 * © Copyright 2013-2025 Codemarket - Todos os direitos reservados.
 * API para o Bling ERP criada pela equipe da CODEMARKET INOVACAO EM TI LTDA
 * Class ControllerFeedRestApi
 */
class ControllerFeedRestApi extends Controller
{
    private $debugIt = false;
    private $secretKey;
    private $conf = '';
    private $log;
    private $debugLog = false;
    private $codeCache;
    private $miniCodeCache;

    /**
     * ControllerFeedRestApi constructor.
     *
     * @param $registry
     *
     * @throws \Exception
     */
    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->load->model('module/code_bling');
        $this->load->model('module/code_bling_native_order');
        $this->load->model('module/code_bling_native_product');
        $this->load->model('catalog/product');

        $this->load->model('module/codemarket_module');
        $conf = $this->model_module_codemarket_module->getModulo('548');
        $this->log = new log('Code-Bling-' . date('m-Y') . '.log');

        if (
            empty($conf) || empty($conf->code_habilitar)
            || (int) $conf->code_habilitar === 2
            || empty($conf->code_payment_id)
            || empty($conf->code_chave)
        ) {
            $this->log->write(
                'ControllerFeedRestApi__construct() - Bling desativado, verifique a configuração'
            );
            exit('Desativado ou sem configuração');
        }

        if (!empty($conf->code_debug_log) && (int) $conf->code_debug_log === 1) {
            $this->debugLog = true;
        }

        $this->conf = $conf;
        $this->secretKey = trim($conf->code_chave);

        if (!empty($this->request->get['debug'])
            && (int) $this->request->get['debug'] === 1
        ) {
            $this->debugIt = true;
        }

        if (version_compare(VERSION, '3.0.0.0') < 0) {
            $cache_type = $this->config->get('cache_type');
        } else {
            $cache_type = $this->config->get('cache_engine');
        }

        // 172800s = 48h - Cache
        //$this->codeCache = new Cache($cache_type, 172800);

        // 10 dias
        $this->codeCache = new Cache($cache_type, 864000);

        // 900s = 15 minutos - Cache
        $this->miniCodeCache = new Cache($cache_type, 900);
    }

    /**
     *
     * Teste, usado principalmente no Desenvolvimento
     * @return void
     */
    public function testAPI()
    {
        $this->checkPlugin();
        echo "<h1>Rodando o testAPI</h1>";
        $this->model_module_code_bling->testAPI();
    }

    /**
     * Homologação no Bling ERP
     * @return void
     */
    public function homologationAPI()
    {
        $this->checkPlugin();
        $this->model_module_code_bling->homologation();
    }

    /*
    * Retorna os Produtos
    * Get products
    */
    public function products()
    {
        $this->checkPlugin();
        $this->log->write('ControllerFeedRestApi_products() - Rodando');
        $products
            = $this->model_module_code_bling_native_product->getAllProduct();

        if (count($products)) {
            foreach ($products as $product) {
                $variation
                    = $this->model_module_code_bling_native_product->getVariation(
                    $product
                );

                $newVariation = [];
                if (!empty($variation)) {
                    foreach ($variation as $v) {
                        if (empty($v['sku'])) {
                            continue;
                        }

                        $v['tipoVariacao'] = strip_tags(
                            trim($v['tipoVariacao'])
                        );
                        $v['tipoVariacao'] = str_replace(
                            ['"', "″"],
                            "",
                            $v['tipoVariacao']
                        );
                        $newVariation[] = $v;
                    }
                }
                //HookGetVariation

                if (empty($product['product_id'])) {
                    continue;
                }

                $codeProduct = $this->model_module_code_bling->getProduct(
                    $product['product_id']
                );

                if (empty($codeProduct['product_id'])) {
                    continue;
                }

                $productSKU = '';
                if (!empty($codeProduct['sku'])) {
                    $productSKU = $codeProduct['sku'];
                }

                if (!empty($this->conf->code_produto_ncm)
                    && !empty($codeProduct[$this->conf->code_produto_ncm])
                ) {
                    $productNCM = preg_replace(
                        '/[^0-9]/',
                        "",
                        trim($codeProduct[$this->conf->code_produto_ncm])
                    );
                } else {
                    $productNCM = '';
                }

                if (!empty($this->conf->code_produto_cest)
                    && !empty($codeProduct[$this->conf->code_produto_cest])
                ) {
                    $productCEST = preg_replace(
                        '/[^0-9]/',
                        "",
                        trim($codeProduct[$this->conf->code_produto_cest])
                    );
                } else {
                    $productCEST = '';
                }

                if (!empty($codeProduct['special'])) {
                    $productPricePromo = $codeProduct['special'];
                } else {
                    $productPricePromo = '';
                }

                if (!empty($codeProduct['manufacturer'])) {
                    $productBrand = $codeProduct['manufacturer'];
                } else {
                    $productBrand = '';
                }

                if (empty($newVariation)) {
                    $newVariation = null;
                }

                if (!empty($product['name'])) {
                    $aProducts[] = [
                        'id' => $product['product_id'],
                        'name' => $codeProduct['name'],
                        'description' => 'b64' . base64_encode(
                                $codeProduct['description']
                            ),
                        'model' => 'b64' . base64_encode($codeProduct['model']),
                        'sku' => $productSKU,
                        'ncm' => $productNCM,
                        'cest' => $productCEST,
                        'brand' => $productBrand,
                        'pricePromo' => $productPricePromo,
                        'quantity' => $product['quantity'],
                        'price' => $product['price'],
                        'weight' => $this->convertWeight($product['weight']),
                        'length' => $this->convertLength($product['length']),
                        'width' => $this->convertLength($product['width']),
                        'height' => $this->convertLength($product['height']),
                        'attribute' => '',
                        'variation' => $newVariation,
                    ];
                }
            }
            $json['success'] = true;
            $json['products'] = $aProducts;
        } else {
            $json['success'] = false;
            $json['error'] = "Problema para retornar os Produtos.";
        }

        $this->log->write(
            'ControllerFeedRestApi_products() - Rodado com sucesso'
        );

        if ($this->debugIt) {
            echo '<pre>';
            print_r($json);
            echo '</pre>';
        } else {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    public function count_products()
    {
        $this->checkPlugin();
        $filters = $this->getParameter();
        $products
            = $this->model_module_code_bling_native_product->getCountProduct();

        foreach ($products as $product) {
            if ($product['NrProducts'] > 0) {
                $json['success'] = true;
                $json['products'] = $product['NrProducts'];
            } else {
                $json['success'] = false;
                $json['error']
                    = "Problema para retornar a quantidade dos Produtos.";
            }
        }
        if ($this->debugIt) {
            echo '<pre>';
            print_r($json);
            echo '</pre>';
        } else {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    /**
     *  Products by FILTERS
     *  Usado para o Bling puxar os Produtos da Loja
     *  Limites: https://ajuda.bling.com.br/hc/pt-br/articles/360046302394-Limites
     */
    public function products_filters()
    {
        $this->checkPlugin();
        $this->log->write('ControllerFeedRestApi_products_filters() - Rodando');

        $filters = $this->getParameter();
        $products
            = $this->model_module_code_bling_native_product->getAllProductFilters(
            $filters
        );

        //Cache para usar no updateProdutoBlingApiV2()
        //$this->cache->delete('codebling_products_filters');

        // Montando o Cache
        $productsCacheData = [];

        if (!empty($this->codeCache->get('codebling_products_filters'))) {
            $productsCache = $this->codeCache->get(
                'codebling_products_filters'
            );

            foreach ($productsCache as $sku => $product_id) {
                $productsCacheData[$sku] = $product_id;
            }

            foreach ($products as $p) {
                if (empty($p["sku"])) {
                    continue;
                }

                $productsCacheData[$p["sku"]] = $p['product_id'];
            }

            $this->codeCache->set(
                'codebling_products_filters',
                $productsCacheData
            );
        } else {
            foreach ($products as $p) {
                if (empty($p["sku"])) {
                    continue;
                }

                $productsCacheData[$p["sku"]] = $p['product_id'];
            }

            $this->codeCache->set(
                'codebling_products_filters',
                $productsCacheData
            );
        }

        //print_r($this->codeCache->get('codebling_products_filters'));

        //Verificando se tem Produtos retornados
        if (count($products)) {
            foreach ($products as $product) {
                // Verificando se tem variações/opções no Produto
                $variation
                    = $this->model_module_code_bling_native_product->getVariation(
                    $product
                );

                $newVariation = [];
                if (!empty($variation)) {
                    foreach ($variation as $v) {
                        if (empty($v['sku'])) {
                            continue;
                        }

                        $v['tipoVariacao'] = strip_tags(
                            trim($v['tipoVariacao'])
                        );
                        $v['tipoVariacao'] = str_replace(
                            ['"', "″"],
                            "",
                            $v['tipoVariacao']
                        );
                        $newVariation[] = $v;
                    }
                }
                //HookGetVariation

                // Sem o ID do Produto
                if (empty($product['product_id'])) {
                    continue;
                }

                // Puxando mais dados do Produto
                $codeProduct = $this->model_module_code_bling->getProduct(
                    $product['product_id']
                );

                // Sem o ID do Produto
                if (empty($codeProduct['product_id'])) {
                    continue;
                }

                $productSKU = '';
                if (!empty($codeProduct['sku'])) {
                    $productSKU = $codeProduct['sku'];
                }

                if (!empty($this->conf->code_produto_ncm)
                    && !empty($codeProduct[$this->conf->code_produto_ncm])
                ) {
                    $productNCM = preg_replace(
                        '/[^0-9]/',
                        "",
                        trim($codeProduct[$this->conf->code_produto_ncm])
                    );
                } else {
                    $productNCM = '';
                }

                if (!empty($this->conf->code_produto_cest)
                    && !empty($codeProduct[$this->conf->code_produto_cest])
                ) {
                    $productCEST = preg_replace(
                        '/[^0-9]/',
                        "",
                        trim($codeProduct[$this->conf->code_produto_cest])
                    );
                } else {
                    $productCEST = '';
                }

                if (!empty($codeProduct['special'])) {
                    $productPricePromo = $codeProduct['special'];
                } else {
                    $productPricePromo = '';
                }

                if (!empty($codeProduct['manufacturer'])) {
                    $productBrand = $codeProduct['manufacturer'];
                } else {
                    $productBrand = '';
                }

                if (empty($newVariation)) {
                    $newVariation = null;
                }

                $product
                    = $this->model_module_code_bling->correctedProductStoreDescription(
                    $product
                );

                // Dados do Produto montado
                $productsData[] = [
                    'id' => $product['product_id'],
                    'name' => $codeProduct['name'],
                    'description' => 'b64' . base64_encode(
                            $codeProduct['description']
                        ),
                    'model' => 'b64' . base64_encode($codeProduct['model']),
                    'sku' => $productSKU,
                    'ncm' => $productNCM,
                    'cest' => $productCEST,
                    'brand' => $productBrand,
                    'pricePromo' => $productPricePromo,
                    'quantity' => $product['quantity'],
                    'price' => $product['price'],
                    'weight' => $this->convertWeight($product['weight']),
                    'length' => $this->convertLength($product['length']),
                    'width' => $this->convertLength($product['width']),
                    'height' => $this->convertLength($product['height']),
                    'attribute' => '',
                    'variation' => $newVariation,
                ];
            }
            $json['success'] = true;
            $json['products'] = $productsData;
        } else {
            $this->log->write(
                'ControllerFeedRestApi_products_filters() - Erro'
            );

            $json['success'] = false;
            $json['error'] = "Nenhum Produto encontrado no período informado.";
        }

        $this->log->write(
            'ControllerFeedRestApi_products_filters() - Rodado com sucesso para '
            . count($products) . ' produtos'
        );

        if ($this->debugIt) {
            echo '<pre>';
            print_r($json);
            echo '</pre>';
        } else {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    /**
     *  Callback dos Estoques
     *  https://ajuda.bling.com.br/hc/pt-br/articles/360046387754-Callback-de-altera%C3%A7%C3%A3o-de-estoque-API
     */
    public function callbackStock()
    {
        $this->log->write(
            "ControllerFeedRestApi_callbackStock() - Dentro do Webhook"
        );

        //Dados
        $json = file_get_contents('php://input');
        $this->log->write(
            'ControllerFeedRestApi_callbackStock() - Dados Json: ' . print_r(
                $json,
                true
            )
        );

        //Enviam com data= antes do Json
        $json = str_replace('data=', '', $json);
        $data = json_decode($json, true);
        //$this->log->write('Dados do Post Json: ' . print_r($data, true));

        if (!empty($data['retorno']['estoques'][0]['estoque'])) {
            $this->model_module_code_bling->runChangeStockProduct(
                $data['retorno']['estoques'][0]['estoque']
            );
            $this->log->write(
                'ControllerFeedRestApi_callbackStock() - Rodado com sucesso'
            );
        } else {
            $this->log->write(
                'ControllerFeedRestApi_callbackStock() - Nenhum Estoque retornado'
            );
        }

        $json = ["success" => true];
        $this->response->setOutput(json_encode($json));
    }

    /**
     *  Callback da Situação do Pedido
     *  https://ajuda.bling.com.br/hc/pt-br/articles/360046387754-Callback-de-altera%C3%A7%C3%A3o-de-estoque-API
     */
    public function callbackOrder()
    {
        $this->log->write(
            "ControllerFeedRestApi_callbackOrder() - Dentro do Webhook"
        );

        //Dados
        $json = file_get_contents('php://input');
        $this->log->write(
            'ControllerFeedRestApi_callbackOrder() - Dados Json: ' . print_r(
                $json,
                true
            )
        );

        //Enviam com data= antes do Json
        $json = str_replace('data=', '', $json);
        $data = json_decode($json, true);
        //$this->log->write('Dados do Post Json: ' . print_r($data, true));

        if (!empty($data['retorno']['pedidos'])) {
            $this->model_module_code_bling->runChangeStatusOrders($data);
            $this->log->write(
                'ControllerFeedRestApi_callbackOrder() - Rodado com sucesso'
            );
        } else {
            $this->log->write(
                'ControllerFeedRestApi_callbackOrder() - Nenhum Pedido retornado'
            );
        }

        $json = ["success" => true];
        $this->response->setOutput(json_encode($json));
    }

    /**
     * Verifica os Dados dos Produtos do Bling ERP com o Opencart
     * Focado inicialmente em verificar o Preço se está igual 08/01/2022
     *
     * @return string
     */
    public function verifyOrders(): string
    {
        $this->checkPlugin();
        echo "<h1>Rodando o verifyOrders()</h1>";

        // Verificando os Preços dos Pedidos se está igual da Loja
        $this->model_module_code_bling->verifyOrders();
        echo "<h2 style='color:#47d35d'>Rodado com sucesso o verifyOrders()</h2>";
    }

    /**
     * Mudando a Situação dos Pedidos conforme o retorno no Bling ERP
     * Se usar o callbackOrder não precisa usar este método
     */
    public function changeStatusOrder()
    {
        $this->checkPlugin();
        echo "<h3>Rodando - Alterando Status dos Pedidos - Horário: " . date(
                'i:s'
            ) . "</h3>";
        $this->log->write(
            'ControllerFeedRestApi_changeStatusOrder() - Rodando'
        );

        $this->model_module_code_bling->changeStatusOrders();

        echo "<h3 style='color:#47d35d'>Rodando com Sucesso - Horário: " . date('i:s') . "</h3>";
        exit();
    }

    /**
     * Envia dados extras dos Produtos para o Bling, exceto a imagem, foco no Preço e Quantidade
     * https://www.site.com.br/index.php?route=feed/rest_api/updatePriceAndStockBlingApiV2&key=KEY&second=
     * Bling -> Opencart
     * Mesmos recursos do updateProdutoBlingApiV2 e o second na URL
     *
     * @uses updateProdutoBlingApiV2() Atualiza os Produtos no Bling ERP
     * @uses ModelModuleCodeBling::updateProdutoBling($product_id)
     */
    public function updatePriceAndStockBlingApiV2()
    {
        $this->checkPlugin();

        echo "<h1>Rodando updatePriceAndStockBlingApiV2 - Horário: " . date('i:s')
            . "</h1>";

        // https://www.mysqltutorial.org/mysql-interval/
        /**
         * https://ajuda.bling.com.br/hc/pt-br/articles/360046302394-Limites
         * As chamadas à nossa API são limitadas a no máximo 3 requisições por segundo e no máximo 120.000 requisições por dia. Esse limite é controlado por empresa.
         * Se configurado para rodar a cada minuto no Cron Job fica, 1440 chamadas por dia, se tem mil produtos atualizados em cada chamada, daria
         * 1000 * 1440 = 1.440.000 passaria do limite
         * Limite seria 120.000/1440 = 83 produtos por chamada, acima disso chegaria no limite
         */

        // 300s = 5 minutos
        $secondIntervalLimit = !empty($this->request->get['second'])
            ? (int) $this->request->get['second'] : 300;

        $product_id = !empty($this->request->get['product_id'])
            ? (int) $this->request->get['product_id'] : 0;

        if (empty($product_id)) {
            $products = $this->db->query(
                "SELECT product_id, sku FROM " . DB_PREFIX . "product WHERE
                (NOW() - INTERVAL '" . (int) $secondIntervalLimit . "' SECOND) <= date_modified
                "
            )->rows;
        } else {
            $products = $this->db->query(
                "SELECT product_id, sku FROM " . DB_PREFIX . "product WHERE
                product_id = '" . (int) $product_id . "' LIMIT 1
                "
            )->rows;
        }

        if (empty($products)) {
            exit("<h2>Sem Produtos retornados!</h2>");
        }

        $keyCache = 'codebling_products_stock_price';
        //print_r($products);

        // Montando o cache dos Produtos
        $productsCacheData = [];
        if (empty($this->codeCache->get($keyCache))
            || isset($this->codeCache->get($keyCache)[0])
        ) {
            foreach ($products as $p) {
                if (empty($p["sku"])) {
                    continue;
                }

                $productsCacheData[$p["sku"]] = $p['product_id'];
            }

            $this->codeCache->set(
                $keyCache,
                $productsCacheData
            );

            echo "<h3>Rodando para os Produtos do Banco</h3>";
        } else {
            /*
           Primeiro roda os Caches atuais, para depois rodar com novos
           */
            echo "<h3>Rodando para os Produtos do Cache</h3>";
        }

        //print_r($this->codeCache->get($keyCache));
        $this->updateProdutoBlingApiV2($keyCache);
    }

    /**
     * Envia dados extras dos Produtos para o Bling como imagens, descrição html, link externo e outros detalhes
     * https://www.site.com.br/index.php?route=feed/rest_api/updateProdutoBlingApiV2&key=KEY
     * Bling -> Opencart
     *
     * @uses ModelModuleCodeBling::updateProdutoBling($product_id)
     * @uses updatePriceAndStockBlingApiv2() Atualiza os produtos recentemente modificados, exceto as imagens
     */
    public function updateProdutoBlingApiV2(
        $keyCache = 'codebling_products_filters'
    ) {
        $this->checkPlugin();
        $start_time = microtime(true);
        $time_limit = !empty($this->request->get['time_limit'])
            ? (int) $this->request->get['time_limit'] : 80;

        $uniqid = md5(uniqid(rand(), true));
        $this->log->write(
            'ControllerFeedRestApi_updateProdutoBlingApiV2() - Rodando ' . $uniqid
        );

        echo "<h1>Rodando updateProdutoBlingApiV2 - Horário: " . date('i:s')
            . "</h1>";

        $products = (array) $this->codeCache->get($keyCache);

        echo "<h2>Produtos retornados do Cache: </h2>";
        print_r($products);

        if (empty($products) || isset($products[0])) {
            $this->log->write(
                'ControllerFeedRestApi_updateProdutoBlingApiV2() - Rodado - Sem produtos para rodar'
            );
            exit("<h1>Sem produtos para rodar<h1>");
        }

        $qts_products_runs = 0;
        $qts_products = count($products);

        echo "<h2>Rodando para " . $qts_products . " produtos - Horário: "
            . date('i:s')
            . "</h2>";

        foreach ($products as $sku => $product_id) {
            // Atualizando o Produto no Bling
            $update_product
                = $this->model_module_code_bling->updateProdutoBling(
                $product_id,
                $sku,
                $keyCache
            );

            /*
             * // Mesmo caso falhe, não pula, pois pode ser um produto que fique sempre na lista
            if(empty($update_product)){
                continue;
            }
            */

            $this->log->write(
                'ControllerFeedRestApi_updateProdutoBlingApiV2() - Rodado para o Produto '
                . $sku . ' ' . $product_id
            );

            unset($products[$sku]);

            //$qts_products = count($products);
            $qts_products_runs = $qts_products - (count($products));

            if ((microtime(true) - $start_time) >= ($time_limit - 3)) {
                // Atualizando o Cache dos Produtos 01/05/2022 19:46
                $this->codeCache->set(
                    $keyCache,
                    $products
                );

                $this->log->write(
                    'ControllerFeedRestApi_updateProdutoBlingApiV2() - Parando, chegou no tempo limite, rodou '
                    . $qts_products_runs . ' produtos, faltam '
                    . $qts_products . ' produtos '
                    . $uniqid
                );

                exit(
                    "<h2 style='color:#ec0000'>Parando, chegou no tempo limite, rodou "
                    . $qts_products_runs . ", faltou rodar para "
                    . $qts_products . " produtos - Horário: " . date(
                        'i:s'
                    ) . "</h2>"
                );
            }
        }

        //Limpando os cache
        $this->cache->delete($keyCache);
        $key = 'updateProdutoBling';
        array_map('unlink', glob(DIR_CACHE . 'cache.' . $key . '*'));

        $this->log->write(
            'ControllerFeedRestApi_updateProdutoBlingApiV2() - Rodado com sucesso para '
            . $qts_products_runs . ' produtos ' . $uniqid
        );
        exit(
            "<h1 style='color:#47d35d'>Rodado com sucesso para " . $qts_products_runs
            . " produtos - Horário: " . date('i:s') . "</h1>"
        );
    }

    /**
     * Limpa os Logs do Bling ERP
     */
    public function clearLogs()
    {
        $this->checkPlugin();

        //Limpando os Logs
        $key = 'Bling';
        array_map('unlink', glob(DIR_LOGS . 'Code-' . $key . '*'));
        exit("<h1 style='color:#47d35d'>Logs do Bling ERP limpado com sucesso!</h1>");
    }

    /**
     * Limpa os Cache do Bling ERP
     */
    public function clearCache()
    {
        $this->checkPlugin();

        //Limpando os Caches
        $key = 'codebling';
        array_map('unlink', glob(DIR_CACHE . 'cache.' . $key . '*'));

        $key = 'updateProdutoBling';
        array_map('unlink', glob(DIR_CACHE . 'cache.' . $key . '*'));

        $this->log->write(
            'ControllerFeedRestApi_clearCache() - Limpado os Caches'
        );

        echo "<h1 style='color:#47d35d'>Cache do Bling ERP limpado com sucesso!</h1>";
        exit();
    }

    public function infoCache()
    {
        $this->checkPlugin();

        echo "<h1>Exibindo os Produtos em Cache</h1>";

        echo "<h2>Cache da API updateProdutoBlingApiV2</h2>";
        if (!empty($this->codeCache->get('codebling_products_filters'))) {
            print_r((array) $this->codeCache->get('codebling_products_filters'));
        }

        echo "<h2>Cache da API updatePriceAndStockBlingApiV2</h2>";
        if (!empty($this->codeCache->get('codebling_products_stock_price'))) {
            print_r(
                (array) $this->codeCache->get('codebling_products_stock_price')
            );
        }

        exit();
    }

    /**
     * Inserindo ou atualizando Produtos
     * Bling -> Opencart
     */
    public function products_insert()
    {
        $this->checkPlugin();
        $this->log->write('ControllerFeedRestApi_products_insert() - Rodando');

        $parameters = urldecode($this->getParameter());
        if ($this->debugLog) {
            $this->log->write(
                'ControllerFeedRestApi_products_insert() - Parâmetros: '
                . print_r($parameters, true)
            );
        }

        $link
            = "https://www.bling.com.br/Integrations/Export/class-opencart-export-product.php?auth="
            . base64_encode($this->conf->code_chave) . "&parameters=" . $parameters;
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $link,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => 'UTF-8',
            CURLOPT_MAXREDIRS => 3,
            CURLOPT_TIMEOUT => 15,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => [
                "Cache-Control: no-cache",
                "Content-Type: application/json",
            ],
        ]);

        $returnCurl = curl_exec($curl);
        $error = curl_error($curl);
        //Obter info do Curl
        $curlInfo = curl_getinfo($curl);
        curl_close($curl);
        $result = json_decode($returnCurl);

        //Verificando o retorno do Curl
        if (empty($result->codigo) && empty($error)) {
            $this->log->write(
                'ControllerFeedRestApi_products_insert() - Erro, sem o Código/SKU no Produto!'
            );

            $json['success'] = false;
            $json['error'] = 'Problema para Salvar/Editar o Produto. Sem o Código/SKU no Produto!';
        } elseif (!empty($error)) {
            $httpCode = $curlInfo['http_code'];
            $this->log->write(
                'ControllerFeedRestApi_products_insert() - Erro no Curl, HTTP: '
                . $httpCode . ', URL: ' . $link . ' Retorno: ' . print_r(
                    $returnCurl,
                    true
                )
            );
            $this->log->write(
                'ControllerFeedRestApi_products_insert() - Erro no Curl, erros: '
                . print_r($error, true)
            );
            $this->log->write(
                'ControllerFeedRestApi_products_insert() - Info Curl: ' . print_r(
                    $curlInfo,
                    true
                )
            );

            $json['success'] = false;
            $json['error'] = "Curl HTTP Erro, HTTP: " . $httpCode . ", Erros: "
                . $error . ", Retorno: " . $returnCurl;
            $json['url'] = $link;
        } else {
            //Limpando o Cache
            $key = 'codebling_product_';
            array_map('unlink', glob(DIR_CACHE . 'cache.' . $key . '*'));

            $products
                = $this->model_module_code_bling_native_product->insert_oc_products(
                $result
            );

            if ($this->debugLog) {
                $this->log->write(
                    'ControllerFeedRestApi_products_insert() - Result dados: '
                    . print_r($result, true)
                );
                $this->log->write(
                    'ControllerFeedRestApi_products_insert() - Após Inserir/Atualizar Produto: '
                    . print_r($products, true)
                );
            }

            if (empty($products)) {
                $json['success'] = false;
                $json['error']
                    = "Problema para Salvar/Editar o Produto. Verifique os logs e se a loja não está em manutenção";
            }

            //Atualizando os dados do Produto como a Descrição
            if (isset($products['id'])) {
                if ($products['returnUp']) {
                    $description
                        = $this->model_module_code_bling->updateProductStore(
                        $products['id']
                    );
                    $json['desc'] = $description;

                    if ($description) {
                        $json['idProduto'] = $products['id'];
                        $json['success'] = true;
                    } else {
                        $json['success'] = false;
                        $json['error']
                            = "Problema para salvar a descrição do Produto. Verifique os logs e se a loja não está em manutenção 2";
                    }
                } else {
                    $json['success'] = false;
                    $json['error'] = "Problema para atualizar o Produto.";
                }
            } else {
                if (!empty($products['option_id'])) {
                    $json['idProduto'] = $products['option_id'];
                    $json['success'] = true;
                } else {
                    if (!empty($products['option'])) {
                        $json['idProduto'] = 0;
                        $json['success'] = true;
                    } else {
                        //Inserindo os dados da Descrição
                        foreach ($products as $prod) {
                            $description
                                = $this->model_module_code_bling_native_product->insert_oc_description(
                                $result,
                                $prod['maximo']
                            );
                            $this->model_module_code_bling->updateProductStore(
                                $prod['maximo']
                            );
                            //$this->log->write('ControllerFeedRestApi_products_insert() - Descrição: '.print_r($description, true));

                            foreach ($description as $desc) {
                                if ($desc['idMax'] != $prod['maximo']) {
                                    $this->log->write(
                                        'ControllerFeedRestApi_products_insert() - Erro Descrição'
                                    );

                                    $this->model_module_code_bling_native_product->delete_oc_products(
                                        $prod['maximo']
                                    );
                                    $json['success'] = false;
                                    $json['error']
                                        = "Problemas para Salvar o Produto. Verifique os logs e se a loja não está em manutenção";
                                } else {
                                    $json['idProduto'] = $prod['maximo'];
                                    $json['success'] = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        if (!empty($result->codigo)) {
            $this->cache->delete('codebling_product_' . $result->codigo);
        } else {
            $this->log->write(
                'ControllerFeedRestApi_products_insert() - Sem o Código do Produto no result:  '
                . print_r($result, true)
            );
        }

        if (!empty($json['idProduto'])) {
            $this->log->write(
                'ControllerFeedRestApi_products_insert() - Rodado com sucesso, Produto: '
                . $json['idProduto'] . ' Código: ' . $result->codigo
            );
        } else {
            $this->log->write(
                'ControllerFeedRestApi_products_insert() - Sem o ID do Produto - Falhou!, Json retornado: '
                . print_r($json, true)
            );
        }

        if ($this->debugIt) {
            echo '<pre>';
            print_r($json);
            echo '</pre>';
        } else {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    /**
     * Atualiza estoque
     * Bling -> Opencart
     */
    public function products_stock()
    {
        $this->checkPlugin();

        $parameters = urldecode($this->getParameter());
        $exp = explode("|", $parameters);
        $this->log->write(
            'ControllerFeedRestApi_products_stock() dados: ' . print_r($exp, true)
        );

        $tipo = $exp[0];
        $id = $exp[1];
        $qtd = $exp[2];

        if ($tipo == 'P') {
            $products
                = $this->model_module_code_bling_native_product->update_stock_product(
                $id,
                $qtd
            );
            $this->model_module_code_bling->updateStockProductParent('', $id);
        } else {
            $products
                = $this->model_module_code_bling_native_product->update_stock_variation(
                $id,
                $qtd
            );
            $this->model_module_code_bling->updateStockProductParent($id, 0);
        }

        $this->log->write(
            'ControllerFeedRestApi_products_stock() - Alterado os estoques com sucesso!'
        );

        $json['success'] = $products;

        if ($this->debugIt) {
            echo '<pre>';
            print_r($json);
            echo '</pre>';
        } else {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    /**
     * Atualiza o Preço dos Produtos
     * Mudança feita pelo Painel do Bling ERP
     * Bling -> Opencart
     */
    public function products_price()
    {
        $this->checkPlugin();
        $parameters = urldecode($this->getParameter());

        /*
         *
            [0] => V = Tipo
            [1] => 83v25 = ID Opencart
            [2] => 100.0000000000 = Preco Opencart ou Preço Promocional
            [3] => BLING-TESTE-OP1 = Código/SKU
         */
        $exp = explode("|", $parameters);

        $this->log->write(
            'ControllerFeedRestApi_products_price() dados: ' . print_r($exp, true)
        );
        /*$this->log->write(
            'ControllerFeedRestApi_products_price() - Parametros dados: '
            .print_r($parameters, true)
        );
        */

        $tipo = $exp[0];
        $id = $exp[1];
        $price = $exp[2];

        //Tipo P = Produto e V = Opção e no Bling Produto Variação
        if ($tipo == 'P') {
            $products
                = $this->model_module_code_bling_native_product->update_price_product(
                $id
            );
        } else {
            $products
                = $this->model_module_code_bling_native_product->update_price_variation(
                $id,
                $price
            );
        }

        $this->log->write(
            'ControllerFeedRestApi_products_price() - Alterado os preços com sucesso!'
        );

        $json['success'] = $products;

        if ($this->debugIt) {
            echo '<pre>';
            print_r($json);
            echo '</pre>';
        } else {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    /**
     * Retorna todos os Pedidos
     */
    public function orders()
    {
        $this->checkPlugin();
        $this->log->write('ControllerFeedRestApi_orders() - Rodando');

        /*check offset parameter*/
        if (isset($this->request->get['offset'])
            && $this->request->get['offset'] != ""
            && ctype_digit($this->request->get['offset'])
        ) {
            $offset = $this->request->get['offset'];
        } else {
            $offset = 0;
        }

        /*check limit parameter*/
        if (isset($this->request->get['limit'])
            && $this->request->get['limit'] != ""
            && ctype_digit($this->request->get['limit'])
        ) {
            $limit = $this->request->get['limit'];
        } else {
            $limit = 10000;
        }

        $filters = '||tds';

        /*get all orders of user*/
        $results
            = $this->model_module_code_bling_native_order->getAllOrdersFilters(
            $offset,
            $limit,
            $filters
        );

        $orders = [];
        if (count($results)) {
            $orders = $this->ParametersReturnOrder($results);
            $json['success'] = true;
            $json['orders'] = $orders;
        } else {
            $json['success'] = false;
            $json['error']
                = "Problemas para obter Pedidos. Não há Pedidos neste período.";
        }

        $this->log->write(
            'ControllerFeedRestApi_orders() - Rodado com sucesso'
        );

        if ($this->debugIt) {
            echo '<pre>';
            print_r($json);
            echo '</pre>';
        } else {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    /**
     * Retorna os Pedidos com filtro
     */
    public function orders_filters()
    {
        $this->checkPlugin();
        $this->log->write('ControllerFeedRestApi_orders_filters() - Rodando');

        $filters = $this->getParameter();
        //$this->log->write('ControllerFeedRestApi_orders_filters() '.print_r($filters,true));

        /*check offset parameter*/
        if (isset($this->request->get['offset'])
            && $this->request->get['offset'] != ""
            && ctype_digit($this->request->get['offset'])
        ) {
            $offset = $this->request->get['offset'];
        } else {
            $offset = 0;
        }

        /*check limit parameter*/
        if (isset($this->request->get['limit'])
            && $this->request->get['limit'] != ""
            && ctype_digit($this->request->get['limit'])
        ) {
            $limit = $this->request->get['limit'];
        } else {
            $limit = 10000;
        }

        /*get all orders of user*/
        $results
            = $this->model_module_code_bling_native_order->getAllOrdersFilters(
            $offset,
            $limit,
            $filters
        );

        $orders = [];
        if (count($results)) {
            $orders = $this->ParametersReturnOrder($results);
            $json['success'] = true;
            $json['orders'] = $orders;
        } else {
            $json['success'] = false;
            $json['error'] = "Problemas para obter Produtos.";
        }

        $this->log->write(
            'ControllerFeedRestApi_orders_filters() - Rodado com sucesso'
        );

        if ($this->debugIt) {
            echo '<pre>';
            print_r($json);
            echo '</pre>';
        } else {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    /**
     * Retorna o Pedido
     */
    public function order_id()
    {
        $this->checkPlugin();
        $parametro = $this->getParameter();
        //get values of the database
        $results = $this->model_module_code_bling_native_order->getOrderId(
            $parametro
        );

        $orders = [];
        if (count($results)) {
            $orders = $this->ParametersReturnOrder($results);
            $json['success'] = true;
            $json['orders'] = $orders;
        } else {
            $json['success'] = false;
        }

        if ($this->debugIt) {
            echo '<pre>';
            print_r($json);
            echo '</pre>';
        } else {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    /**
     * Adicionando Pedidos no Bling API
     * https://ajuda.bling.com.br/hc/pt-br/articles/360047064693-POST-pedido
     * /index.php?route=feed/rest_api/addOrderBling&key=&order_id=viewData=&disableCache=
     * order_id = IDs dos Pedidos
     * viewData = 1 Exibindo dados, modo de teste
     * disableCache = 1 vai desativar Cache
     */
    public function addOrderBling()
    {
        if (empty($this->request->get['order_id'])) {
            exit("<h1>Sem o ID do Pedido!</h1>");
        }

        $ids_origin = $this->request->get['order_id'];
        echo "<h1>Adicionando Pedidos " . $ids_origin . " ao Bling</h1>";
        $ids = explode(",", $ids_origin);

        $disableCache = !empty($this->request->get['disableCache'])
            ? 1 : 0;

        foreach ($ids as $id) {
            if (empty($disableCache)
                && !empty(
                $this->codeCache->get(
                    'codebling_addorder' . $id
                )
                )
            ) {
                echo "<h2>Cache - Pedido " . $id . " já adicionado ao Bling</h2>";
                continue;
            }

            $order = $this->model_module_code_bling_native_order->getOrderId(
                $id
            );

            if (empty($order[0])) {
                echo "<h2>Pedido " . $id . " não encontrado</h2>";

                $this->log->write(
                    'ControllerFeedRestApi_addOrderBling() - Pedido ' . $id
                    . ' não encontrado'
                );

                continue;
            }

            $order_parameter = $this->ParametersReturnOrder($order)[0];

            if (empty($order_parameter)) {
                echo "<h2>Pedido " . $id . " sem dados retornados</h2>";

                $this->log->write(
                    'ControllerFeedRestApi_addOrderBling() - Pedido ' . $id
                    . ' sem dados retornados'
                );

                continue;
            }

            echo "<h2>Adicionando Pedido " . $id . " ao Bling</h2>";
            $this->model_module_code_bling->addOrderBling(
                $order_parameter
            );
        }

        echo "<h1 style='color:#47d35d'>Pedidos " . $ids_origin
            . " adicionados ao Bling com sucesso</h1>";
    }

    /**
     * Adicionando Pedidos no Bling API percorrendo os pedidos recentes
     * https://ajuda.bling.com.br/hc/pt-br/articles/360047064693-POST-pedido
     * /index.php?route=feed/rest_api/addOrdersBling&key=&status=&limit=&hour=
     * viewData = 1 Exibindo dados, modo de teste
     */
    public function addOrdersBling()
    {
        if (empty($this->request->get['status'])) {
            exit("<h1>Sem Status!</h1>");
        }

        $start_time = microtime(true);
        $time_limit = !empty($this->request->get['time_limit'])
            ? (int) $this->request->get['time_limit'] : 80;

        $status = trim($this->request->get['status']);
        $status = explode(',', $status);
        $order_id_min = !empty($this->request->get['order_id_min'])
            ? (int) $this->request->get['order_id_min'] : 1;

        //codeHelperSemaphoreRelease(548);
        $timeout = (int) ($time_limit / 2.2);
        $semaphore_id = codeHelperSemaphore(548, 1, $timeout, 1, true);

        if (empty($semaphore_id)) {
            // Semáforo 548 em uso
            $this->log->write(
                "Chamada ID: " . $this->idRun
                . " addOrdersBling() - Código já sendo rodado por outra chamada - Semáforo Fechado!"
            );

            $this->log->write(
                'ControllerFeedRestApi_addOrdersBling() - Código já sendo rodado por outra chamada - Semáforo Fechado!'
            );

            echo "<h1>Código sendo rodado por outra chamada! " . date('d/m/Y H:i:s') . "</h1>";
        } else {
            if (!empty($this->codeCache->get("codebling_addorder_last"))
                && $this->codeCache->get("codebling_addorder_last")
                > $order_id_min
            ) {
                $order_id_min = (int) $this->codeCache->get(
                    "codebling_addorder_last"
                );
            }

            echo "<h1>Adicionando os Pedidos ao Bling ERP - Pedido Mínimo: "
                . $order_id_min . " " . date('i:s') . "</h1>";

            $order_id_max = !empty($this->request->get['order_id_max'])
                ? (int) $this->request->get['order_id_max'] : 10000000;
            $limit = !empty($this->request->get['limit'])
                ? (int) $this->request->get['limit'] : 30;

            if (empty($this->request->get['hour'])) {
                $hourIntervalLimit = 25;
            } else {
                $hourIntervalLimit = (int) $this->request->get['hour'];
            }

            $disableCache = !empty($this->request->get['disableCache'])
                ? 1 : 0;

            $price_min = isset($this->request->get['price_min'])
                ? (int) $this->request->get['price_min'] : 1;

            //Não verifica Status com ID 0 ou com Status ID do $this->config->get('config_fraud_status_id') normalmente Cancelado
            // Ordenando da data mais antiga para mais nova, pois assim garante que os dados dos clientes estão mais atualizados
            $sql = "
            SELECT order_id
            FROM `" . DB_PREFIX . "order`
            WHERE
            order_id >= '" . (int) $order_id_min . "' AND
            order_id <= '" . (int) $order_id_max . "' AND
            order_status_id IN ('" . implode("','", $status) . "') AND
            (NOW() - INTERVAL '" . (int) $hourIntervalLimit . "' HOUR) <= date_modified AND
            total >= '" . (int) $price_min . "'
            ORDER BY date_modified ASC
            LIMIT " . $limit . "
        ";

            //print_r($sql);
            $orders = $this->db->query($sql)->rows;

            //print_r($orders);
            $c = 0;

            foreach ($orders as $order) {
                $id = $order['order_id'];
                if (empty($disableCache)
                    && !empty(
                    $this->codeCache->get(
                        'codebling_addorder' . $id
                    )
                    )
                ) {
                    echo "<h2>Cache - Pedido " . $id
                        . " já adicionado ao Bling</h2>";
                    continue;
                }

                $orderGet
                    = $this->model_module_code_bling_native_order->getOrderId(
                    $id
                );

                if (empty($orderGet[0])) {
                    echo "<h2>Pedido " . $id . "não encontrado</h2>";

                    $this->log->write(
                        'ControllerFeedRestApi_addOrdersBling() - Pedido ' . $id
                        . ' não encontrado'
                    );

                    continue;
                }

                echo "<h2>Adicionando Pedido " . $id . " ao Bling</h2>";

                $order_parameter = $this->ParametersReturnOrder($orderGet)[0];

                if (empty($order_parameter)) {
                    echo "<h2>Pedido " . $id . " sem dados retornados</h2>";

                    $this->log->write(
                        'ControllerFeedRestApi_addOrdersBling() - Pedido ' . $id
                        . ' sem dados retornados'
                    );

                    continue;
                }

                $this->model_module_code_bling->addOrderBling(
                    $order_parameter
                );

                $c++;

                // Sleep de 800ms = 0,8s, pois o Bling limita até 3 chamadas por s
                usleep(800000);

                // Verificando tempo limite
                if ((microtime(true) - $start_time) >= ($time_limit - 2)) {
                    if ($order_parameter["order_id"] < 5103488) {
                        $this->codeCache->set(
                            "codebling_addorder_last",
                            $order_parameter["order_id"]
                        );
                    }

                    $this->log->write(
                        'ControllerFeedRestApi_addOrdersBling() - Pedido ' . $id
                        . ' Rodado, parando o código chegou no tempo limite'
                    );

                    echo "<h2 style='color:#ec0000'>Parando, chegou no tempo limite - Horário: " . date(
                            'i:s'
                        ) . "</h2>";

                    break;
                }
            }

            echo "<h1 style='color:#47d35d'>Total de Pedidos " . count($orders)
                . ", {$c} Pedidos adicionados ao Bling com sucesso </h1>";

            // Liberando o Semáforo
            sem_release($semaphore_id);
        }
    }

    /**
     * Adicionando um cliente pelo seu ID
     * https://ajuda.bling.com.br/hc/pt-br/articles/360046378614-POST-contato
     * /index.php?route=feed/rest_api/addCustomerBling&key=&customer_id=
     */
    public function addCustomerBling()
    {
        if (empty($this->request->get['customer_id'])) {
            exit("<h1 > Sem o ID do Pedido!</h1 > ");
        }

        $ids_origin = $this->request->get['customer_id'];
        echo "<h1 > Adicionando Clientes " . $ids_origin . " ao Bling </h1 > ";
        $ids = explode(",", $ids_origin);

        foreach ($ids as $id) {
            $order = $this->model_module_code_bling_native_order->getOrderId(
                $id
            );

            if (empty($order)) {
                echo " < h2>Pedido " . $id . "não encontrado </h2 > ";
                continue;
            }

            echo "<h2 > Adicionando Pedido " . $id . " ao Bling </h2 > ";
            $this->model_module_code_bling->addOrderBling(
                $this->ParametersReturnOrder($order)[0]
            );
        }

        echo "<h1 style='color:#47d35d'> Pedidos " . $ids_origin
            . " adicionados ao Bling com sucesso </h1 > ";
    }

    /**
     * Retorna os dados do Produto
     * Parameters of the functions
     */
    public function ParametersReturnProduct(array $params = null)
    {
        $products = $params;

        $produtos = [];
        foreach ($products as $product) {
            if (empty($product['product_id'])) {
                continue;
            }

            $variation
                = $this->model_module_code_bling_native_order->getVariation(
                $product
            );

            $newVariation = [];
            if (!empty($variation)) {
                foreach ($variation as $v) {
                    //HookForeachGetOrderProductVariation

                    if (empty($v['sku'])) {
                        continue;
                    }

                    $v['tipoVariacao'] = strip_tags(trim($v['tipoVariacao']));
                    $v['tipoVariacao'] = str_replace(
                        ['"', "″"],
                        "",
                        $v['tipoVariacao']
                    );
                    $newVariation[] = $v;
                }
            }

            $this->log->write(
                "ControllerFeedRestApi_ParametersReturnProduct() - Montado as Variações do Produto {$product['product_id']}"
            );

            //HookGetOrderProductVariation
            $codeProduct = $this->model_module_code_bling->getProduct(
                $product['product_id']
            );

            $this->log->write(
                "ControllerFeedRestApi_ParametersReturnProduct() - Consultado mais dados do Produto {$product['product_id']}"
            );

            if (empty($codeProduct['product_id'])) {
                $this->log->write(
                    "ControllerFeedRestApi_ParametersReturnProduct() - Sem dados do Produto {$product['product_id']}"
                );

                continue;
            }

            $productSKU = '';
            if (!empty($codeProduct['sku'])) {
                $productSKU = $codeProduct['sku'];
            }

            if (!empty($this->conf->code_produto_ncm)
                && !empty($codeProduct[$this->conf->code_produto_ncm])
            ) {
                $productNCM = preg_replace(
                    ' / [^0 - 9]/',
                    "",
                    trim($codeProduct[$this->conf->code_produto_ncm])
                );
            } else {
                $productNCM = '';
            }

            if (!empty($this->conf->code_produto_cest)
                && !empty($codeProduct[$this->conf->code_produto_cest])
            ) {
                $productCEST = preg_replace(
                    ' / [^0 - 9]/',
                    "",
                    trim($codeProduct[$this->conf->code_produto_cest])
                );
            } else {
                $productCEST = '';
            }

            if (!empty($codeProduct['special'])) {
                $productPricePromo = $codeProduct['special'];
            } else {
                $productPricePromo = '';
            }

            if (!empty($codeProduct['manufacturer'])) {
                $productBrand = $codeProduct['manufacturer'];
            } else {
                $productBrand = '';
            }

            if (empty($newVariation)) {
                $newVariation = null;
            }

            $name = trim($product['name']);
            $name = str_replace("&", 'E', $name);

            $produtos[] = [
                'order_product_id' => $product['order_product_id'],
                'product_id' => $product['product_id'],
                'name' => $name,
                'model' => trim($product['model']),
                'sku' => $productSKU,
                'ncm' => $productNCM,
                'cest' => $productCEST,
                'brand' => $productBrand,
                'pricePromo' => $productPricePromo,
                'quantity' => $product['quantity'],
                'price' => $product['price'],
                'total' => $product['total'],
                'tax' => $product['tax'],
                //'rewar' => $product['reward'],
                'variation' => $newVariation,
            ];
        }

        return $produtos;
    }

    public function ParametersReturnPayment(array $params = null)
    {
        // https://nfe.prefeitura.sp.gov.br/arquivos/nfe_layout_rps.pdf
        $result = $params;
        $payment = [
            'payment_name' => trim($result['payment_firstname']) . " " . trim(
                    $result['payment_lastname']
                ),
            'payment_company' => trim($result['payment_company']),
            'payment_address_1' => substr(
                trim($result['payment_address_1']),
                0,
                50
            ),
            'payment_address_2' => substr(
                trim($result['payment_address_2']),
                0,
                30
            ),
            'payment_city' => substr(trim($result['payment_city']), 0, 30),
            'payment_postcode' => preg_replace(
                "/[^0-9]/",
                "",
                $result['payment_postcode']
            ),
            'payment_country' => $result['payment_country'],
            'payment_zone' => ($result['payment_country'] == 'Brasil'
                || $result['payment_country'] == 'Brazil')
                ? $result['payment_zone'] : "EX",
            'payment_method' => $result['payment_method'],
            'payment_code' => $result['payment_code'],
        ];

        return $payment;
    }

    public function ParametersReturnShipping(array $params = null)
    {
        $result = $params;

        $shippingValue
            = $this->model_module_code_bling_native_order->getShippingByOrder(
            $result['order_id']
        );

        if (empty($shippingValue[0]['valueShipping'])
            || $shippingValue[0]['valueShipping'] == null
        ) {
            $shippingValue[0]['valueShipping'] = 0;
        }

        $shipping = [
            'shipping_name' => trim($result['shipping_firstname']) . " " . trim(
                    $result['shipping_lastname']
                ),
            'shipping_company' => trim($result['shipping_company']),
            'shipping_address_1' => substr(
                trim($result['shipping_address_1']),
                0,
                50
            ),
            'shipping_address_2' => substr(
                trim($result['shipping_address_2']),
                0,
                30
            ),
            'shipping_city' => substr(trim($result['shipping_city']), 0, 30),
            'shipping_postcode' => preg_replace(
                "/[^0-9]/",
                "",
                $result['shipping_postcode']
            ),
            'shipping_country' => $result['shipping_country'],
            'shipping_zone' => ($result['shipping_country'] == 'Brasil'
                || $result['shipping_country'] == 'Brazil')
                ? $result['shipping_zone'] : "EX",
            'shipping_method' => $result['shipping_method'],
            'shipping_code' => $result['shipping_code'],
            'shipping_price' => $shippingValue[0]['valueShipping'],
        ];

        return $shipping;
    }

    /**
     * Retorna os Pedidos para o Bling
     * Opencart -> Bling
     *
     * @param array|null $params
     *
     * @return array
     */
    public function ParametersReturnOrder(array $params = null)
    {
        $results = $params;
        foreach ($results as $result) {
            $product_total
                = $this->model_module_code_bling_native_order->getTotalOrderProductsByOrderId(
                $result['order_id']
            );
            $voucher_total
                = $this->model_module_code_bling_native_order->getTotalOrderVouchersByOrderId(
                $result['order_id']
            );
            $products
                = $this->model_module_code_bling_native_order->getProductOrderId(
                $result['order_id']
            );

            /*
            Descontinuado 09/04/2021 21:27
            Usado novo método para

            //shipping price and/or discount coupon
            if (empty($couponDiscount[0]['valueCoupon']) || $couponDiscount[0]['valueCoupon'] == null) {
                $couponDiscount[0]['valueCoupon'] = 0;
            }
            */

            $productsOrder = $this->ParametersReturnProduct($products);

            if (empty($productsOrder)) {
                $this->log->write(
                    "ControllerFeedRestApi_ParametersReturnOrder() - Sem Produtos retornados no Pedido {$result['order_id']}"
                );

                continue;
            }

            $payment = $this->ParametersReturnPayment($result);
            $shipping = $this->ParametersReturnShipping($result);

            //DADOS EXTRAS
            $codeOrder = $this->db->query(
                "SELECT * FROM `" . DB_PREFIX . "order`
                WHERE
                order_id = '" . (int) $result['order_id'] . "'
                LIMIT 1
            "
            )->row;

            if (empty($codeOrder['order_id']) || empty($payment)) {
                $this->log->write(
                    "ControllerFeedRestApi_ParametersReturnOrder() - Sem dados do Pedido ou Pagamento no Pedido {$result['order_id']}"
                );

                continue;
            }

            /*
            if (version_compare(VERSION, '2.0.3.1', '>')) {
                $customField = json_decode($codeOrder['custom_field'], true);
                $paymentCustomField = json_decode(
                    $codeOrder['payment_custom_field'],
                    true
                );
                $shippingCustomField = json_decode(
                    $codeOrder['shipping_custom_field'],
                    true
                );
            } else {
                // Opencart 2.0.3.1 ou mais antigo
                $customField = unserialize($codeOrder['custom_field']);
                $paymentCustomField = unserialize(
                    $codeOrder['payment_custom_field']
                );
                $shippingCustomField = unserialize(
                    $codeOrder['shipping_custom_field']
                );
            }
            */

            // Versão maior que 2.0.3.1 27/04/2022 12:42
            $customField = json_decode($codeOrder['custom_field'], true);
            $paymentCustomField = json_decode(
                $codeOrder['payment_custom_field'],
                true
            );
            $shippingCustomField = json_decode(
                $codeOrder['shipping_custom_field'],
                true
            );

            // IE
            if (!empty($this->conf->code_extra_ie)
                && !empty($customField[$this->conf->code_extra_ie])
            ) {
                $codeIE = $customField[$this->conf->code_extra_ie];
            } else {
                if (!empty($this->conf->code_extra_ie)
                    && !empty($paymentCustomField[$this->conf->code_extra_ie])
                ) {
                    $codeIE = $paymentCustomField[$this->conf->code_extra_ie];
                }
            }

            // RG
            if (!empty($this->conf->code_extra_rg)
                && !empty($customField[$this->conf->code_extra_rg])
            ) {
                $codeRG = $customField[$this->conf->code_extra_rg];
            } else {
                if (!empty($this->conf->code_extra_rg)
                    && !empty($paymentCustomField[$this->conf->code_extra_rg])
                ) {
                    $codeRG = $paymentCustomField[$this->conf->code_extra_rg];
                }
            }

            $codeCPF = '';
            if (!empty($this->conf->code_extra_cpf2)
                && !empty($customField[$this->conf->code_extra_cpf2])
            ) {
                $codeCPF = $customField[$this->conf->code_extra_cpf2];
            } else {
                if (!empty($this->conf->code_extra_cpf2)
                    && !empty($paymentCustomField[$this->conf->code_extra_cpf2])
                ) {
                    $codeCPF
                        = $paymentCustomField[$this->conf->code_extra_cpf2];
                }
            }

            if (!empty($this->conf->code_extra_cpf)
                && !empty($customField[$this->conf->code_extra_cpf])
            ) {
                $codeCPF = $customField[$this->conf->code_extra_cpf];
            } else {
                if (!empty($this->conf->code_extra_cpf)
                    && !empty($paymentCustomField[$this->conf->code_extra_cpf])
                ) {
                    $codeCPF = $paymentCustomField[$this->conf->code_extra_cpf];
                }
            }

            $codePerson = 'F';
            if (!empty($codeCPF)) {
                $codeCPF = preg_replace("/[^0-9]/", '', $codeCPF);
                $codeCPFL = strlen($codeCPF);

                if ($codeCPFL > 11) {
                    $codePerson = 'J';
                }
            }

            $codePaymentComplement = '';
            if (!empty($this->conf->code_extra_complemento)
                && !empty($paymentCustomField[$this->conf->code_extra_complemento])
            ) {
                $codePaymentComplement = trim(
                    $paymentCustomField[$this->conf->code_extra_complemento]
                );
            }

            $codePaymentNumber = '';
            if (!empty($this->conf->code_extra_numero)
                && !empty($paymentCustomField[$this->conf->code_extra_numero])
            ) {
                $codePaymentNumber = trim(
                    $paymentCustomField[$this->conf->code_extra_numero]
                );
            }

            $codePaymentTel = '';
            if (!empty($this->conf->code_extra_celular)
                && !empty($paymentCustomField[$this->conf->code_extra_celular])
            ) {
                $codePaymentTel = trim(
                    $paymentCustomField[$this->conf->code_extra_celular]
                );
            } else {
                if (!empty($this->conf->code_extra_celular)
                    && !empty($customField[$this->conf->code_extra_celular])
                ) {
                    $codePaymentTel = trim(
                        $customField[$this->conf->code_extra_celular]
                    );
                }
            }

            $codeShippingComplement = '';
            if (!empty($this->conf->code_extra_complemento)
                && !empty($shippingCustomField[$this->conf->code_extra_complemento])
            ) {
                $codeShippingComplement = trim(
                    $shippingCustomField[$this->conf->code_extra_complemento]
                );
            }

            $codeShippingNumber = '';
            if (!empty($this->conf->code_extra_numero)
                && !empty($shippingCustomField[$this->conf->code_extra_numero])
            ) {
                $codeShippingNumber = trim(
                    $shippingCustomField[$this->conf->code_extra_numero]
                );
            }

            $payment['payment_number_address'] = $codePaymentNumber;
            $payment['payment_address_2'] = $codePaymentComplement;
            $payment['payment_neighborhood'] = trim(
                $codeOrder['payment_address_2']
            );
            $payment['payment_cellphone'] = $codePaymentTel;

            if (!empty($shipping)) {
                $shipping['shipping_address_2'] = $codeShippingComplement;
                $shipping['shipping_number'] = $codeShippingNumber;
                $shipping['shipping_neighborhood'] = trim(
                    $codeOrder['shipping_address_2']
                );
            }

            //Desconto, o Bling não está usando o Total por isso tem que ser passado o Desconto
            $codeOrderDiscount = $this->db->query(
                "SELECT * FROM `" . DB_PREFIX . "order_total`
                WHERE
                order_id = '" . (int) $result['order_id'] . "' AND
                value < 0
            "
            )->rows;

            $discount = 0;
            if (isset($codeOrderDiscount[0]['value'])) {
                $discount = 0;
                foreach ($codeOrderDiscount as $od) {
                    if ($od['value']) {
                        $discount = $discount + $od['value'];
                    }
                }
            }

            $name = trim($result['firstname']) . ' ' . trim($result['lastname']);
            $name = str_replace("&", 'E', $name);

            $orders[] = [
                'order_id' => $result['order_id'],
                'name' => $name,
                'customer_id' => $result['customer_id'],
                'email' => trim($result['email']),
                'telephone' => $result['telephone'],
                'status' => $result['status'],
                'date_added' => $result['date_added'],
                'products_totals' => ($product_total + $voucher_total),
                'products' => $productsOrder,
                'payment' => $payment,
                'shipping' => !empty($shipping['shipping_postcode']) ? $shipping
                    : $payment,
                'discount' => $discount,
                'total' => $result['total'],
                'comment' => trim($result['comment']),
                'currency_code' => $result['currency_code'],
                'currency_value' => $result['currency_value'],
                'cpf_cnpj' => $codeCPF,
                'persontype' => $codePerson,
                'ie' => $codeIE,
                'rg' => $codeRG,
            ];
        }

        $this->log->write(
            "ControllerFeedRestApi_ParametersReturnOrder() - Retornando os dados do Pedido {$result['order_id']}"
        );

        return $orders;
    }

    /**
     * Converte o peso para KG
     *
     * @param $weight
     *
     * @return mixed
     */
    private function convertWeight($weight)
    {
        $weight_class_id = !empty($this->conf->code_weight_kg)
            ? (int) $this->conf->code_weight_kg : 1;

        return $this->weight->convert(
            $weight,
            $this->config->get('config_weight_class_id'),
            $weight_class_id
        );
    }

    /**
     * Converte as Dimensões para CM
     *
     * @param $length
     *
     * @return mixed
     */
    private function convertLength($length)
    {
        $length_class_id = !empty($this->conf->code_length_cm)
            ? (int) $this->conf->code_length_cm : 1;

        return $this->length->convert(
            $length,
            $this->config->get('config_length_class_id'),
            $length_class_id
        );
    }

    /**
     * Retorna os parâmetros
     * get GET parameters
     */
    private function getParameter()
    {
        if (isset($this->request->get['parametro'])
            && !empty($this->request->get['parametro'])
        ) {
            $parametro = $this->request->get['parametro'];

            if ($this->debugLog) {
                $this->log->write(
                    'ControllerFeedRestApi_getParameter() - Parâmetros URL: '
                    . print_r($parametro, true)
                );
            }
        } else {
            $parametro = null;
        }

        return $parametro;
    }

    /**
     * Verifica a Chave
     */
    private function checkPlugin()
    {
        $json = ["success" => true];

        /*validate api security key*/
        if (!empty($this->request->get['key']) && $this->request->get['key'] != $this->secretKey) {
            $json["error"] = 'Chave inválida';
        }

        if (isset($json["error"])) {
            $this->response->addHeader('Content - Type: application / json');

            if ($this->debugLog) {
                $this->log->write(
                    'ControllerFeedRestApi_checkPlugin() - Erro: ' . print_r(
                        $json,
                        true
                    )
                );
            }

            echo(json_encode($json));
            exit;
        } else {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }
}