<?php
class ControllerExtensionPaymentFreeProject extends Controller {
	public function index() {
		// $data['continue'] = $this->url->link('checkout/success');

		return $this->load->view('extension/payment/free_project');
	}

	public function confirm() {

		$json = array();
		
		if ($this->session->data['payment_method']['code'] == 'free_project') {
			$this->load->model('checkout/order');

			$comment = "Pagamento gratuito - projeto social.";

			$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('payment_free_project_order_status_id'), $comment, true);
			// $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('payment_bank_transfer_order_status_id'), $comment, true);
			$json['redirect'] = $this->url->link('checkout/success');
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));		
	}
}
