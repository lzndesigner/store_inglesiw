<?php

require_once DIR_SYSTEM . '/library/code_vindi/vendor/autoload.php';

use Vindi\Bill;
use Vindi\Exceptions\ValidationException;

/**
 * Class ControllerExtensionPaymentCodeMP
 *
 * @author Felipo Antonoff Araújo e Victor Henrique Ramos
 * @version 1.0
 * @package code_vindi
 */
class ControllerExtensionPaymentCodeVindiBoleto extends Controller
{
    /**
     * @var \stdClass
     */
    private $conf;
    private $log;

    /**
     * Definicoes iniciais de boot
     *
     * @param $registry
     *
     * @throws \Exception
     */
    public function __construct($registry)
    {
        parent::__construct($registry);

        $this->load->model('extension/payment/code_vindi');
        $this->load->model('module/codemarket_module');
        $this->conf = $this->model_module_codemarket_module->getModulo('570');

        if ($this->conf->code_env === 'sandbox') {
            putenv('VINDI_API_KEY=' . $this->conf->code_sandbox_private);
            putenv('VINDI_API_URI=https://sandbox-app.vindi.com.br/api/v1/');
        } else {
            putenv('VINDI_API_KEY=' . $this->conf->code_production_private);
            putenv('VINDI_API_URI=https://app.vindi.com.br/api/v1/');
        }

        $this->log = new Log('Code-Vindi-Boleto.log');

        //TESTE APENAS
        //$this->conf->code_custom_cpf = 1;
        //$this->conf->code_custom_number = 2;
    }

    /**
     * Carregamento da view
     *
     * @return string
     * @throws \Exception
     */
    public function index(): string
    {
        $this->load->model('checkout/order');
        $data['order'] = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        if (!empty($data['order']['custom_field'][$this->conf->code_custom_cpf])) {
            $data['cpf'] = $data['order']['custom_field'][$this->conf->code_custom_cpf];
        } else {
            $data['cpf'] = '';
        }

        if ($this->conf->code_env === 'sandbox') {
            $data['url_token'] = 'https://sandbox-app.vindi.com.br/api/v1/public/payment_profiles';
            $data['public_key'] = $this->conf->code_sandbox_public;
        } else {
            $data['url_token'] = 'https://app.vindi.com.br/api/v1/public/payment_profiles';
            $data['public_key'] = $this->conf->code_production_public;
        }

        $data['url_confirm'] = $this->url->link('extension/payment/code_vindi_boleto/confirm', '', true);

        //PERSONALIZAÇÕES
        $data['code_botao_boleto'] = $this->conf->code_botao_boleto;
        $data['code_mensagem_boleto'] = $this->conf->code_mensagem_boleto;

        return $this->load->view('extension/payment/code_vindi_boleto', $data);
    }

    /**
     * Retira o pedido do status de abandonado e define nova situacao
     *
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function confirm()
    {
        $this->response->addHeader('Content-Type: application/json');

        $this->load->model('checkout/order');
        $order = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        $cid = $this->model_extension_payment_code_vindi->addCliente($order);

        if (empty($cid)) {
            $this->log->write('Confirm() - Falha na criação do cliente');
            return $this->model_extension_payment_code_vindi->returnFalse();
        }

        $pv = $this->model_extension_payment_code_vindi->addProduto($order);

        if (empty($pv->id)) {
            $this->log->write('Confirm() - Falha na criação do produto');
            return $this->model_extension_payment_code_vindi->returnFalse();
        }

        //Verificando se tem Produto com Assinatura
        if (!empty($this->cart->getRecurringProducts())) {
            $assinar = $this->model_extension_payment_code_vindi->addAssinatura($cid, $pv->id, $order, ['payment_method_code' => 'bank_slip', 'token' => '']);

            if (empty($assinar)) {
                $this->log->write('Confirm() - Falha na criação da assinatura');
                return $this->model_extension_payment_code_vindi->returnFalse();
            }
        } else {
            $transacao = $this->transacao($cid, $pv->id, $order);

            if (empty($transacao)) {
                $this->log->write('Confirm() - Falha na criação da transação');
                return $this->model_extension_payment_code_vindi->returnFalse();
            }
        }

        return $this->response->setOutput(json_encode([
            'success'  => true,
            'redirect' => $this->url->link('checkout/success'),
        ]));
    }

    private function transacao($customer_id, $product_id, $order)
    {
        try {
            $billService = new Bill();
            $bill = $billService->create([
                'customer_id'         => $customer_id,
                'code'                => $order['order_id'],
                'payment_method_code' => 'bank_slip',
                'bill_items'          => [
                    [
                        'product_id' => $product_id,
                        'amount'     => $order['total'],
                    ],
                ],
                'metadata'            => $order['order_id'],
            ]);

            $this->log->write('Pagamento, dados: ' . print_r($bill, true));
            $updateOrder = $this->model_extension_payment_code_vindi->updateOrder($bill, $order['order_id']);
            return $updateOrder;
        } catch (ValidationException $v) {
            //d($v);
            $this->log->write('Pagamento falhou: ' . print_r($v, true));
            return false;
        }
    }

    public function webhook()
    {
        $webhook = $this->model_extension_payment_code_vindi->webhook();

        if ($webhook) {
            return $this->response->output('OK');
        }
    }
}
