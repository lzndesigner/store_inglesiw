<?php

//CORS HEADERS
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: X-Requested-With, Content-Type, Origin, Cache-Control, Pragma, Authorization, Accept, Accept-Encoding");
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

/**
 * © Copyright 2014-2021 Codemarket - Todos os direitos reservados.
 * Class ControllerModuleCodeCodigo
 */
class ControllerModuleCodeCodigo extends Controller
{
    private $post;
    private $log;
    private $conf;

    /**
     * ControllerModuleCodeCodigo constructor.
     *
     * @param $registry
     */
    public function __construct($registry)
    {
        parent::__construct($registry);

        $this->load->model('module/codemarket_module');
        $conf = $this->model_module_codemarket_module->getModulo('604');
        $this->log = new log('Code-Codigo-Unidades.log');

        if (
            empty($conf) || empty($conf->code_status) || $conf->code_status == 2
        ) {
            $this->log->write('construct() - Desativado, verifique a configuração');
            exit();
        }

        if (!empty($this->request->get['route']) && $this->request->get['route'] != 'module/code_codigo/validate' && (
                empty($conf->code_token_url) || $conf->code_token_url != $this->request->get['token'])
        ) {
            $this->log->write('construct() - Token inválido, verifique a configuração');
            exit();
        }

        $this->conf = $conf;
        $this->post = json_decode(file_get_contents('php://input'));
    }

    /**
     * Valida se o código for existente e não esteja sendo utilizado
     */
    public function validate()
    {
        if ($this->request->server['REQUEST_METHOD'] != 'POST') {
            $this->log->write('validate() - Sem Post');
            exit();
        }

        $this->log->write("validate() - Dentro");
        //$this->log->write('Post Dados: ' . print_r($this->request->post, true));

        if (!empty($this->session->data['code_codigo'])) {
            unset($this->session->data['code_codigo']);
        }

        if (!empty($this->request->post['code_codigo'])) {
            $dataCodigo = $this->request->post['code_codigo'];
            $codigo = $this->db->query("SELECT * FROM " . DB_PREFIX . "code_codigos_codigos WHERE codigo = '{$dataCodigo}' AND customer_id is null")->row;

            if (!empty($codigo['codigo_id']) && empty($codigo['customer_id'])) {
                $this->session->data['code_codigo'] = $codigo['codigo_id'];

                $this->log->write("validate() - Validado com sucesso, código: " . $dataCodigo);

                $this->response->addHeader('Content-type: application/json');
                return $this->response->setOutput(json_encode(['success' => true]));
            } else {
                $this->log->write("validate() - Não encontrou o código ou já tem cliente usando, código: " . $dataCodigo);
            }
        }

        $this->response->addHeader('Content-type: application/json');
        return $this->response->setOutput(json_encode(['success' => false, 'error' => 'Informe um Código não usado e válido!']));
    }

    /**
     * Lista unidades
     */
    public function unidades()
    {
        $unidades = $this->db->query('SELECT * FROM ' . DB_PREFIX . 'code_codigos_unidades')->rows;

        foreach ($unidades as $k => $unidade) {
            $unidades[$k]['date_created'] = date('d/m/Y H:i:s', strtotime($unidade['date_created']));
            $unidades[$k]['date_modified'] = date('d/m/Y H:i:s', strtotime($unidade['date_modified']));
            $unidades[$k]['visible'] = false;
        }

        $this->response->addHeader('Content-type: application/json');
        $this->response->setOutput(json_encode($unidades));
    }

    /**
     * Salva as unidades
     */
    public function saveUnidade()
    {
        if ($this->request->server['REQUEST_METHOD'] != 'POST') {
            $this->log->write('saveUnidade() - Sem Post');
            exit();
        }

        if (!empty($this->post->unidade_id)) {
            $this->db->query("UPDATE " . DB_PREFIX . "code_codigos_unidades SET nome = '{$this->post->nome}', prefixo = '{$this->post->prefixo}', date_modified = NOW() WHERE unidade_id = {$this->post->unidade_id}");
        } else {
            $this->log->write('saveUnidade() - Salvando a unidade');

            $this->db->query("INSERT INTO " . DB_PREFIX . "code_codigos_unidades SET nome = '{$this->post->nome}', prefixo = '{$this->post->prefixo}', date_created = NOW(), date_modified = NOW()");
            $id = $this->db->getLastId();

            if ($this->post->qtd > 0) {
                $this->gerarCodigos($id, $this->post->prefixo, $this->post->qtd);
            }
        }

        $this->response->addHeader('Content-type: application/json');
        $this->response->setOutput(json_encode([
            'success' => true,
        ]));;
    }

    /**
     * Gera codigos por post
     */
    public function gerar()
    {
        if ($this->request->server['REQUEST_METHOD'] != 'POST') {
            $this->log->write('gerar() - Sem Post');
            exit();
        }

        if ($this->post->qtd > 0) {
            $unidade = $this->db->query('SELECT * FROM ' . DB_PREFIX . 'code_codigos_unidades WHERE unidade_id = ' . $this->post->unidade)->row;

            if (!empty($unidade)) {
                $this->gerarCodigos($this->post->unidade, $unidade['prefixo'], $this->post->qtd);
            }
        }
    }

    /**
     * Lista os codigos sem filtros
     *
     * @throws Exception
     */
    public function codigos()
    {
        $codigos = $this->db->query('SELECT * FROM ' . DB_PREFIX . 'code_codigos_codigos order by date_created DESC')->rows;

        $output = [];

        $this->load->model('account/customer');

        foreach ($codigos as $i => $codigo) {
            $unidade = $this->db->query('SELECT * FROM ' . DB_PREFIX . 'code_codigos_unidades WHERE unidade_id = ' . $codigo['unidade_id'])->row;

            $output[$i] = $codigo;
            $output[$i]['customer_name'] = null;
            $output[$i]['customer_email'] = null;
            $output[$i]['unidade'] = $unidade['nome'];
            $output[$i]['date_created'] = date('d/m/Y H:i:s', strtotime($codigo['date_created']));
            $output[$i]['date_modified'] = date('d/m/Y H:i:s', strtotime($codigo['date_modified']));

            if (!empty($codigo['customer_id'])) {
                $customer = $this->model_account_customer->getCustomer($codigo['customer_id']);
                $output[$i]['customer_name'] = $customer['firstname'] . ' ' . $customer['lastname'];
                $output[$i]['customer_email'] = $customer['email'];
            }
        }

        $this->response->addHeader('Content-type: application/json');
        $this->response->setOutput(json_encode($output));
    }

    /**
     * Lista os codigos utilizando filtros
     *
     * @throws Exception
     */
    public function codigosfilter()
    {
        $query = 'SELECT * FROM ' . DB_PREFIX . 'code_codigos_codigos';
        if (!empty($this->post->unidades)) {
            $query .= ' WHERE unidade_id in (' . implode(',', $this->post->unidades) . ')';
        } else {
            $query .= ' WHERE unidade_id > 0';
        }

        if (!empty($this->post->status) && count($this->post->status) < 2) {
            if (!empty($this->post->status) && in_array("1", $this->post->status)) {
                $query .= ' AND customer_id > 0';
            }

            if (!empty($this->post->status) && in_array("0", $this->post->status)) {
                $query .= ' AND customer_id is null';
            }
        }

        $query .= ' order by date_created DESC';

        $codigos = $this->db->query($query)->rows;

        $output = [];

        $this->load->model('account/customer');

        foreach ($codigos as $i => $codigo) {
            $unidade = $this->db->query('SELECT * FROM ' . DB_PREFIX . 'code_codigos_unidades WHERE unidade_id = ' . $codigo['unidade_id'])->row;

            $output[$i] = $codigo;
            $output[$i]['customer_name'] = null;
            $output[$i]['customer_email'] = null;
            $output[$i]['unidade'] = $unidade['nome'];
            $output[$i]['date_created'] = date('d/m/Y H:i:s', strtotime($codigo['date_created']));
            $output[$i]['date_modified'] = date('d/m/Y H:i:s', strtotime($codigo['date_modified']));

            if (!empty($codigo['customer_id'])) {
                $customer = $this->model_account_customer->getCustomer($codigo['customer_id']);
                $output[$i]['customer_name'] = $customer['firstname'] . ' ' . $customer['lastname'];
                $output[$i]['customer_email'] = $customer['email'];
            }
        }

        $this->response->addHeader('Content-type: application/json');
        $this->response->setOutput(json_encode($output));
    }

    /**
     * Metodo pra gerar os codigos recursivamente validando existencia
     *
     * @param $unidade
     * @param $prefixo
     * @param $qtd
     */
    private function gerarCodigos($unidade, $prefixo, $qtd)
    {
        $generate = function () use (&$generate, $unidade, $prefixo) {
            $code = $prefixo . mt_rand(100000000, 999999999);

            $exist = $this->db->query("SELECT * FROM " . DB_PREFIX . "code_codigos_codigos WHERE codigo = '" . $code . "' ")->rows;

            if (count($exist) > 0) {
                return $generate();
            }

            return $this->db->query("INSERT INTO " . DB_PREFIX . "code_codigos_codigos SET codigo = '{$code}', unidade_id = {$unidade}, date_created = NOW(), date_modified = NOW()");
        };

        for ($i = 0; $i < $qtd; $i++) {
            $generate();
        }
    }

    public function csv()
    {
        $query = 'SELECT * FROM ' . DB_PREFIX . 'code_codigos_codigos';
        if (!empty($this->post->unidades)) {
            $query .= ' WHERE unidade_id in (' . implode(',', $this->post->unidades) . ')';
        } else {
            $query .= ' WHERE unidade_id > 0';
        }

        if (!empty($this->post->status) && count($this->post->status) < 2) {
            if (!empty($this->post->status) && in_array("1", $this->post->status)) {
                $query .= ' AND customer_id > 0';
            }

            if (!empty($this->post->status) && in_array("0", $this->post->status)) {
                $query .= ' AND customer_id is null';
            }
        }

        $query .= ' order by date_created DESC';

        $codigos = $this->db->query($query)->rows;

        $output = [];

        $this->load->model('account/customer');

        foreach ($codigos as $i => $codigo) {
            $unidade = $this->db->query('SELECT * FROM ' . DB_PREFIX . 'code_codigos_unidades WHERE unidade_id = ' . $codigo['unidade_id'])->row;

            $output[$i] = $codigo;
            $output[$i]['customer_name'] = null;
            $output[$i]['customer_email'] = null;
            $output[$i]['unidade'] = $unidade['nome'];
            $output[$i]['date_created'] = date('d/m/Y H:i:s', strtotime($codigo['date_created']));
            $output[$i]['date_modified'] = date('d/m/Y H:i:s', strtotime($codigo['date_modified']));

            if (!empty($codigo['customer_id'])) {
                $customer = $this->model_account_customer->getCustomer($codigo['customer_id']);
                $output[$i]['customer_name'] = $customer['firstname'] . ' ' . $customer['lastname'];
                $output[$i]['customer_email'] = $customer['email'];
            }
        }

        $lines = implode(',', array_keys(reset($output))) . "\n";

        foreach ($output as $entry) {
            foreach ($entry as $key => $value) {
                $lines .= $value . ',';
            }
            $lines = substr($lines, 0, -1) . "\n";
        }

        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename=codigos.csv');
        $this->response->setOutput($lines);
    }
}

