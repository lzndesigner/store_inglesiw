<?php
// Heading
$_['heading_title'] = 'Thank you!!!';

// Text
$_['text_basket']   = 'Carrinho de compras';
$_['text_checkout'] = 'Finalizar pedido';
$_['text_success']  = 'Confirmação';
$_['text_customer'] = '<p>Agora é com a gente!</p><p>Nós já recebemos o seu pedido e vamos deixar tudo pronto pra você!.</p><p>Você também pode acompanhar o andamento do seu pedido através da sua <a href="%s"><b>conta</b></a>, acessando o menu <a href="%s"><b>histórico de pedidos</b></a>.</p><p>Se você precisar de alguma coisa, estaremos aqui pra te ajudar.</p>';
$_['text_guest']    = '<p>O seu pedido foi cadastrado.</p><p>Entre em contato com nosso <a href="%s">atendimento</a> caso tenha dúvidas.</p><p>Obrigado por comprar em nossa loja.</p>';