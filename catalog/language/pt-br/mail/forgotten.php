<?php
// Text
$_['text_hello']          = 'Olá aluno iW!';
$_['text_subject']  = '%s - Solicitação de nova senha';
$_['text_greeting'] = 'Uma nova senha foi solicitada através da nossa loja online.';
$_['text_change']   = 'Para redefinir sua senha, clique no link abaixo:';
$_['text_ip']       = 'O IP utilizado para solicitar a nova senha foi:';
$_['text_complement']     = 'Se você precisar de alguma coisa, estaremos aqui pra te ajudar.';
$_['text_thanks']         = 'Best regards,';