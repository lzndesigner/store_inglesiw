<?php
// Heading
$_['heading_title'] = 'Thank You!!!';

// Text
$_['text_message']  = '<p>Olá aluno iW!<br>Muito obrigado por se cadastrar!</p><p>Nós já te enviamos um e-mail de confirmação com as suas informações.</p><p>Utilizando seu e-mail e senha você poderá acessar nossa loja e visualizar o histórico de seus pedidos, <br> imprimir faturas e modificar as informações de sua conta.</p> <p>Se você precisar de alguma coisa, estaremos aqui pra te ajudar</p><p>Best regards, <br> Inglês iW</p>';
$_['text_approval'] = '<p>É com grande satisfação que lhe agradecemos por se cadastrar na loja <b>%s</b>.</p> <p>Assim que sua conta for aprovada, você receberá um e-mail de aprovação.</p> <p>Se você não receber o e-mail de aprovação dentro de 24 horas, entre em <a href="%s">contato</a> conosco.</p>';
$_['text_account']  = 'Minha conta';
$_['text_success']  = 'Confirmação';

$_['button_continue']       = 'Prosseguir <i class="fa fa-angle-right"></i>';