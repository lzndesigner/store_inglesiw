<?php

class ModelModuleCodeActivemodule extends Model
{
    public function index()
    {

                $this->load->model('module/codemarket_module');
                $conf = $this->model_module_codemarket_module->getModulo('604');
                $log = new log('Code-Codigo-Unidades.log');

                if (empty($conf) || empty($conf->code_status) || $conf->code_status == 2 || empty($conf->code_token_url)) {
                    $log->write('active() - Melhoria desativada');
                   return false;
                }else{
                    $log->write('active() - Criando/verificando tabelas');

                    $this->db->query("
                        CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "code_codigos_unidades`  (
                          `unidade_id` int(11) NOT NULL AUTO_INCREMENT,
                          `nome` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                          `prefixo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                          `date_created` datetime(0) NULL DEFAULT NULL,
                          `date_modified` datetime(0) NULL DEFAULT NULL,
                          PRIMARY KEY (`unidade_id`)
                        ) ENGINE=InnoDB  CHARSET=utf8 COLLATE=utf8_general_ci;
                    ");

                     $this->db->query("
                        CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "code_codigos_codigos`  (
                            `codigo_id` int(11) NOT NULL AUTO_INCREMENT,
                            `codigo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                            `customer_id` int(11) NULL DEFAULT NULL,
                            `unidade_id` int(11) NULL DEFAULT NULL,
                            `date_created` datetime(0) NULL DEFAULT NULL,
                            `date_modified` datetime(0) NULL DEFAULT NULL,
                            PRIMARY KEY (`codigo_id`) 
                        ) ENGINE=InnoDB  CHARSET=utf8 COLLATE=utf8_general_ci;
                    ");
                }
            
        //eventActiveModule

                $this->load->model('module/codemarket_module');
                $c570 = $this->model_module_codemarket_module->getModulo('570');

                $c570_status = 0;
                if(!empty($c570->code_status_cartao) && $c570->code_status_cartao == 1){
                    $c570_status = 1;
                }

                $this->model_module_codemarket_module->addExtensionStore('payment', 'code_vindi');
                $this->model_module_codemarket_module->editSettingStore('payment', 'code_vindi', 'code_vindi_status', $c570_status);

                $c570_status = 0;
                if(!empty($c570->code_status_boleto) && $c570->code_status_boleto == 1){
                    $c570_status = 1;
                }

                $this->model_module_codemarket_module->addExtensionStore('payment', 'code_vindi_boleto');
                $this->model_module_codemarket_module->editSettingStore('payment', 'code_vindi_boleto', 'code_vindi_boleto_status', $c570_status);

                //Dados extras
                $query570 = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "order` LIKE 'vindi_order_id'");
                if (!$query570->num_rows) {
                    $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD (`vindi_order_id` int )");
                    $this->db->query("ALTER TABLE `" . DB_PREFIX . "customer` ADD (`vindi_customer_id` int )");
                }

                $query570 = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "recurring` LIKE 'vindi_plano_id'");
                if (!$query570->num_rows) {
                    $this->db->query("ALTER TABLE `" . DB_PREFIX . "recurring` ADD (`vindi_plano_id` int )");
                    $this->db->query("ALTER TABLE `" . DB_PREFIX . "order_recurring` ADD (`vindi_assinatura_id` int )");
                }
            

                $query = $this->db->query("SHOW COLUMNS FROM `".DB_PREFIX."product_option_value` LIKE 'code_sku'");
                if(!$query->num_rows){
                    $this->db->query("ALTER TABLE `".DB_PREFIX."product_option_value` ADD `code_sku` VARCHAR(80) NOT NULL");
                }
            
    }

    public function versionApp()
    {
        $data = [];
        //eventVersionApp

                $data['570'] = [
                    'version' => '1.0',
                    'date' => '07/10/2020'
                ];
            

                $data['604'] = [
                    'version' => '1.1',
                    'date' => '08/01/2021'
                ];
            

                $data['548'] = [
                    'version' => '1.5',
                    'date' => '15/12/2020'
                ];
            

        return $data;
    }
}
