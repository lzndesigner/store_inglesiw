<?php

/* common/login.twig */
class __TwigTemplate_162bba5f7153046349ddb433175366bb7008ef60e68a2e7e4133b7d43f20843b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
<div id=\"content\">
  <div class=\"container-fluid\"><br />
    <br />
    <div class=\"row\">
      <div class=\"col-sm-offset-2 col-md-offset-4 col-sm-6 col-md-4\">
        <div class=\"boxLogin\">
          <h1 class=\"panel-title\"><i class=\"fa fa-lock\"></i> ";
        // line 8
        echo (isset($context["text_login"]) ? $context["text_login"] : null);
        echo "</h1>
          ";
        // line 9
        if ((isset($context["success"]) ? $context["success"] : null)) {
            // line 10
            echo "          <div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ";
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
          </div>
          ";
        }
        // line 14
        echo "          ";
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 15
            echo "          <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
          </div>
          ";
        }
        // line 19
        echo "          <form action=\"";
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\">
            <div class=\"form-group\">
              <label for=\"input-username\">";
        // line 21
        echo (isset($context["entry_username"]) ? $context["entry_username"] : null);
        echo "</label>
              <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>
                <input type=\"text\" name=\"username\" value=\"";
        // line 23
        echo (isset($context["username"]) ? $context["username"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_username"]) ? $context["entry_username"] : null);
        echo "\" id=\"input-username\" class=\"form-control\" />
              </div>
            </div>
            <div class=\"form-group\">
              <label for=\"input-password\">";
        // line 27
        echo (isset($context["entry_password"]) ? $context["entry_password"] : null);
        echo "</label>
              <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-lock\"></i></span>
                <input type=\"password\" name=\"password\" value=\"";
        // line 29
        echo (isset($context["password"]) ? $context["password"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_password"]) ? $context["entry_password"] : null);
        echo "\" id=\"input-password\" class=\"form-control\" />
              </div>                
            </div>
            <div class=\"text-left\">
              <button type=\"submit\" class=\"btn btn-primary\"><i class=\"fa fa-key\"></i> ";
        // line 33
        echo (isset($context["button_login"]) ? $context["button_login"] : null);
        echo "</button>
              ";
        // line 34
        if ((isset($context["forgotten"]) ? $context["forgotten"] : null)) {
            // line 35
            echo "              <a href=\"";
            echo (isset($context["forgotten"]) ? $context["forgotten"] : null);
            echo "\" class=\"btn btn-danger\">";
            echo (isset($context["text_forgotten"]) ? $context["text_forgotten"] : null);
            echo "</a>
              ";
        }
        // line 37
        echo "            </div>
            ";
        // line 38
        if ((isset($context["redirect"]) ? $context["redirect"] : null)) {
            // line 39
            echo "            <input type=\"hidden\" name=\"redirect\" value=\"";
            echo (isset($context["redirect"]) ? $context["redirect"] : null);
            echo "\" />
            ";
        }
        // line 41
        echo "          </form>
        </div><!-- box Login -->
      </div>
    </div>
  </div>
</div>
";
        // line 47
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "common/login.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 47,  113 => 41,  107 => 39,  105 => 38,  102 => 37,  94 => 35,  92 => 34,  88 => 33,  79 => 29,  74 => 27,  65 => 23,  60 => 21,  54 => 19,  46 => 15,  43 => 14,  35 => 10,  33 => 9,  29 => 8,  19 => 1,);
    }
}
/* {{ header }}*/
/* <div id="content">*/
/*   <div class="container-fluid"><br />*/
/*     <br />*/
/*     <div class="row">*/
/*       <div class="col-sm-offset-2 col-md-offset-4 col-sm-6 col-md-4">*/
/*         <div class="boxLogin">*/
/*           <h1 class="panel-title"><i class="fa fa-lock"></i> {{ text_login }}</h1>*/
/*           {% if success %}*/
/*           <div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> {{ success }}*/
/*             <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*           </div>*/
/*           {% endif %}*/
/*           {% if error_warning %}*/
/*           <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*             <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*           </div>*/
/*           {% endif %}*/
/*           <form action="{{ action }}" method="post" enctype="multipart/form-data">*/
/*             <div class="form-group">*/
/*               <label for="input-username">{{ entry_username }}</label>*/
/*               <div class="input-group"><span class="input-group-addon"><i class="fa fa-user"></i></span>*/
/*                 <input type="text" name="username" value="{{ username }}" placeholder="{{ entry_username }}" id="input-username" class="form-control" />*/
/*               </div>*/
/*             </div>*/
/*             <div class="form-group">*/
/*               <label for="input-password">{{ entry_password }}</label>*/
/*               <div class="input-group"><span class="input-group-addon"><i class="fa fa-lock"></i></span>*/
/*                 <input type="password" name="password" value="{{ password }}" placeholder="{{ entry_password }}" id="input-password" class="form-control" />*/
/*               </div>                */
/*             </div>*/
/*             <div class="text-left">*/
/*               <button type="submit" class="btn btn-primary"><i class="fa fa-key"></i> {{ button_login }}</button>*/
/*               {% if forgotten %}*/
/*               <a href="{{ forgotten }}" class="btn btn-danger">{{ text_forgotten }}</a>*/
/*               {% endif %}*/
/*             </div>*/
/*             {% if redirect %}*/
/*             <input type="hidden" name="redirect" value="{{ redirect }}" />*/
/*             {% endif %}*/
/*           </form>*/
/*         </div><!-- box Login -->*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/* </div>*/
/* {{ footer }}*/
